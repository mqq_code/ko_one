// babel配置对象
module.exports = {
  presets: [ // 预设：相当于插件的集合
    '@babel/preset-env' // 此预设内包含 es6+ 大部分常用语法
  ],
  plugins: [ // 配置babel转换语法的插件
    // '@babel/plugin-transform-block-scoping', // 转换let、const
    // '@babel/plugin-transform-arrow-functions' // 转换箭头函数
  ]
}
import './index.scss'
import { query } from '../utils/util'
import axios from 'axios'

axios.get('https://zyxcl-music-api.vercel.app/playlist/detail', {
  params: { id: query().id }
}).then(res => {
  // console.log(res.data.playlist)
})

axios.get('https://zyxcl-music-api.vercel.app/comment/playlist', {
  params: { id: query().id }
}).then(res => {
  console.log(res.data)
})


// 单独抛出
export const $ = (el, parent = document) => {
  return parent.querySelector(el)
}

export const gets = (el, parent = document) => {
  return [...parent.querySelectorAll(el)]
}

export const formatCount = count => {
  return count > 10000 ? (count / 10000).toFixed(1) + '万' : count
}

export const query = () => {
  // 删除? =>  id=156934569&a=100&b=200
  // 根据 & 拆分 => ['id=156934569', 'a=100', 'b=200']
  // 遍历数组，根据=拆分每一项 
  let obj = {}
  let arr = location.search.slice(1).split('&')
  arr.forEach(item => {
    let [key, val] = item.split('=')
    obj[key] = val
  })
  return obj
}

// 默认抛出
export default {
  $,
  gets,
  formatCount
}
import './index.scss'
import axios from 'axios'
import utils, { $, gets, formatCount } from '../utils/util'
// import * as utils from '../utils/util'
import img from '../assets/abc.jpg'
import './js/test'

// tab切换
gets('nav span').forEach((span, index) => {
  span.addEventListener('click', () => {
    // 修改导航高亮
    $('.active').classList.remove('active')
    span.classList.add('active')
    // 显示对应的内容
    $('.content.show').classList.remove('show')
    gets('.content')[index].classList.add('show')
  })
})

// 渲染推荐音乐
async function renderSongsList() {
  const res = await axios.get('https://zyxcl-music-api.vercel.app/personalized', {
    params: {
      limit: 6
    }
  })
  console.log(res.data.result)
  $('.song-list').innerHTML = res.data.result.map(item => `
    <li data-id="${item.id}">
      <img src="${item.picUrl}"/>
      <p>${item.name}</p>
      <span>${formatCount(item.playCount)}</span>
    </li>
  `).join('')
  // 跳转详情
  gets('.song-list li').forEach(li => {
    li.addEventListener('click', () => {
      location.href = './detail.html?id=' + li.getAttribute('data-id')
    } )
  })
}
renderSongsList()


async function renderNewSong() {
  const res = await axios.get('https://zyxcl-music-api.vercel.app/personalized/newsong')
  $('.newsong').innerHTML = res.data.result.map(item => `
    <li>
      <h4>${item.name}</h4>
      <p>${item.song.artists.map(val => val.name).join('/')} - ${item.song.album.name}</p>
      <b></b>
    </li>
  `).join('')
}
renderNewSong()

// let img1 = new Image()
// img1.src = img
// img1.width = 200
// $('.search').appendChild(img1)
// console.log(123)


axios.post('/search/hot').then(res => {
  console.log(res.data)
})
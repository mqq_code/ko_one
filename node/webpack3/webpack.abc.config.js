const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: {
    index: './src/index/index.js',
    detail: './src/detail/index.js'
  },
  devtool: 'source-map', // 生成打包前后代码的映射文件
  output: {
    path: path.join(__dirname, 'build'),
    // 输出的文件名添加hash值，防止用户缓存
    filename: 'js/[name].[hash:6].js',
    // 管理输出的图片名
    assetModuleFilename: 'images/[name].[hash:6][ext]'
  },
  module: {
    rules: [
      {
        test: /\.(scss|css|sass)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|gif|webp|svg|ttf|woff|eot)$/i,
        type: 'asset'
      },
      {
        test: /\.js$/i,
        exclude: /node_modules/, // 不转译此目录下的js文件
        use: ['babel-loader']
      },
      {
        test: /\.html$/i,
        use: 'html-loader'
      }
    ]
  },
  plugins: [
    // 创建html文件
    new HtmlWebpackPlugin({
      template: './src/index/index.html',
      filename: 'index.html',
      chunks: ['index']
    }),
    new HtmlWebpackPlugin({
      template: './src/detail/index.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    // 抽离js中的css文件
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash:6].css'
    }),
    // 打包前清空上一次打包的文件
    new CleanWebpackPlugin()
  ],
  resolve: {
    // 定义路径别名
    alias: {
      '@': path.join(__dirname, 'src'),
      'uu': path.join(__dirname, 'src/utils'),
      aa$: path.join(__dirname, 'src/utils/util.js'),
      // Utilities: path.resolve(__dirname, 'src/utilities/'),
      // Templates: path.resolve(__dirname, 'src/templates/'),
    },
  },
  devServer: { // 起服务
    port: 3000,
    open: true,
    setupMiddlewares: (middlewares, devServer) => {
      // 模拟接口 
      // devServer.app === express()
      devServer.app.post('/search/hot', (req, res) => {
        res.send({
          code: 1,
          msg: '成功',
          values: ['刘德华', '蔡徐坤', '肖战', '许嵩', '徐良', '汪苏泷']
        })
      });
      devServer.app.get('/search/hot', (req, res) => {
        res.send({
          code: 1,
          msg: '成功',
          values: ['刘德华', '蔡徐坤', '肖战', '许嵩', '徐良', '汪苏泷']
        })
      });
      return middlewares
    },
  },
}

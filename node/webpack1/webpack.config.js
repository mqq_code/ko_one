const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// webpack配置项
module.exports = {
  // 打包模式
  // mode: 'development', // 开发模式不会压缩代码
  mode: 'production', // 生产模式
  // webpack打包的入口文件
  entry: './src/main.js',
  // 输出文件
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  // 配置加载器（loader），配置js加载其他类型文件的工具
  module: {
    // 加载规则
    rules: [
      {
        test: /\.(scss|css)$/i, // 配置加载文件的后缀名
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(jpe?g|png|gif|webp|svg)$/i,
        type: 'asset' // webpack5使用内置的asset module 解析图片
      }
    ]
  },
  // 插件：扩展webpack功能
  plugins: [
    // 自动创建html文件添加到打包后的目录，并且自动引入打包后的js文件
    new HtmlWebpackPlugin({
      template: './src/index.html', // 创建的模版
      filename: 'index.html' // 生成的文件名
    })
  ],
  // 配置devServer
  devServer: {
    port: 3000, // 起服务的端口号
    open: true // 自动打开浏览器
  }
}
// CommonJS规范（require引入，module.exports 抛出）
// const Person = require('./a')
// require('./b')

// es module
// 引入 import
// 抛出 export default、export

import './scss/style.scss'
// import Person from './js/a'
import img from './assets/img.jpg'
// let xm = new Person('小明')
// console.log('这是index.js')
// console.log(xm)

// let title = document.querySelector('h1')
// console.log(title)
// title.style.color = 'green'

// 创建img添加到页面
console.log(img)
let img1 = new Image()
img1.src = img
img1.onload = () => {
  document.querySelector('.wrap').appendChild(img1)
}



let xhr = new XMLHttpRequest()
xhr.open('GET', 'https://zyxcl-music-api.vercel.app/banner')
xhr.send()
xhr.onreadystatechange = () => {
  if(xhr.readyState === 4) {
    console.log(xhr.responseText)
  }
}
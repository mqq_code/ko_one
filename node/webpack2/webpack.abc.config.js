const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  mode: 'development', // 开发模式
  // entry: './src/index.js', // 单入口文件
  // entry: ['./src/index.js', './src/main.js'], // 多入口文件,输出成一个js
  entry: { // 多入口文件,输出成多个js
    app: './src/index.js',
    app_test: './src/main.js'
  },
  output: { // 输出文件
    path: path.join(__dirname, 'build'),
    filename: 'js/[name].js' // 输出文件名
  },
  module: {
    rules: [
      {
        test: /\.(scss|css|sass)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|gif|webp|svg|ttf|woff|eot)$/i,
        type: 'asset'
      },
      {
        test: /\.js$/i,
        exclude: /node_modules/, // 不转译此目录下的js文件
        use: ['babel-loader']
      }
    ]
  },
  plugins: [
    // 创建html文件
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/main.html',
      filename: 'main.html',
      chunks: ['app_test']
    }),
    // 抽离js中的css文件
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    }),
    // 打包前清空上一次打包的文件
    new CleanWebpackPlugin()
  ],
  devServer: { // 起服务
    port: 3000,
    open: true,
    proxy: { // 配置请求代理，解决跨域问题
      // 请求 /api/user/lunch === http://localhost:5000/api/user/lunch
      // '/api': 'http://localhost:5000',

      // 请求 /user/lunch === http://localhost:5000/user/lunch
      '/user': 'http://localhost:5000',

      // 请求 /api/user/lunch === http://localhost:5000/user/lunch
      '/api': {
        target: 'http://localhost:5000',
        pathRewrite: { '^/api': '' },
      },
    }
  }
}

const express = require('express')
const path = require('path')
const axios = require('axios')

// 创建服务器应用
const app = express()

// 处理静态资源，让public目录下的所有文件都可以被访问
app.use(express.static(path.join(__dirname, './public')))


app.post('/test', (req, res) => {
  axios.post('http://192.168.1.147:5000/user/lunch').then(data => {
    res.send(data.data)
  })
})



app.listen(3000, () => {
  console.log('服务启动成功 http://localhost:3000')
  console.log('服务启动成功 http://192.168.1.147:3000')
})
const express = require('express')

// 创建服务器应用
const app = express()


// 设置允许跨域访问该服务
// app.all('*', function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
//   res.header('Access-Control-Allow-Headers', 'Content-Type');
//   next();
// });


// 获取列表接口
app.get('/user/info', (req, res) => {
  console.log(req.query.callback)
  // 设置响应头
  res.setHeader('Content-type', 'application/javascript;charset=utf-8')
  let obj = {
    code: 1,
    msg: '成功',
    values: {
      username: '张志毅',
      age: 17,
      sex: '男',
      hobby: ['睡觉', '英雄联盟'],
      money: 10
    }
  }
  res.send(`
    ${req.query.callback}(${JSON.stringify(obj)});
  `)
})


app.get('/user/list', (req, res) => {
  console.log(req.query.callback)
  let arr = [1,2,3,4,5,6,7]
  res.send(`
    ${req.query.callback}(${JSON.stringify(arr)});
  `)
})



app.post('/user/lunch', (req, res) => {
  res.send([
    '黄焖鸡',
    '大盘鸡',
    '地锅鸡',
    '炒鸡',
    '烤鸡',
    '炸鸡',
    '麻辣鸡脖'
  ])
})


app.listen(5000, () => {
  console.log('服务启动成功 http://localhost:5000')
  console.log('服务启动成功 http://192.168.1.147:5000')
})
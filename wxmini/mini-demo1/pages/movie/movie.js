// pages/movie/movie.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "开始学习小程序",
    list: ['吃饭', '睡觉', '学习', '逛街'],
    hobby: [
      {
        id: 0,
        text: '打篮球'
      },
      {
        id: 1,
        text: '打台球'
      },
      {
        id: 2,
        text: '打羽毛球'
      },
      {
        id: 3,
        text: '保龄球'
      },
      {
        id: 4,
        text: '高尔夫球'
      }
    ],
    num: 60,
    obj: {
      name: '王小明',
      age: 10
    }
  },

  changeObjval (e) {
    console.log('修改obj.name', e.detail.value) // 获取input value值
    // 手动更新数据
    this.setData({
      obj: {...this.data.obj, name: e.detail.value}
    })
  },

  changeAge (e) {
    const obj = {...this.data.obj}
    console.log(e)
    // console.log('button', e.target) // 触发事件的元素
    // console.log('button', e.currentTarget) // 绑定事件的元素
    this.setData({
      obj: {...obj, age: obj.age + e.target.dataset.num}
    })
  },

  clickRow (e) {
    // console.log('clickRow', e.target) // 触发事件的元素
    // console.log('clickRow', e.currentTarget) // 绑定事件的元素 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
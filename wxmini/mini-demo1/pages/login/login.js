// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    visible: false,
    username: '',
    password: '',
    list: [
      {
        id: 0,
        title: '苹果',
        price: 20,
        count: 0
      },
      {
        id: 1,
        title: '香蕉',
        price: 30,
        count: 0
      },
      {
        id: 2,
        title: '梨',
        price: 10,
        count: 0
      }
    ],
    total: 0
  },

  changCount (e) {
    // e.detail: 自组件传给父组件的数据
    const { num, id } = e.detail
     // 拷贝数据
    const list = [...this.data.list]
    // 遍历数组根据id查找要修改的数据
    list.forEach(item => {
      if (id === item.id) {
        item.count += num
      }
    })
    // 计算总价
    const total = list.reduce((prev, val) => prev + val.price * val.count, 0)
    // 更新数据
    this.setData({ list, total })
  },

  showDialog () {
    this.setData({
      visible: true
    })
  },

  hideDialog () {
    this.setData({
      visible: false
    })
  },

  ok () {
    console.log('点击了确定按钮', this.data.username, this.data.password)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
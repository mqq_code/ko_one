// pages/info/info.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    total: null
  },
  getList (cb) {
    if (this.data.total && this.data.list.length >= this.data.total) {
      wx.showToast({
        title: '已经加载完所有数据了',
        icon: 'success',
        duration: 2000
      })      
      return
    }
    wx.showLoading({
      title: '加载中',
    })
    wx.request({
      url: 'https://zyxcl-music-api.vercel.app/top/playlist',
      data: {
        limit: 30,
        offset: this.data.list.length
      },
      success: res => {
        wx.hideLoading()
        this.setData({
          list: [...this.data.list, ...res.data.playlists],
          total: res.data.total
        })
        cb && cb()
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('下拉刷新');
    this.setData({
      list: [],
      total: null
    })
    this.getList(() => {
      // 数据加载完成关闭下拉动画
      wx.stopPullDownRefresh()
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getList()
    console.log('到底了');
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
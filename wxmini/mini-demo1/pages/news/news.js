// pages/news/news.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    visible: false,
    scrollTop: 0,
    banners: [{
      imageUrl: "http://p1.music.126.net/QyrB8qDQ2ioiBpG5zSpF4g==/109951167691054083.jpg",
      targetId: 1964644539
    }, {
      imageUrl: "http://p1.music.126.net/pbuRmbUXGJF4xNP-_YrJ0A==/109951167691058452.jpg",
      targetId: 1964443045
    }, {
      imageUrl: "http://p1.music.126.net/qgoEC_jhNkC2wpN9cvzKtQ==/109951167691051450.jpg",
      targetId: 1964381438
    }, {
      imageUrl: "http://p1.music.126.net/0MOVmdETgpGzBcuqPZSEtw==/109951167691519166.jpg",
      targetId: 2
    }, {
      imageUrl: "http://p1.music.126.net/vKELFkODIR4CDV40eA6ZuQ==/109951167691082017.jpg",
      targetId: 25628
    }, {
      imageUrl: "http://p1.music.126.net/r13J7wRkav3rz--Re9n6Ag==/109951167691103678.jpg",
      targetId: 1957503897
    }, {
      imageUrl: "http://p1.music.126.net/PKpZgN4ehId7UDT_RG_Ddg==/109951167691116102.jpg",
      targetId: 0
    }, {
      imageUrl: "http://p1.music.126.net/yfXlivyMMW7fAeX_yf63UA==/109951167691098673.jpg",
      targetId: 1964383136
    }]
  },

  scroll(e) {
    console.log(e.detail.scrollTop)
    this.setData({
      visible: e.detail.scrollTop > 500
    })
  },

  backTop() {
    this.setData({
      scrollTop: 0
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
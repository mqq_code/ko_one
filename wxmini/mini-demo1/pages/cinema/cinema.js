// pages/cinema/cinema.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [
      {
        id: 0,
        title: '服装品牌',
        child: ['阿迪达斯', '耐克', '李宁', '安踏', '乔丹', '鸿星尔克']
      },
      {
        id: 1,
        title: '牛奶品牌',
        child: ['蒙牛', '伊利', '三元', '完达山', '君乐宝', '旺仔']
      },
      {
        id: 2,
        title: '快餐',
        child: ['kfc', '金拱门', '必胜客', '德克士', '华莱士', '李先生', '黄焖鸡', '沙县小吃']
      },
      {
        id: 3,
        title: '小吃',
        child: ['臭豆腐', '烤冷面', '瘦肉丸', '肠粉', '铁板烧', '煎饼', '驴肉火烧']
      }
    ],
    curIndex: 0
  },

  linkDetail (e) {
    const { text } = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/detail/detail?text=${text}&user=zyx&time=${Date.now()}`,
    })
  },

  changeTab (e) {
    const { index } = e.currentTarget.dataset
    console.log(index)
    this.setData({
      curIndex: index
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
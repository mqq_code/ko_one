// components/goods/goods.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    info: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    change (e) {
      const { num } = e.currentTarget.dataset
      // 调用父组件的自定义事件，把参数传给父组件
      this.triggerEvent('changeCount', {
        num,
        id: this.data.info.id
      })
    }
  },

  // 监听数据变化
  observers: {
    'info.count': function () {
      console.log('info.count发生改变', this.data.info);
    }
  }

})

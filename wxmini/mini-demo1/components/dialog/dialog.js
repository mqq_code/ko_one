// components/dialog/dialog.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String,
      value: "默认标题",
    },
    okText: {
      type: String,
      value: "确定",
    },
    cancelText: {
      type: String,
      value: "取消",
    },
  },

  /**
   * 组件的初始数据
   */
  data: {},

  /**
   * 组件的方法列表
   */
  methods: {
    close() {
      console.log("点击了关闭按钮");
      // 调用父组件传过来的自定义事件
      this.triggerEvent("close");
    },
    cancel() {
      this.triggerEvent("cancel");
    },
    ok() {
      this.triggerEvent("ok");
    },
  },
  // 组件内的声明周期
  lifetimes: {
    attached: function () {
      // 在组件实例进入页面节点树时执行
      console.log("弹窗显示了");
    },
    detached: function () {
      // 在组件实例被从页面节点树移除时执行
      console.log("弹窗关闭了");
    },
  },
});

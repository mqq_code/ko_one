// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money: 10000, // 当前身家
    chipMoney: 0, // 当前的下注金额
    diceArr: [
      {
        id: 0,
        num: 1
      },
      {
        id: 1,
        num: 2
      },
      {
        id: 2,
        num: 3
      }
    ] // 当前骰子
  },
  // 开始下注
  start (e) {
    if (this.loading) return
    if (this.data.chipMoney > 0) {
      // 开始摇骰子
      this.loading = true
      this.changeDice().then(res => {
        let money = this.data.money
        if (e.detail.text === res) { // 赢了
          money += this.data.chipMoney * e.detail.scale
        } else { // 输了
          money -= this.data.chipMoney
        }
        this.setData({
          money,
          chipMoney: 0
        })
      })
    } else {
      wx.showToast({
        title: '请下注',
        icon: 'error'
      })
    }
  },

  // 摇骰子
  changeDice () {
    return new Promise((resolve, reject) => {
      const timer = setInterval(() => {
        this.setData({
          diceArr: this.data.diceArr.map((item) => ({
            ...item,
            num: Math.floor(Math.random() * 6 + 1)
          }))
        })
      }, 100)
      setTimeout(() => {
        // 3s之后停止摇骰子
        clearInterval(timer)
        this.loading = false
        resolve(this.getRes(this.data.diceArr))
        // 计算结果
      }, 3000)
    })
  },

  // 计算结果
  getRes (arr) {
    let res = arr.map(v => v.num)
    if ([...new Set(res)].length === 1) {
      return '豹子'
    }
    let total = res.reduce((prev, val) => prev + val)
    if (total > 9) return '大'
    return '小'
  },

  // 修改下注金额
  changeChipMoney (e) {
    this.setData({
      chipMoney: e.detail.n
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
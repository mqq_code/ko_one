// components/dice/dice.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    diceArr: {
      type: Array,
      value: [
        {
          id: 0,
          num: 1
        },
        {
          id: 1,
          num: 2
        },
        {
          id: 2,
          num: 3
        }
      ]
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})

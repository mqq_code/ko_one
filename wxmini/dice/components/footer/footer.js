// components/footer/footer.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    btns: [
      {
        text: '大',
        scale: 1
      },
      {
        text: '豹子',
        scale: 24
      },
      {
        text: '小',
        scale: 1
      }
    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    start (e) {
      // 调用父组件的下注事件
      this.triggerEvent('start', e.currentTarget.dataset.info)
    }
  }
})

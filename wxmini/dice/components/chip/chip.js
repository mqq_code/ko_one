// components/chip/chip.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 当前下注金额
    chipMoney: {
      type: Number
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    chipArr: [1, 2, 5, 20],
    x: '-30rpx',
    y: '-30rpx'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    ante (e) {
      // 触发父组件的下注事件
      this.triggerEvent('ante', {
        n: this.data.chipMoney + e.target.dataset.num
      })
    },
    clear () {
      this.triggerEvent('ante', { n: 0 })
    },
    drag (e) {
      // console.log(e.detail.x)
      this.endX = e.detail.x
      this.endY = e.detail.y
    },
    touchend (e) {
      console.log('end', this.endX, this.endY)
      if (this.endX < -80 || this.endX > 50 || this.endY < -80 || this.endY > 50) {
        this.clear()
      } else {
        this.setData({
          x: '-30rpx',
          y: '-30rpx'
        })
      }
    }
  }
})

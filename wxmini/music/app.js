// app.js
App({
  onLaunch() {
    const res = wx.getMenuButtonBoundingClientRect()
    this.globalData.menuInfo = res
    this.globalData.titleBarHeight = res.bottom + 10 + 'px'
  },
  globalData: {
    menuInfo: {},
    titleBarHeight: '',
    audioCtx: wx.createInnerAudioContext()
  }
})

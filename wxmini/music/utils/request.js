const baseURL = 'https://zyxcl-music-api.vercel.app'

const axios = function ({ url, data = {}, method = 'GET', header = {} }) {
  wx.showLoading({
    title: '加载中'
  })  
  return new Promise((resolve, reject) => {
    wx.request({
      url: baseURL + url,
      data,
      header,
      method,
      timeout: 5000,
      success: res => {
        resolve(res.data)
        wx.hideLoading()
      },
      fail: e => {
        reject(e)
        wx.hideLoading()
      }
    })
  })
}
axios.get = function (url, data) {
  return axios({ url, data, method: 'GET' })
}
axios.post = function (url, data) {
  return axios({ url, data, method: 'POST' })
}

export default axios
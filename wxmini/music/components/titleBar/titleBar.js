// components/titleBar/titleBar.js
const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String,
      value: '音乐'
    },
    opacity: {
      type: Boolean,
      value: false
    },
    background: {
      type: String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    height: app.globalData.menuInfo.bottom + 10 + 'px',
    titleHeight: app.globalData.menuInfo.height + 'px',
    leftWidth: app.globalData.menuInfo.width + 'px' 
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})

// pages/player/player.js
import axios from '../../utils/request'
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    titleBarHeight: app.globalData.titleBarHeight,
    al: {},
    lyric: [],
    playing: false,
    activeIndex: 0,
    scrollTop: 0,
    hotComments: []
  },
  play () {
    if (this.data.playing) {
      app.globalData.audioCtx.pause()
    } else {
      app.globalData.audioCtx.play()
    }
    this.setData({ playing: !this.data.playing })
  },
  // 获取歌曲播放地址
  getSongUrl (id) {
    axios.get('/song/url', { id }).then(res => {
      app.globalData.audioCtx.src = res.data[0].url
      app.globalData.audioCtx.onCanplay(() => {
        this.play()
        app.globalData.audioCtx.onTimeUpdate(this.timeUpdate)
      })
    })
  },
  // 获取歌曲详情
  getSongDetail (ids) {
    axios.post('/song/detail?time=' + Date.now(), { ids }).then(res => {
      this.setData({
        al: res.songs[0].al
      })
    })
  },
  formatTime (str) {
    let [m, s] = str.split(':').map(v => Number(v))
    return Math.round((m * 60 + s))
  },
  // 获取歌词
  getLyric (id) {
    axios.get('/lyric?time=' + Date.now(), { id }).then(res => {
      let lyric = res.lrc.lyric.split(/\n/g).map(item => {
        const [time, text] = item.split(']')
        return {
          time: this.formatTime(time.slice(1)),
          text
        }
      }).filter(v => !isNaN(v.time))
      console.log(lyric)
      this.setData({
        lyric
      })
    })
  },
  // 获取评论
  getComment (id) {
    axios.get('/comment/hot?type=0&time=' + Date.now(), { id }).then(res => {
      console.log(res);
      this.setData({
        hotComments: res.hotComments
      })
    })
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function ({ id }) {
    // 显示右上角的分享按钮
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
    // id = '1441758494'
    this.id = id
    this.getLyric(id)
    this.getSongDetail(id)
    this.getSongUrl(id)
    this.getComment(id)
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage () {
    return {
      title: this.data.al.name,
      path: '/pages/player/player?id=' + this.id,
      imageUrl: this.data.al.picUrl
    }
  },

  timeUpdate () {
    const currentTime = Math.round(app.globalData.audioCtx.currentTime)
    const index = this.data.lyric.findIndex(v => v.time === currentTime)
    if (index > -1) {
      this.setData({
        activeIndex: index,
        scrollTop: 64 * index - 128 + 'rpx'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    app.globalData.audioCtx.offTimeUpdate(this.timeUpdate)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
})

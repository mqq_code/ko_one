// pages/search/search.js
import axios from '../../utils/request'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchText: '',
    hotList: [],
    historyList: wx.getStorageSync('historyList') || [],
    titleBarHeight: app.globalData.titleBarHeight,
    currentPage: 0, // 0: 默认页面， 1: 模糊搜索， 2: 歌曲列表
    focus: true,
    keywordsList: [],
    songsList: []
  },

  focus () {
    if (this.data.searchText) {
      this.setData({
        focus: true,
        currentPage: 1
      })
    } else {
      this.setData({ focus: true })
    }
  },
  blur () {
    this.setData({ focus: false })
  },
  // 搜索建议
  getSuggest () {
    axios.get('/search/suggest', {
      keywords: this.data.searchText,
      type: 'mobile'
    }).then(res => {
      this.setData({
        keywordsList: res.result.allMatch
      })
    })
  },
  // 输入搜索内容
  input (e) {
    if (e.detail.value) {
      this.setData({ currentPage: 1 })
      // 添加防抖：连续调用同一个函数只执行最后一次
      if (this.timer) clearTimeout(this.timer)
      this.timer = setTimeout(() => {
        this.getSuggest()
        this.timer = null
      }, 1000)
    } else {
      this.setData({ currentPage: 0 })
      if (this.timer) clearTimeout(this.timer)
    }
  },
  // 确定搜素
  confirm () {
    if (this.data.searchText) {
      let historyList = [...new Set([this.data.searchText, ...this.data.historyList])]
      if (historyList.length > 10) {
        historyList.pop()
      }
      wx.setStorageSync('historyList', historyList)
      this.setData({
        currentPage: 2,
        historyList
      })
      // 搜索
      axios.get('/cloudsearch', {
        keywords: this.data.searchText
      }).then(res => {
        this.setData({
          songsList: res.result.songs.map(item => {
            // 拼接歌手和撞击
            let arText = item.ar.map(v => v.name).join('/') + ' - ' + item.al.name
            // 关键字高亮
            arText = this.highLightKeywords(this.data.searchText, arText)
            let name = this.highLightKeywords(this.data.searchText, item.name)
            return {
              ...item,
              name,
              arText
            }
          })
        })
      })
    }
  },
  // 点击搜索建议
  searchSuggest (e) {
    const { keyword } = e.currentTarget.dataset
    this.setData({
      searchText: keyword
    })
    this.confirm()
  },
  clear () {
    this.setData({
      searchText: '',
      currentPage: 0,
      keywordsList: [],
      songsList: []
    })
  },
  highLightKeywords (keyword, allText) {
    return allText.replace(keyword, `<span style="color: blue">${keyword}</span>`)
  },
  clearHistory () {
    this.setData({
      historyList: []
    })
    wx.setStorageSync('historyList', [])
  },
  goPlayer (e) {
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/player/player?id=${id}`,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    const res = await axios.get('/search/hot/detail')
    this.setData({
      hotList: res.data
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
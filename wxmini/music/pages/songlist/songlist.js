import axios from '../../utils/request'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    songs: [],
    info: {},
    loading: true,
    titleBarHeight: app.globalData.titleBarHeight
  },
  // 跳转歌单详情页
  goDetail () {
    wx.navigateTo({
      url: `/pages/songdetail/songdetail?id=${this.queryId}`,
    })
  },
  // 跳转播放页面
  goPlay (e) {
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/player/player?id=${id}`,
    })
  },
  // 获取歌单详情
  async getListDetail (id) {
    const res = await axios.get('/playlist/detail', { id })
    this.setData({
      info: {
        ...res.playlist,
        playCount: (res.playlist.playCount / 100000000).toFixed(1) + '亿'
      }
    })
  },
  // 获取歌单所有歌曲
  async getListSongs () {
    const res = await axios.get('/playlist/track/all', {
      id: this.queryId,
      limit: 20,
      offset: 50
    })
    this.setData({ songs: [...this.data.songs, ...res.songs] })
  },
  async getData (id) {
    await Promise.all([this.getListDetail(id), this.getListSongs()])
    this.setData({ loading: false })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function ({ id }) {
    this.queryId = id
    try {
      await this.getData(id)
    } catch (e) {
      wx.showToast({
        title: '网络错误，请稍后重试'
      })
      await this.getData(id)
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.info.trackCount && this.data.songs.length >= this.data.info.trackCount) return
    this.getListSongs() // 获取歌单所有歌曲
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
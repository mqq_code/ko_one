// pages/index/index.js
import axios from '../../utils/request'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    otherList: [],
    loading: true // 控制骨架屏显示隐藏
  },

  goSongList(e) {
    const { id } = e.currentTarget.dataset
    console.log('跳转歌单页面', id)
    wx.navigateTo({
      url: `/pages/songlist/songlist?id=${id}`,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    const res = await axios.get('/toplist/detail')
    console.log(res.list)
    if (res.code === 200) {
      this.setData({
        list: res.list.filter(v => v.tracks.length > 0),
        otherList: res.list.filter(v => v.tracks.length === 0),
        loading: false
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
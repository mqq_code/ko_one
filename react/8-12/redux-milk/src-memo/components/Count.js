import { useState, memo } from 'react'

const Count = () => {
  const [count, setCount] = useState(0)
  console.log('count组件更新了')
  return (
    <div>
      <button onClick={() => setCount(count - 1)}>-</button>
        {count}
      <button onClick={() => setCount(count + 1)}>+</button>
    </div>
  )
}

export default memo(Count, (prevProps, props) => {
  // 深比较props需要传入第二个参数，是一个回调函数，返回ture不更新组件，返回false更新组件
  if (prevProps.obj.obj.age !== props.obj.obj.age) {
    return false
  }
  return true
})

// 函数组件性能优化：
// memo高阶组件: 浅比较最新的props和当前的props
// export default memo(Count)






// import React, { Component, PureComponent } from 'react'

// // PureComponent: 内部实现了 shouldComponentUpdate 浅比较上一次的state、props和最新的state、props的属性
// class Count extends Component {

//   state = {
//     count: 0,
//     obj: {
//       age: 10
//     },
//     hobby: ['吃饭']
//   }

//   shouldComponentUpdate (nextProps, nextState) {
//     // for(let key in nextProps) {
//     //   if (nextProps[key] !== this.props[key]) {
//     //     return true
//     //   }
//     // }
//     // for(let key in nextState) {
//     //   if (nextState[key] !== this.state[key]) {
//     //     return true
//     //   }
//     // }
//     if (this.state.count !== nextState.count) {
//       return true
//     }
//     if (this.state.obj.age !== nextState.obj.age) {
//       return true
//     }
//     return false
//   }

//   changeCount = n => {
//     this.setState({
//       count: this.state.count + n
//     })
//   }

//   render() {
//     console.log('count组件渲染了')
//     return (
//       <div>
//         count: 
//         <button onClick={() => this.changeCount(-1)}>-</button>
//         {this.state.count}
//         <button onClick={() => this.changeCount(1)}>+</button>
//         <hr />
//         <div>age: {this.state.obj.age}
//           <button onClick={() => {
//             this.setState({
//               obj: {
//                 age: this.state.obj.age + 1
//               }
//             })
//           }}>+</button>
//         </div>
//         <hr />
//         <div>{this.state.hobby}</div>
//       </div>
//     )
//   }
// }
// export default Count
import React, { useState } from 'react'
import Count from './components/Count'

const App = () => {
  const [title, setTitle] = useState('')
  const [obj, setObj] = useState({ obj: { age: 10 } })

  return (
    <div>
      <h2>性能优化 - {title}</h2>
      <input type="text" value={title} onChange={e => {
        setTitle(e.target.value)
        if (e.target.value.length % 5 === 0) {
          setObj({ obj: { age: obj.obj.age + 1 } })
        }
      }} />
      <hr />
      <Count obj={obj} />
    </div>
  )
}

export default App
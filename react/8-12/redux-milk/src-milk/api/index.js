import axios from 'axios'

export const getZh = () => {
  return axios.get('/api/zonghe')
}
export const getXl = () => {
  return axios.get('/api/xiaoliang')
}
export const getSx = () => {
  return axios.get('/api/shangxin')
}
export const getDetail = (id) => {
  return axios.get('/api/detail', {
    params: { id }
  })
}
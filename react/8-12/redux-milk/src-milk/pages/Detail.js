import { useState, useEffect } from 'react'
import { getDetail } from '../api'

const Detail = props => {
  const [info, setInfo] = useState({})

  useEffect(() => {
    getDetail(props.match.params.id).then(res => {
      if (res.data.code === 0) {
        setInfo(res.data.values)
      } else {
        alert(res.data.msg)
      }
    })
  }, [])
  
  return (
    <div>{JSON.stringify(info)}</div>
  )
}

export default Detail
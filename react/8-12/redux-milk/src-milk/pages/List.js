import { useEffect, useState, useMemo } from 'react'
import { getZh, getXl, getSx } from '../api'
import { connect } from 'react-redux'

const apiList = {
  zh: getZh,
  xl: getXl,
  sx: getSx
}

const List = props => {
  const id = props.match.params.id
  const [list, setList] = useState([])

  useEffect(() => {
    // 监听路由变化
    if (apiList[id]) {
      apiList[id]().then(res => {
        setList(res.data.items)
        console.log(res.data.items)
      })
    } else {
      props.history.push('/404')
    } 
  }, [id])

  const curList = useMemo(() => {
    switch (props.sortType) {
      case 1:
        return [...list].sort((a, b) => a.price - b.price)
      case 2:
        return [...list].sort((a, b) => b.price - a.price)
      default:
        return list
    }
   }, [list, props.sortType])

   const goDetail = id => {
    props.history.push(`/detail/${id}`)
   }

  return (
    <div className={['list', props.grid ? 'grid' : ''].join(' ')}>
      {curList.map(item =>
        <dl key={item.item_id} onClick={() => goDetail(item.item_id)}>
          <dt><img src={item.img} alt="" /></dt>
          <dd>
            <h3>{item.title}</h3>
            <p className='price'>¥{item.price}</p>
            <p className='sold'>月销{item.sold}</p>
          </dd>
        </dl>
      )}
    </div>
  )
}

const mapStateToProps = state => {
  return {
    grid: state.grid,
    sortType: state.sortType
  }
}

export default connect(mapStateToProps)(List)

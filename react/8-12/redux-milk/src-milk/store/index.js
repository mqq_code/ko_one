import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

const initState = {
  grid: false, // 列表还是网格布局
  sortType: 0 // 0: 默认排毒， 1:升序，2:降序
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case 'CHANGE_GRID':
      return {...state, grid: !state.grid}
    case 'CHANGE_SORT':
      let sortType = state.sortType + 1
      if (sortType > 2) sortType = 0
      return {...state, sortType }
    default:
      return state
  }
}

const store = createStore(reducer, applyMiddleware(logger))
export default store

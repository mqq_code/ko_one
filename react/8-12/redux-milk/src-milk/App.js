import React from 'react'
import './App.scss'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Home from './pages/Home'
import Detail from './pages/Detail'
import Search from './pages/Search'
import NotFound from './pages/404'

const App = () => {
  return (
    <Switch>
      <Route path="/home" component={Home} />
      <Route path="/detail/:id" component={Detail} />
      <Route path="/search" component={Search} />
      <Route path="/404" component={NotFound} />
      <Redirect from='/' to="/home" />
    </Switch>
  )
}

export default App
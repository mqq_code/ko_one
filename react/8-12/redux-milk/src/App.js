import React, { useReducer } from 'react'
import Count from './components/Count'
import { Provider } from './context/store'

const reducer = (state, action) => {
  switch (action.type) {
    case 'change_name':
      return {...state, name: action.paylaod}
    case 'add_money':
      return {...state, money: state.money + 1}
    default :
      return state
  }
}

const App = () => {
  // useReducer和redux的区别
  // useReducer不可以拆分模块，存储的数据比较多时难以维护
  // useReducer没有中间件，无法扩展，无法处理异步
  const [state, dispatch] = useReducer(reducer, { name: '小明', money: 10000 })

  return (
    <Provider value={[state, dispatch]}>
      <div>
        <h2>性能优化</h2>
        <p>姓名: {state.name} <input type="text" value={state.name} onChange={e => {
          dispatch({
            type: 'change_name',
            paylaod: e.target.value
          })
        }} /></p>
        <p>财产: {state.money} <button onClick={() => {
          dispatch({
            type: 'add_money'
          })
        }}>+</button></p>

        <Count />
      </div>
    </Provider>
  )
}

export default App
import { createContext } from 'react'

const store = createContext({})


export const Provider = store.Provider
export const Consumer = store.Consumer
export default store



import React from 'react'
import { NavLink } from 'react-router-dom'
import RouterView from '../../../router/RouterView'

const Movie = (props) => {
  return (
    <div className='movie'>
      <nav>
        <NavLink to="/home/movie/hot">正在热映</NavLink>
        <NavLink to="/home/movie/coming">即将上映</NavLink>
      </nav>
      <div className="content">
        <RouterView routes={props.routes} />
      </div>
    </div>
  )
}

export default Movie
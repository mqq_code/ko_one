import { useState } from 'react'

const Count = () => {
  const [count, setCount] = useState(0)
  return (
    <div style={{height: '100px', background: 'red'}}>
      <button onClick={() => setCount(count -1)}>-</button>
      {count}
      <button onClick={() => setCount(count + 1)}>+</button>
    </div>
  )
}

export default Count
import React, { lazy, Suspense } from 'react'

// 组件懒加载，父级组件中最少有一个 Suspense 组件，
// 加载异步组件时会显示最近的 Suspense 组件中 fallback 的内容
const Count = lazy(() => import('./components/Count'))

const Coming = props => {
  return (
    <div>
      <h2>Coming</h2>
      <Suspense fallback={<b>加载中。。。。。</b>}>
        <Count />
      </Suspense>
    </div>
  )
}

export default Coming
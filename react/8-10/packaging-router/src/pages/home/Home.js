import React, { Suspense } from 'react'
import { NavLink } from 'react-router-dom'
import RouterView from '../../router/RouterView'


const Home = (props) => {
  return (
    <div className='home'>
      <main>
        {/* <Suspense fallback={<h1>正在加载</h1>}> */}
          <RouterView routes={props.routes} />
        {/* </Suspense> */}
      </main>
      <footer>
        <NavLink to="/home/movie">电影</NavLink>
        <NavLink to="/home/cinema">影院</NavLink>
        <NavLink to="/home/news">咨询</NavLink>
        <NavLink to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home
import { lazy } from 'react'
import withAuth from '../hoc/withAuth'
import Home from '../pages/home/Home'
import Login from '../pages/login/Login'
import Detail from '../pages/detail/Detail'
import City from '../pages/city/City'
import NotFound from '../pages/404'
// 二级路由
import Movie from '../pages/home/movie/Movie'
// import Cinema from '../pages/home/cinema/Cinema'
// import News from '../pages/home/news/News'
// import Mine from '../pages/home/mine/Mine'
// 三级路由
import Hot from '../pages/home/movie/hot/Hot'
import Coming from '../pages/home/movie/coming/Coming'

const routes = [
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/home/movie',
        component: Movie,
        children: [
          {
            path: '/home/movie/hot',
            component: Hot
          },
          {
            path: '/home/movie/coming',
            component: Coming
          },
          {
            exact: true,
            path: '/home/movie',
            to: '/home/movie/hot'
          },
          {
            to: '/404'
          }
        ]
      },
      {
        path: '/home/cinema',
        component: lazy(() => import('../pages/home/cinema/Cinema'))
      },
      {
        path: '/home/news',
        component: withAuth(lazy(() => import('../pages/home/news/News')))
      },
      {
        path: '/home/mine',
        // 封装高阶组件拦截登陆
        component: withAuth(lazy(() => import('../pages/home/mine/Mine')))
      },
      {
        exact: true,
        path: '/home',
        to: '/home/movie'
      },
      {
        to: '/404'
      }
    ]
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/detail',
    component: Detail
  },
  {
    path: '/city',
    component: City
  },
  {
    path: '/404',
    component: NotFound,
    exact: true
  },
  {
    exact: true,
    path: '/',
    to: '/home'
  },
  {
    to: '/404'
  }
]

export default routes

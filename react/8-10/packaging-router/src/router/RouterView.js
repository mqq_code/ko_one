import React from 'react'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'

const RouterView = (props) => {
  const routes = props.routes.filter(v => v.component)
  const redirect = props.routes.filter(v => v.to)
  return (
    <Switch>
      {routes.map(item =>
        <Route
          key={item.path}
          exact={item.exact}
          path={item.path}
          render={routeInfo => {
            // routeInfo: 路由信息对象
            const props = item.children ? { routes: item.children } : {}
            return <item.component {...props} {...routeInfo} />
          }}
        />
      )}
      {redirect.map(item =>
        <Redirect key={item.to} exact={item.exact} from={item.path} to={item.to} />
      )}
    </Switch>
  )
}

export default RouterView
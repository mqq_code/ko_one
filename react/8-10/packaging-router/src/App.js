import React, { Suspense } from 'react'
import './App.scss'
import RouterView from './router/RouterView'
import routes from './router/router.config'

const App = () => {
  return (
    <Suspense fallback={<h2 style={{color: 'red'}}>loading。。。</h2>}>
      <RouterView routes={routes} />
    </Suspense>
  )
}

export default App
import { Redirect } from 'react-router-dom'

export default (Com) => {

  const Auth = (props) => {
    const token = localStorage.getItem('token')
    if (token) {
      return <Com {...props} />
    }
    return <Redirect to="/login" />
  }

  return Auth
}
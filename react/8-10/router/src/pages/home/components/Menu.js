import { useState, useEffect } from 'react'
import style from '../home.module.scss'
import { NavLink, useLocation, useHistory } from 'react-router-dom'
import navlist, { allnav } from './navConfig'

const Menu = (props) => {
  const [curIndex, setIndex] = useState(0)
  const [title, setTitle] = useState('')
  const location = useLocation()
  const { push } = useHistory()

  useEffect(() => {
    // 路由改变根据当前的pathname，从所有导航列表中查找对应的数据
    const curNav = allnav.find(v => v.path === location.pathname)
    curNav && setTitle(curNav.name)
    // 根据当前路径查找tab的下标
    navlist.forEach((item, index) => {
      if (item.list.find(v => v.path === location.pathname)) {
        setIndex(index)
      }
    })
  }, [location])

  const changeTab = i => {
    setIndex(i)
    // tab切换的时候跳转到第一个子路径
    push(navlist[i].list[0].path)
  }

  return (
    <div className={style.menu}>
      <div className={style.tabBar}>
        {navlist.map((item, index) => (
          <p
            key={item.title}
            className={curIndex === index ? style.active : ''}
            onClick={() => changeTab(index)}
          >
            <i>{item.icon}</i>
            <span>{item.title}</span>
          </p>
        ))}
      </div>
      <div className={style.tabCon}>
        <h2>{title}</h2>
        <ul>
          {navlist[curIndex].list.map(item => (
            <li key={item.path}>
              <NavLink activeClassName={style.linkLight} to={item.path}>{item.name}</NavLink>
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default Menu
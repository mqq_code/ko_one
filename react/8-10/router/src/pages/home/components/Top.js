import { useState, useEffect } from 'react'
import style from '../home.module.scss'
import { useLocation, useHistory, NavLink } from 'react-router-dom'
import { allnav } from './navConfig'

const Top = () => {
  const [list, setList] = useState([])
  const location = useLocation()
  const { push } = useHistory()
  useEffect(() => {
    // 如果列表中不存在当前地址就添加到list
    if (!list.find(v => v.path === location.pathname)) {
      // 路由改变根据当前的pathname，从所有导航列表中查找对应的数据
      const curNav = allnav.find(v => v.path === location.pathname)
      setList([...list, curNav])
    }
  }, [location, list])

  const remove = (path, index, e) => {
    e.preventDefault() // 阻止跳转
    if (list.length === 1) {
      push('/login')
      localStorage.removeItem('token')
    } else if (index === list.length - 1) {
      push(list[index - 1].path)
    } else {
      push(list[index + 1].path)
    }
    setList(list.filter(v => v.path !== path))
  }

  return (
    <div className={style.top}>
      <div className={style.topNav}>
        {list.map((item, index) =>
          <NavLink activeClassName={style.active} key={item.path} to={item.path}>
            {item.name}
            <span onClick={(e) => remove(item.path, index, e)}>x</span>
          </NavLink>
        )}
      </div>
    </div>
  )
}

export default Top
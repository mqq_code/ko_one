import React from 'react'
import style from './home.module.scss'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Money from './money/Money'
import Flow from './flow/Flow'
import Source from './source/Source'
import User from './user/User'
import Menu from './components/Menu'
import Top from './components/Top'

const Home = () => {
  return (
    <div className={style.home}>
      <Menu />
      <div className={style.content}>
        <Top />
        <div className={style.main}>
          <Switch>
            <Route path="/home/money" component={Money} />
            <Route path="/home/flow" component={Flow} />
            <Route path="/home/source" component={Source} />
            <Route path="/home/user" component={User} />
            <Redirect from="/home" to="/home/money" />
          </Switch>
        </div>
      </div>
    </div>
  )
}

export default Home
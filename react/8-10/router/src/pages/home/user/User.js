import React from 'react'

const User = () => {
  return (
    <div>
      <h1>用户管理</h1>
      <h2>员工列表</h2>
      <ul>
        <li>洗碗工：侯婷儿</li>
        <li>快递员：娇娇</li>
        <li>代驾：坤坤</li>
        <li>滴滴：邦总</li>
      </ul>
    </div>
  )
}

export default User
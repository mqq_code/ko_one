import React from 'react'

const Source = () => {
  return (
    <div>
      <h1>资金来源</h1>
      <h2>渠道管理</h2>
      <ol>
        <li>送快递</li>
        <li>送外卖</li>
        <li>代价</li>
        <li>滴滴</li>
        <li>刷碗</li>
      </ol>
    </div>
  )
}

export default Source
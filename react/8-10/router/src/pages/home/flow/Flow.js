import React from 'react'

const Flow = () => {
  return (
    <div>
      <h1>资金流向</h1>
      <table border="1">
        <thead>
          <tr>
            <th>资金流向1</th>
            <th>资金流向2</th>
            <th>资金流向3</th>
            <th>资金流向4</th>
            <th>资金流向5</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>流向内容1</td>
            <td>流向内容2</td>
            <td>流向内容3</td>
            <td>流向内容4</td>
            <td>流向内容5</td>
          </tr>
          <tr>
            <td>流向内容1</td>
            <td>流向内容2</td>
            <td>流向内容3</td>
            <td>流向内容4</td>
            <td>流向内容5</td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default Flow
import React from 'react'
import style from './login.module.scss'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Checkbox, Form, Input } from 'antd'

const Login = (props) => {
  // 获取form表单的实例对象
  const [form] = Form.useForm()
  const login = () => {
    // 通过form实例对象触发表单验证
    form.validateFields().then(res => {
      localStorage.setItem('token', res.user + res.pwd)
      props.history.push('/home')
    }).catch(err => {
      console.log(err)
    })
  }
  return (
    <div className={style.login}>
      <Form
        form={form}
        className={style.loginForm}
        initialValues={{ remember: true }}
      >
        <Form.Item
          name="user"
          rules={[
            {
              required: true,
              message: '请输入用户名',
            },
          ]}
        >
          <Input prefix={<UserOutlined />} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          name="pwd"
          rules={[
            {
              required: true,
              message: '请输入密码!',
            }
          ]}
        >
          <Input prefix={<LockOutlined />} type="password" placeholder="密码"/>
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>记住账号</Checkbox>
          </Form.Item>
        </Form.Item>
        <Form.Item>
          <Button type="primary" block onClick={login}>登陆</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Login

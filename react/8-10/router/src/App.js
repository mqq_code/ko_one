import React from 'react'
import './App.scss'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import Home from './pages/home/Home'
import Login from './pages/login/Login'

const App = () => {
  return (
    <Switch>
      <Route path="/home" render={() => {
        const token = localStorage.getItem('token')
        if (!token) return <Redirect to="/login" />
        return <Home />
      }} />
      <Route path="/login" component={Login} />
      <Redirect to="/home" />
    </Switch>
  )
}

export default App
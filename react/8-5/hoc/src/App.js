import './App.css';
import React, { Component } from 'react'
import First from './components/First'
import Second from './components/Second'
import Third from './components/Third'

class App extends Component {
  render() {
    return (
      <div className='app'>
        <First />
        <Second title="这是测试标题" list={[1,2,3,4]} />
        <Third />
      </div>
    )
  }
}

export default App

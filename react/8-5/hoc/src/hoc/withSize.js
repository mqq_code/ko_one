import React, { Component } from 'react'

function withSize(Com) {
  class Size extends Component {
    state = {
      size: 'big' // big middle mini
    }
    resize = () => {
      const screenW = document.documentElement.clientWidth
      // console.log(screenW)
      if (screenW >= 800) {
        this.setState({
          size: 'big'
        })
      } else if (screenW >= 480 && screenW < 800) {
        this.setState({
          size: 'middle'
        })
      } else {
        this.setState({
          size: 'mini'
        })
      }
    }
    componentDidMount() {
      window.addEventListener('resize', this.resize)
      this.resize()
    }

    render() {
      // console.log(this.props)
      return (
        <Com size={this.state.size} {...this.props} />
      )
    }
  }
  return Size
}

export default withSize
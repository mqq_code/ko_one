import React, { Component } from 'react'
import withSize from '../hoc/withSize'

class Third extends Component {

  fontSize = {
    big: '40px',
    middle: '25px',
    mini: '12px'
  }

  render() {
    const { size } = this.props
    return (
      <div className='third' style={{ fontSize: this.fontSize[size] }}>
        <span>Third</span> {size}
      </div>
    )
  }
}

export default withSize(Third)
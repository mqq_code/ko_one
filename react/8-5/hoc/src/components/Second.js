import React, { Component } from 'react'
import withSize from '../hoc/withSize'

class Second extends Component {

  color = {
    big: 'tomato',
    middle: 'yellowgreen',
    mini: '#ccc'
  }
  
  render() {
    const { size, title, list } = this.props
    console.log(this.props)
    return (
      <div className='second' style={{ background: this.color[size] }}>
        {size}
        <h2>{title}</h2>
        <ul>
          {list.map(v => <li key={v}>{v}</li>)}
        </ul>
      </div>
    )
  }
}


const com = withSize(Second)
export default com
import React, { Component } from 'react'
import withSize from '../hoc/withSize'

const menu = new Array(5).fill(0)
class First extends Component {
  state = {
    list: [1,3,4,5]
  }

  render() {
    const { size } = this.props
    return (
      <div className='first'>
        {size === 'big' && <div className="logo">logo</div>}
        {size !== 'mini' && 
          <nav>
            {menu.map((v, i) => <span key={i}>导航{i + 1}</span>)}
          </nav>
        }
        {size === 'mini' &&
          <div className='menu'>
            <button>显示菜单</button>
          </div>
        }
      </div>
    )
  }
}

// 高阶组件：参数为组件返回值为新组件的函数
// 增强组件功能
export default withSize(First)

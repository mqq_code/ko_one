import React, { Component } from 'react'
import Second from './Second'
import { Provider } from '../context/color'

export default class First extends Component {
  state = {
    textColor: '#333333'
  }
  changeColor = color => {
    this.setState({
      textColor: color
    })
  }
  render() {
    return (
      <Provider value={{
        textColor: this.state.textColor,
        changeColor: this.changeColor
      }}>
        <div className='first' style={{ color: this.state.textColor }}>
          <h2>这是First.js</h2>
          <Second />
        </div>
      </Provider>
    )
  }
}

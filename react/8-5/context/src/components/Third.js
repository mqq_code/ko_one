import React, { Component } from 'react'
import { Consumer } from '../context/count'
import { Consumer as ColorConsumer } from '../context/color'

export default class Third extends Component {
  render() {
    return (
      <ColorConsumer>
        {value => (
          <Consumer>
            {({ count, changeCount }) => (
              <div className='third'>
                <h3>Third</h3>
                <p>修改App.j的count</p>
                <button onClick={() => changeCount(-1)}>-</button>
                {count}
                <button onClick={() => changeCount(1)}>+</button>
                <div>
                  <h4>颜色: {value.textColor}</h4>
                  <input type="color" value={value.textColor} onChange={e => {
                    value.changeColor(e.target.value)
                  }} />
                </div>
              </div>
            )}
          </Consumer>
        )}
      </ColorConsumer>
    )
  }
}

import React, { Component } from 'react'
import Third from './Third'
import { Consumer } from '../context/count'

export default class Second extends Component {
  render() {
    return (
      <Consumer>
        {value => (
          <div className='second'>
            <h2>这是second.js -- {value.count}</h2>
            <Third />
          </div>
        )}
      </Consumer>
    )
  }
}

import React, { Component } from 'react'
import First from './components/First'
import './App.css'
import { Provider } from './context/count'

class App extends Component {
  state = {
    count: 0
  }

  changeCount = (n) => {
    this.setState(state => {
      return {
        count: state.count + n
      }
    })
  }

  render() {
    return (
      // 嵌套传值
      // 所有包涵在 Provider组件内的后代组件都可以通过 Consumer 获取value
      <Provider value={{
        count: this.state.count,
        changeCount: this.changeCount
      }}>
        <div className="App">
          <h1>这是App.js</h1>
          <button onClick={() => this.changeCount(-1)}>-</button>
          {this.state.count}
          <button onClick={() => this.changeCount(1)}>+</button>
          <First />
        </div>
      </Provider>
    );
  }
}

export default App
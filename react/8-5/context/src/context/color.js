import { createContext } from 'react'

// 创建上下文对象
const ctx = createContext()
export const Provider = ctx.Provider
export const Consumer = ctx.Consumer
export default ctx

import './App.scss'
import { Button, Space, Table, Tag } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import React from 'react';

interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: Record<'color' | 'text', string>[];
}

const columns: ColumnsType<DataType> = [
  {
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: '地址',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: '标签',
    key: 'tags',
    dataIndex: 'tags',
    render: (tags: DataType['tags'], record) => {
      // 如果有dataIndex，第一个参数是每一行对应的dataIndex的属性，第二个参数是每一行的数据
      return tags.map(v => <Tag key={v.text} color={v.color}>{v.text}</Tag>)
    }
  },
  {
    title: '操作',
    key: 'action',
    render: (_: DataType, record) => {
      // 没有dataIndex 两个参数都是每一行的所有数据
      console.log(_.address)
      return (
        <Space>
          <Button type="primary">编辑</Button>
          <Button type="ghost" danger>删除</Button>
        </Space>
      )
    },
  },
];

const data: DataType[] = [
  {
    key: '1',
    name: '约翰',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: [{color: 'tomato', text: 'html'}, {color: 'green', text: 'vue'}],
  },
  {
    key: '2',
    name: '接客',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: [{color: 'skyblue', text: 'react'}],
  },
  {
    key: '3',
    name: '集美',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
    tags: [{color: 'yellowgreen', text: 'nodejs'}],
  },
];

const App: React.FC = () => {
  return (
    <Table columns={columns} dataSource={data}  />
  )
}

export default App;
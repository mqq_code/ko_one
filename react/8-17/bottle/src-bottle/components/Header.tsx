import { useSelector, useDispatch } from 'react-redux'
import { changeBottle } from '../store/action'

interface IProps {
  compare: () => void;
}

const Header: React.FC<IProps> = props => {

  const dispatch = useDispatch()
  const topList = useSelector((state: IStoreState) => state.topList)

  return (
    <header>
      <button disabled={topList.length < 2} onClick={props.compare}>比较</button>
      <ul>
        {topList.map(item =>
          <li key={item.title}>
            <img src={item.src} alt="" />
            <span onClick={() => {
              dispatch(changeBottle(item.title))
            }}>x</span>
          </li>
        )}
        {topList.length < 3 && <li className='empty'></li>}
      </ul>
    </header>
  )
}

export default Header
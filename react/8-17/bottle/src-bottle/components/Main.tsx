import { useSelector, useDispatch } from 'react-redux'
import { changeBottle } from '../store/action'

const Main = () => {
  const dispatch = useDispatch()
  const list = useSelector((state: IStoreState) => state.list)

  const change = (title: string) => {
    dispatch(changeBottle(title))
  }

  return (
    <main>
      {list.map(item =>
        <div onClick={() => change(item.title)} className={['bottle', item.checked ? 'active' : ''].join(' ')} key={item.title}>
          <img src={item.src} alt="" />
          <h3>{item.title}</h3>
          <p>{item.price}</p>
          <button>add to cart</button>
          <span>+</span>
        </div>
      )}
    </main>
  )
}

export default Main
import { useSelector } from 'react-redux'

interface IProps {
  close: () => void;
}

const Mark: React.FC<IProps> = props => {
  const topList = useSelector((state: IStoreState) => state.topList)

  return (
    <div className='mark'>
      <div className="close" onClick={props.close}>x</div>
      {topList.map(item =>
        <div key={item.title} className='item'>
          <img src={item.src} alt="" />
          <h3>{item.title}</h3>
          <p>{item.price}</p>
          <p>{item.alcohol}</p>
          <p>{item.region}</p>
          <p>{item.varietal}</p>
          <p>{item.year}</p>
        </div>
      )}
    </div>
  )
}

export default Mark
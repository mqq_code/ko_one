interface IBottle {
  alcohol: string;
  price: string;
  region: string;
  src: string;
  title: string;
  varietal: string;
  year: string;
  checked?: boolean;
}


interface IResponse {
  code: number;
  values: IBottle[]
}
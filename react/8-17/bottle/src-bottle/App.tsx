import './App.scss'
import Header from './components/Header'
import Main from './components/Main'
import Mark from './components/Mark'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { getBottleAction } from './store/action'

function App() {
  const dispatch = useDispatch()
  const topList = useSelector((state: IStoreState) => state.topList)
  const [show, setShow] = useState(false)

  useEffect(() => {
    dispatch(getBottleAction())
  }, [])

  return (
    <div className="App">
      {topList.length > 0 && <Header compare={() => setShow(true)} />}
      <Main />
      {show && <Mark close={() => setShow(false)} />}
    </div>
  );
}

export default App;

import { IAction, SET_LIST, CHANGE_BOTTLE } from '../action'
const initState: IStoreState = {
  topList: [],
  list: []
}

const reducer = (state = initState, action: IAction): IStoreState => {
  const newState: IStoreState = JSON.parse(JSON.stringify(state))
  switch (action.type) {
    case SET_LIST:
      return {...state, list: action.payload}
    case CHANGE_BOTTLE:
      newState.list.forEach(item => {
        if (item.title === action.payload) {
          if (newState.topList.length < 3 || item.checked) {
            item.checked = !item.checked
            if (item.checked) {
              newState.topList.push(item)
            } else {
              newState.topList = newState.topList.filter(v => v.title !== item.title)
            }
          }
        }
      })
      return newState
  }
  return state
}

export default reducer
import { AxiosResponse } from 'axios'
import { takeEvery, call, put } from 'redux-saga/effects'
import { getBottle } from '../../api'
import { GET_BOTTLE_LIST, setListAction } from '../action'

function* getList () {
  const res: AxiosResponse<IResponse> = yield call(getBottle)
  yield put(setListAction(res.data.values))
}


function* saga () {
  yield takeEvery(GET_BOTTLE_LIST, getList)
}

export default saga

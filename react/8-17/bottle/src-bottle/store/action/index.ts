export const GET_BOTTLE_LIST = 'GET_BOTTLE_LIST' as const
export const SET_LIST = 'SET_LIST' as const
export const CHANGE_BOTTLE = 'CHANGE_BOTTLE' as const


// 获取list
export const getBottleAction = () => {
  return {
    type: GET_BOTTLE_LIST
  }
}
// 修改仓库的list
export const setListAction = (payload: IBottle[]) => {
  return {
    type: SET_LIST,
    payload
  }
}
// 修改瓶子选中状态
export const changeBottle = (payload: string) => {
  return {
    type: CHANGE_BOTTLE,
    payload
  }
}


// ReturnType: 获取返回值类型
export type IAction = ReturnType<
typeof getBottleAction |
typeof setListAction |
typeof changeBottle>


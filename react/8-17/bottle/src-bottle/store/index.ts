import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import createSagaMiddleware from '@redux-saga/core'
import reducer from './reducer'
import saga from './saga'

// 创建saga中间件
const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware, logger))
// 运行saga
sagaMiddleware.run(saga)

export default store

import Mock from 'mockjs'
import data from './data/data.json'

// 模拟接口
Mock.mock('/api/list', 'post', () => {
  return {
    code: 0,
    values: data
  }
})

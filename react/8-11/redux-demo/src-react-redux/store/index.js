import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

const initState = {
  count: 100,
  title: '默认标题'
}

// 返回和修改数据
const reducer = (state, action) => {
  // state: 上一次reducer函数的返回值
  // action:  描述如何修改state的对象
  if (action.type === 'ADD') {
    return {...state, count: state.count + 1}
  } else if (action.type === 'SUB') {
    return {...state, count: state.count - 1}
  } else if (action.type === 'CHANGE_TITLE') {
    return {...state, title: action.payload}
  }
  return state
}

// applyMiddleware: 添加中间件，增强dispatch
// 创建仓库
const store = createStore(reducer, initState, applyMiddleware(logger))

export default store

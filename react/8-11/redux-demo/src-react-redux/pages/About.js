import { useState, useEffect } from 'react'
import { connect, useDispatch, useStore, useSelector } from 'react-redux'

const About = props => {
  // const store = useStore() // 获取store对象
  const count = useSelector(state => state.count)
  const title = useSelector(state => state.title)
  const dispatch = useDispatch()

  const add = () => {
    dispatch({ type: 'ADD' })
  }
  const sub = () => {
    dispatch({ type: 'SUB' })
  }
  const changeTitle = e => {
    dispatch({
      type: 'CHANGE_TITLE',
      payload: e.target.value
    })
  }

  return (
    <div>
      <h1>这是about页面</h1>
      <h2>{title}</h2>
      <p>修改标题: <input type="text" value={title} onChange={changeTitle} /></p>
      <hr />
      <div>
        <button onClick={sub}>-</button>
        {count}
        <button onClick={add}>+</button>
      </div>
    </div>
  )
}

export default About






// const About = props => {

//   const add = () => {
//     props.dispatch({ type: 'ADD' })
//   }
//   const sub = () => {
//     props.dispatch({ type: 'SUB' })
//   }
//   const changeTitle = e => {
//     props.dispatch({
//       type: 'CHANGE_TITLE',
//       payload: e.target.value
//     })
//   }

//   return (
//     <div>
//       <h1>这是about页面</h1>
//       <h2>{props.title}</h2>
//       <p>修改标题: <input type="text" value={props.title} onChange={changeTitle} /></p>
//       <hr />
//       <div>
//         <button onClick={sub}>-</button>
//         {props.count}
//         <button onClick={add}>+</button>
//       </div>
//     </div>
//   )
// }
// const mapState = state => {
//   return state
// }
// export default connect(mapState)(About)
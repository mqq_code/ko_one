import React, { Component } from 'react'
import { connect } from 'react-redux'

class Home extends Component {

  addCount = () => {
    this.props.dispatch({
      type: 'ADD'
    })
  }
  subCount = () => {
    this.props.dispatch({
      type: 'SUB'
    })
  }
  changeTitle = e => {
    this.props.dispatch({
      type: 'CHANGE_TITLE',
      payload: e.target.value
    })
  }

  render() {
    const { title, count } = this.props
    return (
      <div>
        <h1>这是首页页面</h1>
        <h2>{title}</h2>
        <input type="text" value={title} onChange={this.changeTitle} />
        <p>
          <button onClick={this.subCount}>-</button>
          {count}
          <button onClick={this.addCount}>+</button>
        </p>
      </div>
    )
  }
}

// connect: 高阶组件，连接仓库和组件，把仓库的数据传给组件的props
const mapState = state => {
  // state: 仓库数据
  // return的数据会添加到组件的props中
  return {
    a: 100,
    b: 200,
    count: state.count,
    title: state.title
  }
}
export default connect(mapState)(Home)
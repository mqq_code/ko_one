import React from 'react'
import './App.css'
import {
  HashRouter,
  Switch,
  Route,
  NavLink
} from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'

const App = () => {
  return (
    <HashRouter>
      <nav>
        <NavLink exact to="/">首页</NavLink>
        <NavLink to="/about">关于我们</NavLink>
      </nav>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
      </Switch>
    </HashRouter>
  )
}

export default App
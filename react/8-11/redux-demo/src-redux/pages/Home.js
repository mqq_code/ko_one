import React, { Component } from 'react'
import store from '../store'

// 获取仓库数据
// console.log(store)

class Home extends Component {
  state = {
    title: store.getState().title,
    count: store.getState().count
  }

  componentDidMount() {
    // 仓库的数据更新会执行此函数
    this.unsubscribe = store.subscribe(() => {
      this.setState({
        title: store.getState().title,
        count: store.getState().count
      })
    })
  }
  componentWillUnmount() {
    // 取消监听
    this.unsubscribe()
  }

  addCount = () => {
    // 触发reducer函数执行，改变仓库数据
    // store.dispatch(action)
    // action: 描述仓库如何修改数据的普通对象，必须有type属性
    store.dispatch({
      type: 'ADD'
    })
  }
  subCount = () => {
    // 触发reducer函数执行，改变仓库数据
    store.dispatch({
      type: 'SUB'
    })
  }
  changeTitle = e => {
    store.dispatch({
      type: 'CHANGE_TITLE',
      payload: e.target.value
    })
  }

  render() {
    return (
      <div>
        <h1>这是首页页面</h1>
        <h2>{this.state.title}</h2>
        <input type="text" value={this.state.title} onChange={this.changeTitle} />
        <p>
          <button onClick={this.subCount}>-</button>
          {this.state.count}
          <button onClick={this.addCount}>+</button>
        </p>
      </div>
    )
  }
}

export default Home
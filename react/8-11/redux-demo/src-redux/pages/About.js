import { useState, useEffect } from 'react'
import store from '../store'

const About = () => {
  const [title, setTitle] = useState(store.getState().title)
  const [count, setCount] = useState(store.getState().count)

  useEffect(() => {
    let unscribe = store.subscribe(() => {
      console.log('about更新')
      setTitle(store.getState().title)
      setCount(store.getState().count)
    })
    return () => {
      unscribe()
    }
  }, [])

  const add = () => {
    store.dispatch({ type: 'ADD' })
  }
  const sub = () => {
    store.dispatch({ type: 'SUB' })
  }
  const changeTitle = e => {
    store.dispatch({
      type: 'CHANGE_TITLE',
      payload: e.target.value
    })
  }

  return (
    <div>
      <h1>这是about页面</h1>
      <h2>{title}</h2>
      <p>修改标题: <input type="text" value={title} onChange={changeTitle} /></p>
      <hr />
      <div>
        <button onClick={sub}>-</button>
        {count}
        <button onClick={add}>+</button>
      </div>
    </div>
  )
}

export default About
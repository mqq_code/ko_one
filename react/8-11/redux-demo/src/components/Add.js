import { useState } from 'react'
import { Card, Input, Button, Col, Row } from 'antd'
import { PlusCircleOutlined } from '@ant-design/icons'
import { useDispatch } from 'react-redux'

const Add = () => {
  const [text, setText] = useState('')
  const dispatch = useDispatch()

  const add = () => {
    dispatch({
      type: 'ADD',
      text
    })
    setText('')
  }
  return (
    <Card
      title="Create a new todo"
      bordered={false}
    >
      <Row gutter={20}>
        <Col span={18}>
          <Input placeholder="todo内容" value={text} onChange={e => setText(e.target.value)} />
        </Col>
        <Col span={6}>
          <Button onClick={add} block icon={<PlusCircleOutlined />} type="primary">添加</Button>
        </Col>
      </Row>
    </Card>
  )
}

export default Add
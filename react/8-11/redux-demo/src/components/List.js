import React from 'react'
import { Card, Switch, Table, Tag, Button } from 'antd'
import { CheckOutlined, CloseOutlined } from '@ant-design/icons'
import { useSelector, useDispatch } from 'react-redux'

const List = () => {
  // 从仓库获取表格数据
  const list = useSelector(s => s)
  const dispatch = useDispatch()
  // 列的数据
  const columns = [
    {
      dataIndex: 'text',
      key: 'text',
      render: (text, record) => <Tag color={record.done ? 'cyan' : 'red'}>{text}</Tag> 
    },
    {
      dataIndex: 'done',
      key: 'done',
      width: 60,
      render: (checked, record) => (
        <Switch
          checkedChildren={<CheckOutlined />}
          unCheckedChildren={<CloseOutlined />}
          checked={checked}
          onChange={() => {
            dispatch({
              type: 'CHANGE_DONE',
              done: !checked,
              key: record.key
            })
          }}
        />
      )
    },
    {
      key: 'action',
      width: 60,
      render: (_) => (
        <Button
          type="primary"
          danger
          icon={<CloseOutlined />}
          onClick={() => {
            dispatch({
              type: 'DEL',
              key: _.key
            })
          }}
        ></Button>
      )
    }
  ];

  return (
    <Card
      title="Todo List"
      bordered={false}
      style={{ marginTop: '20px' }}
    >
      <Table
        showHeader={false}
        columns={columns}
        dataSource={list}
        pagination={{
          defaultPageSize: 3,
          pageSizeOptions: ['3', '5', '7'],
          showSizeChanger: true
        }}
      />
    </Card>
  )
}

export default List
import React from 'react'
import './App.css'
import Add from './components/Add'
import List from './components/List'

const App = () => {
  return (
    <div className='app'>
      <h1>todolist</h1>
      <Add />
      <List />
    </div>
  )
}

export default App
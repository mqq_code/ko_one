import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

const initState = []

const reducer = (state = initState, action) => {
  // 拷贝数据
  const newState = JSON.parse(JSON.stringify(state))

  if (action.type === 'ADD') {
    return [{
      text: action.text,
      done: false,
      key: Date.now()
    }, ...state]
  } else if (action.type === 'DEL') {
    return state.filter(v => v.key !== action.key)
  } else if (action.type === 'CHANGE_DONE') {
    newState.forEach(item => {
      if (item.key === action.key) {
        item.done = action.done
      }
    })
    return newState
  }
  return state
}

const store = createStore(reducer, applyMiddleware(logger))

export default store


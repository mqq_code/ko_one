import Mock from 'mockjs'

const data = Mock.mock({
  "list|20": [
    {
      "id": "@id",
      "name": "@cname",
      "checked": false
    }
  ]
})

// 模拟接口
Mock.mock('/list', 'get', data.list)


import { useState, useEffect } from 'react'
import style from './select.module.scss'
import Options from '../options/Options'

const Select = (props) => {
  const [visible, setVisible] = useState(false)
  const [list, setList] = useState([]) // 所有数据
  const [selectedList, setSelectedList] = useState([]) // 选中的数据

  const select = (id) => {
    let index = list.findIndex(v => v.id === id)
    list[index].checked = !list[index].checked
    setList([...list])
    if (list[index].checked) {
      setSelectedList([...selectedList, list[index]])
    } else {
      setSelectedList(selectedList.filter(v => v.id !== id))
    }
  }

  const show = e => {
    e.stopPropagation()
    setVisible(!visible)
  }

  useEffect(() => {
    const handleClick = () => setVisible(false)
    document.addEventListener('click', handleClick)
    return () => {
      document.removeEventListener('click', handleClick)
    }
  }, [])

  useEffect(() => {
    props.select(selectedList)
  }, [selectedList])

  useEffect(() => {
    setList([...props.list])
  }, [props.list])

  return (
    <div className={[style.select, visible ? style.active : ''].join(' ')} onClick={show}>
      {selectedList.length > 0 ?
        <div className={style.selected}>
          {selectedList.map(v =>
            <div className={style.item} key={v.id}>
              {v.name}
              <span onClick={e => {
                e.stopPropagation()
                select(v.id)
              }}>x</span>
            </div>
          )}
        </div>
      :
        <div className={style.placeholder}>请选择</div>
      }
      <div className={style.icon}>
        <svg t="1660008838415" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2332" width="12" height="12"><path d="M951.1626 819.412438 72.8374 819.412438 511.999488 204.586538Z" p-id="2333"></path></svg>
      </div>
      {visible && <Options list={list} select={select} />}
    </div>
  )
}

export default Select
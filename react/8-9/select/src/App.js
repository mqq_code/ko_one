import { useState, useEffect } from 'react'
import './App.scss'
import axios from 'axios'
import Select from './components/select/Select'

const App = () => {
  const [list, setList] = useState([])
  
  useEffect(() => {
    axios.get('/list').then(res => {
      console.log(res.data)
      setList(res.data)
    })
  }, [])

  const select = (selected) => {
    console.log('选中的数组', selected)
  }

  return (
    <div className='app'>
      <h3>xm-select下拉框多选带搜索实例</h3>
      <Select list={list} select={select} />
      aaaaaa
    </div>
  )
}

export default App
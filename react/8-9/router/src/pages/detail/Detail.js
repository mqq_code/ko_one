import { useEffect } from "react"
import { useParams } from 'react-router-dom'

const Detail = (props) => {

  const params = useParams()

  useEffect(() => {
    // console.log(props.location.search) // 获取search参数
    // console.log(props.location.state) // 获取state参数
    // console.log(props.match.params) // 获取动态路由传参数
    console.log(params)
  }, [])

  return (
    <div>Detail</div>
  )
}

export default Detail
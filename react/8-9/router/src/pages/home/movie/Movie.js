import React, { useEffect, useState } from 'react'
import axios from 'axios'
import MovieItem from './components/MovieItem'

const Movie = (props) => {
  const [list, setList] = useState([])

  useEffect(() => {
    axios.get('/hot').then(res => {
      setList(res.data)
    })
  }, [])

  return (
    <div style={{ height: '100%', overflow: 'auto' }}>
      {list.map(item => (
        <MovieItem key={item.filmId} {...item} />
      ))}
    </div>
  )
}

export default Movie
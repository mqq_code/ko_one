import React from 'react'
import { withRouter, useHistory, useParams, useLocation } from 'react-router-dom'

const MovieItem = (props) => {

  const h = useHistory() // 获取history对象
  const l = useLocation() // 获取location对象
  const p = useParams() // 获取动态路由参数
 

  const goDetail = (id) => {
    // console.log(id)
    // console.log(props)
    console.log(h)
    console.log(l)
    console.log(p)
    // 如果当前组件作为路由页面传给Route组件的component，props中会有路由信息

    // search传参，会在浏览器的地址栏显示参数
    // props.history.push(`/detail?id=${id}&a=100&b=200`)

    // 不会再浏览器的地址栏显示参数
    // props.history.push({
    //   pathname: '/detail',
    //   state: {
    //     id,
    //     a: 1000
    //   }
    // })

    // 动态路由传参数
    h.push(`/detail/${id}`)
  }

  return (
    <dl onClick={() => goDetail(props.filmId)}>
      <dt><img width='100' src={props.poster} alt="" /></dt>
      <dd>
        <h3>{props.name}</h3>
        <p>评分:{props.grade}</p>
        <p>{props.actors.map(v => v.name).join('/')}</p>
      </dd>
    </dl>
  )
}

export default MovieItem

// 把路由信息传给组件的props
// export default withRouter(MovieItem)
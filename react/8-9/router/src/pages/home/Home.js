import {
  Switch,
  Route,
  Redirect,
  Link,
  NavLink
} from 'react-router-dom'
import Movie from './movie/Movie'
import Cinema from './cinema/Cinema'
import Mine from './mine/Mine'
import style from './home.module.scss'

const Home = () => {
  return (
    <div className='home'>
      <main>
        <Switch>
          <Route path='/home/movie' component={Movie} />
          <Route path='/home/cinema' component={Cinema} />
          <Route path='/home/mine' component={Mine} />
          <Redirect exact from='/home' to='/home/movie' />
          <Redirect to='/404' />
        </Switch>
      </main>
      <footer>
        <NavLink activeStyle={{ fontSize: '50px' }} activeClassName={style.light} to='/home/movie'>电影</NavLink>
        <NavLink activeClassName={style.light} to='/home/cinema'>影院</NavLink>
        <NavLink activeClassName={style.light} to='/home/mine'>我的</NavLink>
      </footer>
    </div>
  )
}

export default Home
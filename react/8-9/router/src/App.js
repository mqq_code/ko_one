import React from 'react'
import './App.scss'
import {
  HashRouter, // hash模式路由根组件一个项目只用一次
  BrowserRouter, // history模式路由根组件一个项目只用一次
  Route, // 配置路由组件
  Switch, // 只显示匹配成功的第一个组件
  Redirect // 重定向
} from 'react-router-dom'
import Home from './pages/home/Home'
import Login from './pages/login/Login'
import Detail from './pages/detail/Detail'
import City from './pages/city/City'
import NotFound from './pages/404'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/home' render={() => {
          // 判断登陆
          // if (!token) return <Redirect to='/login' />
          return <Home />
        }} />
        <Route path='/login' component={Login} />
        <Route path='/detail/:id' component={Detail} />
        <Route path='/city' component={City} />
        <Route path='/404' component={NotFound} />
        <Redirect exact from='/' to='/home' />
        <Redirect to='/404' />
      </Switch>
    </BrowserRouter>
  )
}

export default App
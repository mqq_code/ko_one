import { useState, useEffect, useMemo, useCallback } from 'react'
import './App.scss'
import Header from './components/Header'
import Form from './components/Form'
import List from './components/List'

const navList = ['All', 'Active', 'Completed']

const App = () => {
  const [list, setList] = useState(JSON.parse(localStorage.getItem('list')) || [])
  const [curType, setType] = useState('All') // 当前类型

  const add = useCallback(text => {
    setList([{
      id: Date.now(),
      text,
      checked: false
    }, ...list])
  }, [list])

  const remove = id => {
    setList(list.filter(v => v.id !== id))
  }

  const change = id => {
    setList(list.map(v => {
      if (v.id === id) {
        return { ...v, checked: !v.checked }
      }
      return v
    }))
  }

  const clearCompleted = () => {
    setList(list.filter(v => !v.checked))
    setType('All')
  }

  // 当前选中的列表
  const curList = useMemo(() => {
    if (curType === 'All') return list
    const checked = curType === 'Completed'
    return list.filter(v => v.checked === checked)
  }, [list, curType])

  // 未选中的数量
  const itemLength = useMemo(() => {
    return list.filter(v => !v.checked).length
  }, [list])

  useEffect(() => {
    localStorage.setItem('list', JSON.stringify(list))
  }, [list])

  return (
    <div className="app">
      <main>
        <Header />
        <Form add={add} />
        {list.length > 0 ?
          <>
            <List list={curList} remove={remove} change={change} />
            <footer>
              <span>{itemLength} item left</span>
              <p>
                {navList.map((t, i) => {
                  if (i !== 0 && (list.length === itemLength || itemLength === 0)) return null
                  return <span
                    key={t}
                    className={curType === t ? 'active': ''}
                    onClick={() => setType(t)}
                  >{t}</span>
                })}
              </p>
              {list.length !== itemLength &&
                <p><span onClick={clearCompleted}>clear completed</span></p>
              }
            </footer>
          </>
        : 
          <div className="empty-todos">
            <span className="msg">Congrat, you have no more tasks to do</span>
          </div>
        }
      </main>
    </div>
  );
}

export default App;

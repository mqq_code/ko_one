import React, { useState, useCallback } from 'react'

const Form = props => {

  const [text, setText] = useState('')
  const submit = useCallback(() => {
    if (text.trim()) {
      props.add(text)
    }
    setText('')
  }, [text])

  return (
    <div className="form-field">
      <h1 className="title">~ Today I need to ~</h1>
      <div className="form-wrapper">
        <div className="form-input">
          <input
            placeholder="Add new todo..."
            value={text}
            onChange={e => setText(e.target.value)}
            onKeyDown={e => {
              if (e.key === 'Enter') {
                submit()
              }
            }}
          />
        </div>
        <button className="submit-btn" onClick={submit}>
          <span>Submit</span>
        </button>
      </div>
    </div>
  )
}

export default Form
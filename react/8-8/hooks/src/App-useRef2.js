import { useState, useEffect, useRef, useImperativeHandle, forwardRef, Component  } from 'react'
import './App.css'

class Test extends Component {
  state = {
    num: 0
  }
  count = 0
  render() {
    console.log('更新组件')
    return (
      <div>
        {/* {this.state.num} <button onClick={() => {
          this.setState({
            num: this.state.num + 1
          })
        }}>+</button> */}
        {this.count} <button onClick={() => {
          this.count ++
          console.log(this.count)
        }}>+</button>
      </div>
    )
  }
}

function App () {
  const count = useRef(0)
  const [num, setNum] = useState(0)
  const changeNum = n => setNum(num + n)
  const changeCount = n => {
    count.current += n
    console.log(count)
  }
  console.log('组件更新了')
  return (
    <div className="App">
      <div className='num'>
        <b>num: </b>
        <button onClick={() => changeNum(-1)}>-</button>
        {num}
        <button onClick={() => changeNum(1)}>+</button>
      </div>
      <div className='count'>
        <b>count: </b>
        <button onClick={() => changeCount(1)}>-</button>
        {count.current}
        <button onClick={() => changeCount(1)}>+</button>
      </div>
    </div>
  );
}

export default Test;

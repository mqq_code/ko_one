import { useState, useEffect  } from 'react'
import axios from 'axios'
import './App.css'

const Timer = () => {
  const [count, setCount] = useState(10)
  const [disabled, setDisabled] = useState(false)

  const countDown = () => {
    setDisabled(true)
  }

  useEffect(() => {
    if (disabled) {
      let timer = setInterval(() => {
        setCount(c => {
          if (c - 1 <= 0) {
            clearInterval(timer) 
            return 10
          }
          return c - 1
        })
      }, 1000)

      return () => {
        clearInterval(timer)
      }
    }

  }, [disabled])

  useEffect(() => {
    if (count === 10) {
      setDisabled(false)
    }
  }, [count])


  return <div>
    <button disabled={disabled} onClick={countDown}>{disabled ? `${count}s后重试` : '获取验证码'}</button>
  </div>
}



function App () {
  const [count, setCount] = useState(0)
  const [age, setAge] = useState('10')
  const [banners, serBanners] = useState([])
  const [flag, setFlag] = useState(true)

  // 处理组件副作用：定时器、调用接口、操作dom...
  // 可以实现和类组件的声明周期类似的功能
  // useEffect(函数, [依赖项])

  // useEffect(函数, []) 类似 componentDidMount，进入页面只执行一次，可以掉接口，定时器，操作dom
  useEffect(() => {
    axios.get('https://zyxcl-music-api.vercel.app/banner').then(res => {
      serBanners(res.data.banners)
    })
    console.log(document.querySelector('h1'))
  }, [])

  // useEffect(函数) 类似componentDidUpdate，每次更新组件都会执行，可以获取最新的数据和dom元素
  // useEffect(() => {
  //   console.log('组件更新了', count)
  //   console.log('组件更新了', banners)
  // })

  // useEffect(函数, [count]) count改变会执行函数，其他变量改变不会执行
  useEffect(() => {
    console.log('count更新了', count)
    console.log('age更新了', age)
    console.log('banners更新了', banners)
  }, [count, banners])

  

  return (
    <div className="App">
      <h1>开始学习hook</h1>
      {flag && <Timer />}
      <button onClick={() => setFlag(!flag)}>显示隐藏定时器</button>
      <hr />
      <input type="text" value={age} onChange={e => setAge(e.target.value)} /> 年龄: {age}
      <div>
        <button onClick={() => setCount(count - 1)}>-</button>
        {count}
        <button onClick={() => setCount(count + 1)}>+</button>
      </div>
      <ul>
        {banners.map(item =>
          <li key={item.targetId}><img src={item.imageUrl} alt="" /></li>
        )}
      </ul>
    </div>
  );
}

export default App;

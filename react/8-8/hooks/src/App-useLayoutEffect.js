import React, { useState, useMemo, useCallback, useEffect, useLayoutEffect } from 'react'
import './App.css'

const App = () => {
  const [title, setTitle] = useState('App')

  // dom元素更新完成，渲染到页面之前执行
  useLayoutEffect(() => {
    let n = 0
    while(n < 1000) {
      n ++
      console.log(n)
    }
    setTitle('aaaaaa')
  }, [title])

  // dom元素更新完成，渲染到页面之后执行
  // useEffect(() => {
  //   let n = 0
  //   while(n < 1000) {
  //     n ++
  //     console.log(n)
  //   }
  //   setTitle('aaaaaa')
  // }, [title])

  return (
    <div className='app'>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
    </div>
  )
}

export default App
import React from 'react'
import Third from './Third'

const Second = () => {
  return (
    <div>
      <h2>Second</h2>
      <Third />
    </div>
  )
}

export default Second
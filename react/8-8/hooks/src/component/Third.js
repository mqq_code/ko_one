import React, { useContext } from 'react'
import countCtx from '../context/count'

const Third = () => {
  // 获取 countCtx 的数据
  const value = useContext(countCtx)

  return (
    <div>
      <h2>Third</h2>
      {value.count}
      <button onClick={() => value.changeCount(1)}>+</button>
    </div>
  )
}

export default Third
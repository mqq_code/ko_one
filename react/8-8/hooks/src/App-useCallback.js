import React, { useState, useMemo, useCallback } from 'react'
import './App.css'

let arr = []

const App = () => {
  console.log('组件更新了')
  const [title, setTitle] = useState('App')
  const [count, setCount] = useState(0)

  const add = useCallback(() => {
    setCount(count + 1)
  }, [count])

  // useCallback: 返回一个缓存的函数
  const sub = useCallback(() => {
    setCount(count - 1)
  }, [count])

  // 可以使用useMemo实现useCallback的功能
  const changeTitle = useMemo(() => {
    return () => {
      setTitle(title + Math.random().toString().slice(2, 3))
    }
  }, [title])

  // 验证useCallback是否生效
  if (arr.length === 0) {
    arr.push(changeTitle)
  } else if (changeTitle !== arr[arr.length - 1]) {
    arr.push(changeTitle)
  }
  console.log(arr)

  return (
    <div className='app'>
      <button onClick={changeTitle}>修改title</button>
      <h1>{title}</h1>
      <button onClick={sub}>-</button>
      {count}
      <button onClick={add}>+</button>
    </div>
  )
}

export default App
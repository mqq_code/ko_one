import React, { useState, useRef, useCallback, useEffect, useLayoutEffect } from 'react'
import './App.css'

const arr = []

const App = () => {
  // const [obj, setObj] = useState({ age: 10 })
  const [count, setCount] = useState(0)
  const n = useRef(0) // 如果要定义跟组件渲染无关的变量可以使用useRef
  // useRef中的数据的每次更新不会创建新数据，始终指向同意个地址
  // const obj = useRef({ age: 10 })

  // if (arr.length === 0) {
  //   arr.push(obj.current)
  // } else if (obj.current !== arr[arr.length - 1]) {
  //   arr.push(obj)
  // }

  useEffect(() => {
    if (n.current === 1) {
      console.log('count更新了')
    }
    n.current ++
    console.log('useEffect执行了')
  }, [count])

  return (
    <div className='app'>
      {/* <div>
        <p>'obj.age': {obj.current.age}</p>
        <button onClick={() => {
          obj.current.age += 1
        }}>+</button>
      </div> */}
      <div>{count} <button onClick={() => setCount(count + 1)}>+</button></div>
    </div>
  )
}

export default App
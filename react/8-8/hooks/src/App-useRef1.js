import { useState, useEffect, useRef, useImperativeHandle, forwardRef, Component  } from 'react'
import './App.css'

let Test = (props, ref) => {
  const inp = useRef(null)
  const [num, setNum] = useState(0)
  const changeNum = (n) => {
    setNum(num + n)
  }

  // 给父组件传过来的ref添加值
  useImperativeHandle(ref, () => {
    // 把此函数的返回值添加给父组件传过来的ref
    return {
      focus: () => {
        inp.current.focus()
      },
      bbb: (n) => {
        changeNum(n)
      }
    }
  }, [num])
  return (
    <div>
      <b>测试组件 {num}</b>
      <input ref={inp} type="text" />
    </div>
  )
}
// 转发ref，把父组件传过来的ref传给子组件的第二个参数
Test = forwardRef(Test)

// class Test extends Component {
//   handleClick () {

//   }
//   render() {
//     return (
//       <div>
//         <b>测试组件</b>
//         <input type="text" />
//       </div>
//     )
//   }
// }

function App () {
  const title = useRef(null)
  const testRef = useRef(123)
  useEffect(() => {
    console.log(title.current)
  }, [])
  const getTest = () => {
    console.log(testRef)
    // 使用ref调用子组件的方法
    testRef.current.focus()
  }

  return (
    <div className="App">
      <h1 ref={title}>开始学习hook</h1>
      <Test ref={testRef} />
      <button onClick={getTest}>获取test实例</button>
      <button onClick={() => {
        testRef.current.bbb(1)
      }}>修改Test组件的num</button>
    </div>
  );
}

export default App;

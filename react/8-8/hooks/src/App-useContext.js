import React, { useState } from 'react'
import First from './component/First'
import './App.css'
import { Provider } from './context/count'

const App = () => {
  const [count, setCount] = useState(0)
  const changeCount = n => {
    setCount(count + n)
  }
  return (
    <Provider value={{
      count,
      changeCount
    }}>
      <div className='app'>
        <h1>App</h1>
        <button onClick={() => changeCount(-1)}>-</button>
        {count}
        <button onClick={() => changeCount(1)}>+</button>
        <First />
      </div>
    </Provider>
  )
}

export default App
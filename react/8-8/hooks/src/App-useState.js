import { useState  } from 'react'
import './App.css'

function App () {

  const [count, setCount] = useState(10)
  const [arr, setArr] = useState([])

  const add = () => {
    setArr([...arr, arr.length])
  }

  const changeCount = n => {
    // 修改数据的函数是异步执行的，传入一个值会替换之前的数据
    setCount(count + n)
    // setCount(count + n)
    // setCount(count + n)

    // 传入函数可以获取最新的数据
    // setCount(c => {
    //   console.log(c)
    //   return c + n
    // })
    // setCount(c => {
    //   console.log(c)
    //   return c + n
    // })
    // setCount(c => {
    //   console.log(c)
    //   return c + n
    // })
  }

  return (
    <div className="App">
      <h1>开始学习hook</h1>
      <button onClick={add}>给arr添加数据</button>
      <ul>
        {arr.map(item => <li key={item}>{item}</li>)}
      </ul>
      <div>
        <button onClick={() => changeCount(-1)}>-</button>
        {count}
        <button onClick={() => changeCount(1)}>+</button>
      </div>
    </div>
  );
}

export default App;

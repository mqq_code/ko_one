import React, { useState, useMemo } from 'react'
import './App.css'

const arr = [
  {
    id: 0,
    title: '苹果',
    price: 10,
    count: 0
  },
  {
    id: 1,
    title: '香蕉',
    price: 11,
    count: 0
  },
  {
    id: 2,
    title: '梨',
    price: 12,
    count: 0
  },
  {
    id: 3,
    title: '水蜜桃',
    price: 13,
    count: 0
  }
]


const App = () => {
  const [title, setTitle] = useState('App')
  const [list, setList] = useState(arr)

  const changeCount = (id, n) => {
    const newlist = [...list]
    const i = newlist.findIndex(v => v.id === id)
    newlist[i].count += n
    setList(newlist)
    // setList(list.map(item => {
    //   if (id === item.id) {
    //     return {
    //       ...item,
    //       count: item.count + n
    //     }
    //   }
    //   return item
    // }))
  }

  // 类似vue中的计算属性
  // 返回一个缓存的值，当依赖项改变了执行函数计算返回值，依赖项没有改变从缓存中读取数据
  const total = useMemo(() => {
    return list.reduce((prev, val) => {
      return prev + val.price * val.count
    }, 0)
  }, [list])

  return (
    <div className='app'>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <h1>{title}</h1>
      <ul>
        {list.map((item, index) => (
          <li key={item.id}>
            <h3>{item.title}</h3>
            <p>价格: ¥{item.price}</p>
            <p>
              {item.count > 0 &&
                <>
                  <button onClick={() => changeCount(item.id, -1)}>-</button>
                  {item.count}
                </>
              }
              <button onClick={() => changeCount(item.id, 1)}>+</button>
            </p>
          </li>
        ))}
      </ul>
      <footer>总价：¥{total}</footer>
    </div>
  )
}

export default App
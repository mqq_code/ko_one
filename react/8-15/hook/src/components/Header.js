import { useEffect, useState } from 'react'
import useSize from '../hooks/useSize'

const Header = () => {
  const [count, setCount] = useState(0)
  const size = useSize()

  return (
    <header>
      <button onClick={() => setCount(count + 1)}>+</button> {count}
      <h1>这是header组件 -- {size}</h1>
      {size === 'big' &&
        <nav>
          <span>导航1</span>
          <span>导航2</span>
          <span>导航3</span>
          <span>导航4</span>
        </nav>
      }
    </header>
  )
}

export default Header
import { useEffect, useState } from 'react'
import axios from 'axios'
import useSize from '../hooks/useSize'

const Content = () => {
  const [banner, setBanner] = useState([])
  const size = useSize()

  useEffect(() => {
    axios.get('https://zyxcl-music-api.vercel.app/banner').then(res => {
      setBanner(res.data.banners)
    })
  }, [])

  return (
    <main>
      <h2>content内容 -- {size}</h2>
      <p>{JSON.stringify(banner)}</p>
      <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
        <li>10</li>
      </ul>
    </main>
  )
}

export default Content
import { search } from '../api'
import useLoading from '../hooks/useLoading'

const Search = () => {
  
  const [isLoading, res] = useLoading(search, '张学友')

  return (
    <div>
      {isLoading ?
        <div>加载中</div>
      :
        <ul>
          {res && res.result.songs.map(item => <li key={item.id}>{item.name}</li>)}
        </ul>
      }
    </div>
  )
}

export default Search
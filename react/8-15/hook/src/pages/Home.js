import { Link } from 'react-router-dom'
import { getBanner } from '../api'
import useLoading from '../hooks/useLoading'

const Home = () => {
  const [isLoading, res] = useLoading(getBanner)

  return (
    <div className="home">
      <button><Link to="/search">搜索</Link></button>
      {isLoading ?
        <h1>loading...</h1>
      :
        <div className="banner">
          {res && res.banners.map(item => <img key={item.targetId} src={item.imageUrl} />)}
        </div>
      }
    </div>
  )
}

export default Home
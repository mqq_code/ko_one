import { useState, useEffect } from 'react'

export default function useLoading (request, params) {

  const [res, setRes] = useState(null)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    let flag = false
    setLoading(true)
    request(params).then(res => {
      if (flag) {
        setLoading(false)
      }
      flag = true
      setRes(res.data)
    })
    setTimeout(() => {
      if (flag) {
        setLoading(false)
      }
      flag = true
    }, 1000)
  }, [])

  return [loading, res]
}
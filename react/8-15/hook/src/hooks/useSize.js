import { useEffect, useState } from 'react'

// 自定义 hook 就是名字以 use 开头的函数
function useSize () {
  const [size, setSize] = useState('')
  useEffect(() => {
    const resize = () => {
      const w = document.documentElement.clientWidth
      if (w >= 800) {
        setSize('big')
      } else if (w < 800 && w >= 400) {
        setSize('middle')
      } else {
        setSize('mini')
      }
    }
    resize()
    window.addEventListener('resize', resize)

    console.log('1111')
  
    return () => {
      // 组件销毁清楚事件监听
      window.removeEventListener('resize', resize)
    }
  }, [])

  return size
}

export default useSize
import axios from 'axios'

export const getBanner = () => {
  return axios.post('https://zyxcl-music-api.vercel.app/banner?time=' + Date.now())
}

export const search = (keywords) => {
  return axios.get('https://zyxcl-music-api.vercel.app/search', {
    params: {
      keywords
    }
  })
}
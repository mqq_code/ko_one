import React from 'react'
// import Header from './components/Header'
// import Content from './components/Content'
import './App.scss'
import { getBanner } from './api'
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'
import Home from './pages/Home'
import Search from './pages/Search'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/search' component={Search} />
        <Route exact path='/' component={Home} />
      </Switch>
      {/* <Header />
      <Content /> */}
    </BrowserRouter>
  )
}

export default App
import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

const Address = () => {
  const address = useSelector(state => state.address)

  return (
    <div className='address'>
      <ul>
        {address.map(item =>
          <li key={item.id}>
            <h3>{item.name}</h3>
            <p>手机号：{item.tel}</p>
            <p>地址: {item.city}-{item.street}-{item.detail}</p>
          </li>
        )}
      </ul>
      <button>
        <Link to="/add">添加地址</Link>
      </button>
    </div>
  )
}

export default Address
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addAddressCreater } from '../store/actions'

const Add = (props) => {
  const dispatch = useDispatch()

  const [form, setForm] = useState({
    name: '',
    tel: '',
    city: '',
    street: '',
    detail: ''
  })

  const changeForm = (key, val) => {
    setForm({
      ...form,
      [key]: val
    })
  }

  const add = () => {
    // 发起action
    dispatch(addAddressCreater({ ...form, id: Date.now()}))
    props.history.push('/address')
  }

  return (
    <div>
      <div>姓名：<input type="text" value={form.name} onChange={e => changeForm('name', e.target.value)} /></div>
      <div>手机号：<input type="text" value={form.tel} onChange={e => changeForm('tel', e.target.value)} /></div>
      <div>城市：<input type="text" value={form.city} onChange={e => changeForm('city', e.target.value)} /></div>
      <div>街道：<input type="text" value={form.street} onChange={e => changeForm('street', e.target.value)} /></div>
      <div>详细地址：<input type="text" value={form.detail} onChange={e => changeForm('detail', e.target.value)} /></div>
      <div>
        <button onClick={add}>添加</button>
      </div>
    </div>
  )
}

export default Add
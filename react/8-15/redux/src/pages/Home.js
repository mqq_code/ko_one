import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getGoods } from '../store/actions'

const Home = props => {

  const dispatch = useDispatch()
  const goods = useSelector(state => state.goods.goods)

  useEffect(() => {
    dispatch(getGoods)
  }, [])

  return (
    <div>
      {goods.map(item =>
        <div key={item.wareId} onClick={() => {
          props.history.push(`/detail/${item.wareId}`)
        }}>
          <img src={item.wareImg} alt="" />
          <h3>{item.wareName}</h3>
        </div>
      )}
    </div>
  )
}

export default Home
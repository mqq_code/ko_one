import React, { useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getGoods } from '../store/actions'

const Detail = (props) => {
  const dispatch = useDispatch()
  const goods = useSelector(s => s.goods.goods)

  const info = useMemo(() => {
    return goods.find(v => v.wareId === props.match.params.id) || {}
  }, [goods])

  useEffect(() => {
    if (goods.length === 0) {
      // 使用redux-thunk中间件后，dispatch可以接受一个函数作为参数，会自动调用此函数，并且把dispatch传给此函数的参数
      dispatch(getGoods)
    }
  }, [])

  return (
    <div>
      <h2>{info.wareName}</h2>
      <img src={info.wareImg} alt="" />
    </div>
  )
}

export default Detail
import React from 'react'
import {
  Switch,
  Route,
  Redirect,
  NavLink
} from 'react-router-dom'
import Home from './pages/Home'
import Cart from './pages/Cart'
import Detail from './pages/Detail'
import Address from './pages/Address'
import Add from './pages/Add'
import './App.scss'

const App = () => {
  return (
    <div className='app'>
      <main>
        <Switch>
          <Route path="/home" component={Home} />
          <Route path="/cart" component={Cart} />
          <Route path="/detail/:id" component={Detail} />
          <Route path="/address" component={Address} />
          <Route path="/add" component={Add} />
          <Redirect to="/home" />
        </Switch>
      </main>
      <footer>
        <NavLink to="/home">商品列表</NavLink>
        <NavLink to="/cart">购物车</NavLink>
        <NavLink to="/address">地址管理</NavLink>
      </footer>
    </div>
  )
}

export default App
import axios from 'axios'

// 管理所有的 actiontype 
export const ADD_ADDRESS = 'ADD_ADDRESS' // 添加地址
export const GET_GOODS = 'GET_GOODS' // 获取商品数据



// 管理所有的 action
// 添加收获地址
export const addAddressCreater = (payload) => {
  return {
    type: ADD_ADDRESS,
    payload
  }
}

// 获取商品数据
export const getGoodsCreater = (payload) => {
  return {
    type: GET_GOODS,
    payload
  }
}

// 获取商品列表数据
export const getGoods = dispatch => {
  axios({
    url: 'https://searchgw.dmall.com/mp/search/wareSearch',
    method: 'post',
    data: {
      param: {
        venderId: 1,
        storeId: 12527,
        businessCode: 1,
        from: 1,
        categoryType: 1,
        categoryId: '11340',
        categoryLevel: 1
      }
    },
    // 调用接口之前会先执行此数组中的所有函数
    transformRequest: [
      function (data) {
        // data: 接口传给后端的数据
        // 把data转换格式城 => 'param={venderId: 1,storeId: 12527,businessCode: 1,from: 1,categoryType: 1,pageNum: 1,pageSize: 20,categoryId: "11340",categoryLevel: 1}&a=100&b=200'
        let str = ''
        Object.keys(data).forEach((key) => {
          str += `${key}=${JSON.stringify(data[key])}&`
        })
        str = str.slice(0, -1)
        return str
      }
    ],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  }).then(res => {
    dispatch({
      type: GET_GOODS,
      payload: res.data.data.wareList
    })
  })
}
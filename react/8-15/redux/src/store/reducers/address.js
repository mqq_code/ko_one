import { ADD_ADDRESS } from '../actions'

const initState = {
  address: []
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_ADDRESS:
      return {...state, address: [...state.address, action.payload]}
  }
  return state
}

export default reducer
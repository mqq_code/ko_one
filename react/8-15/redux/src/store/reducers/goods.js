import { GET_GOODS } from '../actions'


const initState = {
  goods: [],
  cartList: []
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case GET_GOODS:
      return {...state, goods: action.payload}
  }
  return state
}

export default reducer
import { createStore, applyMiddleware, combineReducers } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk' // redux中间件，让dispatch可以发送函数，可以处理异步
import address from './reducers/address'
import goods from './reducers/goods'

const reducer = combineReducers({
  address,
  goods
})

// redux-saga: 原理Generator

// redux中间件：扩展dispatch功能
const store = createStore(reducer, applyMiddleware(thunk, logger))

export default store
const initState = {
  banners: [],
  songs: []
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case 'SET_BANNER':
      return { ...state, banners: action.payload}
    case 'SET_SONGS':
      return { ...state, songs: action.payload}
  }
  return state
}

export default reducer
import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import mySaga from './saga'
import reducer from './reducer'

// 创建saga中间件
const sagaMiddleware = createSagaMiddleware()

// redux中间件：扩展dispatch功能
const store = createStore(reducer, applyMiddleware(sagaMiddleware, logger))

// 执行saga
sagaMiddleware.run(mySaga)

export default store
import { takeEvery, put, call } from 'redux-saga/effects'
import { getBanner, search } from '../../api'


function* banner() {
  // 处理异步，调用接口
  const res = yield call(getBanner)

  // 使用 put 发起 dispatch
  yield put({
    type: 'SET_BANNER',
    payload: res.data.banners
  })
}

function* searchSongs({ payload }) {
  const res = yield call(search, payload)
  yield put({
    type: 'SET_SONGS',
    payload: res.data.result.songs
  })
}


function* saga() {
  // 当 action.type === 'GET_BANNER' 执行banner函数
  yield takeEvery('GET_BANNER', banner)
  yield takeEvery('SEARCH_SONGS', searchSongs)
}

export default saga
import { useEffect, useRef } from 'react'
import './App.scss'
import { useSelector, useDispatch } from 'react-redux'

const App = () => {
  const dispatch = useDispatch()
  const banners = useSelector(state => state.banners)
  const songs = useSelector(state => state.songs)

  useEffect(() => {
    // 发起action
    dispatch({
      type: 'GET_BANNER'
    })
  }, [])


  const input = useRef(null)
  const submit = () => {
    dispatch({
      type: 'SEARCH_SONGS',
      payload: input.current.value
    })
  }

  return (
    <div className='app'>
      <div>
        <h3>轮播图</h3>
        {JSON.stringify(banners)}
      </div>
      <hr />
      <input type="text" ref={input} />
      <button onClick={submit}>搜索</button>
      <div>
        <h3>搜索列表</h3>
        {JSON.stringify(songs)}
      </div>
    </div>
  )
}

export default App
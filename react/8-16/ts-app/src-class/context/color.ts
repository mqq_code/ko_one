import { createContext } from 'react'

interface IColor {
  color?: string;
  changeColor?: (c: string) => void
}

const color = createContext<IColor>({})

export const Provider = color.Provider
export const Consumer = color.Consumer

export default color


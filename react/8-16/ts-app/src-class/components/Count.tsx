import React, { Component } from 'react'
import { Consumer } from '../context/color'

interface IProps {
  count: number;
  title: string;
  changeCount: (n: number) => void;
}

class Count extends Component<IProps> {
  render() {
    const { count, changeCount, title } = this.props
    return (
      <Consumer>
        {value => {
          return (
            <div>
              <h3>{title}</h3>
              <button onClick={() => changeCount(-1)}>-</button>
              {count.toFixed(2)}
              <button onClick={() => changeCount(1)}>+</button>
              <hr />
              <input type="color" value={value.color} onChange={e => {
                value.changeColor && value.changeColor(e.target.value)
              }} />
            </div>
          )
        }}
      </Consumer>
      
    )
  }
}


export default Count
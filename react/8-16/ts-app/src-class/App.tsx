import './App.scss';
import React, { Component, createRef } from 'react'
import Count from './components/Count'
import { Provider } from './context/color'

interface IState {
  count: number;
  title: string;
  age: number;
  color: string;
}
interface IProps {
  a?: number;
  b?: string;
}

class App extends Component<IProps, IState> {

  state = {
    count: 0,
    title: '默认标题',
    age: 10,
    color: '#333'
  }

  title = createRef<HTMLHeadingElement>()

  changeCount = (n: number) => {
    this.setState({
      count: this.state.count + n
    })
  }

  changeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      title: e.target.value
    })
  }

  submit = (e: React.MouseEvent) => {
    console.log(this.title.current?.innerHTML)
  }

  changeColor = (color: string) => {
    this.setState({ color })
  }

  render() {
    const { title, count, color } = this.state
    return (
      <Provider value={{
        color,
        changeColor: this.changeColor
      }}>
        <div style={{ color }}>
          <h2 ref={this.title}>{title}</h2>
          <input type="text" value={title} onChange={this.changeTitle}/>
          <button onClick={this.submit}>提交</button>
          <hr />
          <Count count={count} changeCount={this.changeCount} title={title} />
        </div>
      </Provider>
    )
  }
}

export default App

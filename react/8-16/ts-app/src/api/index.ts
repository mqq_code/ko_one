import axios from 'axios'


export const getBanner = () => {
  return axios.get<IBannerResponse>('https://zyxcl-music-api.vercel.app/banner')
}

export const search = (keywords: string) => {
  return axios.post('https://zyxcl-music-api.vercel.app/search', {
    keywords
  })
}



// *.d.ts 中如果没有使用 import 或者 export,此文件中定义的所有类型都是全局类型，其他文件中可以直接使用，无需引入

interface IBannerResponseItem {
  titleColor: string;
  typeTitle: string;
  imageUrl: string;
  targetId: number;
  adDispatchJson: string | null;
}

// 轮播图返回值类型
interface IBannerResponse {
  code: number;
  banners: IBannerResponseItem[]
}

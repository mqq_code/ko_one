import React from 'react'
import { withRouter, RouteComponentProps } from 'react-router-dom'

type IProps = {
  imageUrl: string;
  targetId: number;
  titleColor: string;
  typeTitle: string;
}

const BannerItem: React.FC<IProps & RouteComponentProps> = (props) => {

  const goDetail = () => {
    props.history.push(`/detail/${props.targetId}`)
  }

  return (
    <div  onClick={goDetail}>
      <h3 style={{ color: props.titleColor }}>{props.typeTitle}</h3>
      <img src={props.imageUrl} />
    </div>
  )
}

export default withRouter(BannerItem)
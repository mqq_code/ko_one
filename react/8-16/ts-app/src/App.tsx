import Home from './pages/Home'
import Detail from './pages/Detail'
import Search from './pages/Search'
import {
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import './App.scss'

const App = () => {
  return (
    <Switch>
      <Route path="/home" component={Home} />
      <Route path="/detail/:id" component={Detail} />
      <Route path="/search" component={Search} />
      <Redirect to="/home" />
    </Switch>
  )
}

export default App
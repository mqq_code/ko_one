import React from 'react'
import { RouteComponentProps, useParams } from 'react-router-dom'

type IParams = {
  id: string;
}

const Detail: React.FC<RouteComponentProps<IParams>> = props => {
  const params = useParams<IParams>()
  
  console.log(params.id)
  console.log(props.match.params.id)

  return (
    <div>Detail</div>
  )
}

export default Detail
import { useEffect, useState } from 'react'
import { getBanner } from '../api'
import { RouteComponentProps } from 'react-router-dom'
import BannerItem from '../components/BannerItem'

const Home: React.FC<RouteComponentProps> = props => {

  const [banner, setBanner] = useState<IBannerResponseItem[]>([])

  useEffect(() => {
    getBanner().then(res => {
      setBanner(res.data.banners)
    })
  }, [])

  return (
    <div>
      <h2>首页</h2>
      {banner.map(item =>
        <BannerItem
          key={item.targetId}
          imageUrl={item.imageUrl}
          targetId={item.targetId}
          titleColor={item.titleColor}
          typeTitle={item.typeTitle}
        />
      )}
    </div>
  )
}

export default Home
import React, { useContext } from 'react'
import colorCtx from '../context/color'


type IProps = {
  title: string;
}

const Color: React.FC<IProps> = (props) => {
  const { color, changeColor } = useContext(colorCtx)

  return (
    <div>
      <h3>{props.title}</h3>
      <input type="color" value={color} onChange={e => changeColor!(e.target.value)} />
    </div>
  )
}

export default Color
import './App.scss';
import React, { useState, useRef, useMemo } from 'react'
import { Provider } from './context/color'
import Color from './components/Color'

interface IProps {
  a?: string;
}
interface IListItem {
  id: number;
  title: string;
  checked: boolean;
}

const App: React.FC<IProps> = (props) => {
  const h2 = useRef<HTMLHeadingElement>(null)
  const [color, setColor] = useState<string>('#333')
  const [title, setTitle] = useState<string>('默认标题')
  const [list, setList] = useState<IListItem[]>([])

  const submit = () => {
    setList([{
      id: Date.now(),
      title,
      checked: false
    }, ...list])
    setTitle('')
  }

  const done = (id: number) => {
    setList(list.map(item => {
      if (item.id === id) {
        return {...item, checked: !item.checked}
      }
      return item
    }))
  }

  const doneList = useMemo(() => {
    return list.filter(v => v.checked)
  }, [list])

  return (
    <Provider value={{
      color,
      changeColor: setColor
    }}>
      <div style={{ color }}>
        {/* <h2 ref={h2}>{title}</h2> */}
        <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
        <button onClick={submit}>获取dom</button>
        <ul>
          {list.map(item =>
            <li key={item.id}>
              {item.checked ?
                <s>{item.title}</s>
              :
                <span>{item.title}</span>
              }
              <button onClick={() => done(item.id)}>完成</button>
            </li>
          )}
        </ul>
        <hr />
        <Color title={title} />
      </div>
    </Provider>
    
  )
}

export default App


import React, { Component } from 'react'
import './app.scss'

class App extends Component {
  state = {
    params: {
      name: '王小明',
      sex: 0,
      address: '广州',
      age: '18',
      remark: '备注备注',
      married: true
    }
  }
  change = (key, val) => {
    this.setState({
      params: {
        ...this.state.params,
        [key]: val
      }
    })
  }
  
  render() {
    const { params } = this.state
    return (
      <div>
        {/* 受控组件：表单的值和state的数据绑定,受控组件必须有change事件修改数据 */}
        <p>姓名：<input type="text" value={params.name} onChange={(e) => this.change('name', e.target.value)} /></p>
        <p>性别：
          <input type="radio" name="sex" defaultChecked={params.sex === 1} value={1} onChange={(e) => this.change('sex', e.target.value)} /> 男
          <input type="radio" name="sex" defaultChecked={params.sex === 0} value={0} onChange={(e) => this.change('sex', e.target.value)} /> 女
        </p>
        <p>年龄：<input type="number" value={params.age} onChange={(e) => this.change('age', e.target.value)}  /></p>
        <p>地址：
          <select value={params.address} onChange={(e) => this.change('address', e.target.value)}>
            <option value="">请选择</option>
            <option value="beijing">北京</option>
            <option value="shanghai">上海</option>
            <option value="guangzhou">广州</option>
          </select>
        </p>
        <p>婚否：<input type="checkbox" checked={params.married} onChange={(e) => this.change('married', e.target.checked)} /></p>
        <p>备注：<textarea value={params.remark} onChange={(e) => this.change('remark', e.target.value)}></textarea></p>
        <button onClick={this.submit}>提交</button>
        <button onClick={this.reset}>重置</button>
      </div>
    )
  }

  submit = () => {
    console.log(this.state.params)
  }
  reset = () => {
    this.setState({
      params: {
        name: '',
        sex: ''
      }
    })
  }
}

export default App
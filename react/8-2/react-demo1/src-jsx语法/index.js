import React from 'react'; // 创建react元素，组件等核心功能
import ReactDOM from 'react-dom/client'; // dom操作相关的内容

// const arr = [1, 2, 3, 4, 5, 6]
// const arr = [
//   <li>1</li>,
//   <li>2</li>,
//   <li>3</li>,
//   <li>4</li>
// ]
const arr1 = [
  {
    name: '王小明',
    age: 10
  },
  {
    name: '小李子',
    age: 12
  }
]

const app = <div>
  <ul>
    {/* {arr.map(item => <li>{item}</li>)} */}
    {arr1.map(item => <li key={item.name}>{item.name} -- {item.age}</li>)}
  </ul>
</div>

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(app)

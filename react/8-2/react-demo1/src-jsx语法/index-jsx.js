import React from 'react'; // 创建react元素，组件等核心功能
import ReactDOM from 'react-dom/client'; // dom操作相关的内容

// let h1 = React.createElement('h1', {
//   className: 'title'
// }, ['开始学习react'])

// jsx: js + xml
// 在js中可以写标签，通过babel转译成 React.createElement
// 在jsx的 {} 中可以解析js表达式，不可以写语句

const getTitle = () => '我今天要开始学习react'
const num = Math.floor(Math.random() * 100)

const renderMain = n => {
  if (n > 60) {
    return <main>
      <p>数字：{100}</p>
      <p>boolean：{false}</p>
      <p>null：{null}</p>
      <p>undefined: {undefined}</p>
      <p>symbol：{Symbol('1')}</p>
      <p>array: {['a', 'b', 'c']}</p>
      {/* <p>object: { {name: '小明'} }</p> jsx中不可以解析对象 */}
    </main>
  }
  return null
}


let app = <div className='app'>
  <header>
    <h1>{getTitle()}<b>!</b></h1>
    <h2 style={{
      color: num > 60 ? 'green' : 'red'
    }}>考试分数: {num} -- {num > 60 ? '及格' : '不及格'}</h2>
  </header>
  {renderMain(num)}
  {/* {num > 60 ?
    <main>
      <p>数字：{100}</p>
      <p>boolean：{false}</p>
      <p>null：{null}</p>
      <p>undefined: {undefined}</p>
      <p>symbol：{Symbol('1')}</p>
      <p>array: {['a', 'b', 'c']}</p>
      <p>object: { {name: '小明'} }</p> jsx中不可以解析对象
    </main>
  : null} */}
  <footer>地步</footer>
</div>


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(app)

import React, { Component } from 'react'
import './app.scss'

class App extends Component {
  // 组件的数据，类似vue中的data
  state = {
    list: [
      {
        name: '周杰伦',
        child: ['告白气球', '青花瓷', '最伟大的作品']
      },
      {
        name: '陈奕迅',
        child: ['浮夸', '富士山下', '孤勇者']
      },
      {
        name: '刀郎',
        child: ['情人', '2002的第一场雪', '冲动的惩罚']
      }
    ],
    activeIndex: 0 // 高亮
  }

  abc = 1000

  changeTab = (index) => {
    // 修改react中的数据想要触发页面更新，必须调用this.setState()
    // setState在react函数中时异步执行的
    this.setState({
      activeIndex: index
    }, () => {
      // 页面更新完成的回调函数，可以获取最新的数据
      console.log(this.state.activeIndex)
    })
  }

  render() {
    const { list, activeIndex } = this.state
    return (
      <div className='app'>
        <div className="left">
          {list.map((item, index) => <p
            className={activeIndex === index ? 'active' : ''}
            key={item.name}
            onClick={() => this.changeTab(index)}
          >
            {item.name}
          </p>)}
        </div>
        <div className="right">
          <h2>{list[activeIndex].name} - {this.abc}</h2>
          <ul>
            {list[activeIndex].child.map(item => <li key={item}>{item}</li>)}
          </ul>
        </div>
      </div>
    )
  }
}

export default App
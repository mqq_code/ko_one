import React, { Component } from 'react'

class App extends Component {
  state = {
    count: 0
  }

  add = () => {
    // 把返回的对象和之前的state合并
    this.setState(newState => {
      // newState: 最新的state
      return {
        count: newState.count + 1
      }
    }, () => {
      console.log('页面更新完成')
    })

    // this.setState({ count: this.state.count + 1 })
    // this.setState({ count: this.state.count + 1 })
    // this.setState({ count: this.state.count + 1 })
    // this.setState({ count: this.state.count + 1 })
    // this.setState({ count: this.state.count + 1 })

    // setState在react函数中时异步执行的
    // 把传入的对象和之前的state进行合并
    // this.setState({
    //   count: this.state.count + 1
    // }, () => {
    //   // 页面更新完成的回调函数，可以获取最新的数据
    //   console.log(this.state.count)
    // })
  }

  render() {
    const { count } = this.state
    return (
      <div className='app'>
        <button>-</button>
        { count }
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}

export default App
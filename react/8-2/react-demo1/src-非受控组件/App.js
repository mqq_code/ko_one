import React, { Component } from 'react'
import './app.scss'

class App extends Component {
  state = {
  }
  // 创建ref对象
  nameRef = React.createRef()
  menRef = React.createRef()
  womenRef = React.createRef()

  submit = () => {
    // 使用非受控组件获取表单数据，（操作dom）
    const params = {
      name: this.nameRef.current.value,
      sex: this.menRef.current.checked ? this.menRef.current.value : this.womenRef.current.value
    }
    console.log(params)

    console.log(this.wrap)
  }
  render() {
    return (
      <div ref={el => this.wrap = el}>
        <p>姓名：<input type="text" ref={this.nameRef} /></p>
        <p>性别：
          <input type="radio" ref={this.menRef} name="sex" value={1}/> 男
          <input type="radio" ref={this.womenRef} name="sex" value={0}/> 女
        </p>
        <button onClick={this.submit}>提交</button>
      </div>
    )
  }
}

export default App
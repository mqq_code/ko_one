import React, { Component } from 'react'
import Header from './components/Header'
import './app.scss'

class App extends Component {
  state = {
    title: '默认标题'
  }

  changeTitle = (a) => {
    console.log(a)
    this.setState({
      title: '标题' + Math.random().toFixed(2)
    })
  }

  render() {
    return (
      <div>
        <Header title={this.state.title} changeTitle={this.changeTitle} />
        <h1>{this.state.title}</h1>
        <button onClick={this.changeTitle}>修改标题</button>
      </div>
    )
  }
}

export default App
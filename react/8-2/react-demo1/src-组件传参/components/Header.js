import React, { Component } from 'react'

class Header extends Component {
  render() {
    // 接收父组件的数据
    console.log(this.props)
    return (
      <header>
        {this.props.title}
        <button onClick={() => this.props.changeTitle(11111)}>修改title</button>
      </header>
    )
  }
}

export default Header




// import React from 'react'

// const Header = (props) => {
//   // 接收父组件传过来的数据
//   console.log(props)
//   return (
//     <header>{props.title}</header>
//   )
// }

// export default Header
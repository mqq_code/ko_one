import React from 'react'
import logo from './assets/logo.png'
import './app.scss'

const App = () => {
  return (
    <div className='app'>
      <ul>
        <li><img src={logo} alt="" /></li>
      </ul>
    </div>
  )
}

export default App




// import React, { Component } from 'react'
// import logo from './assets/logo.png'
// import './app.scss'
// class App extends Component {
//   render() {
//     return (
//       // 由于 class 和 for 是js中的关键字，所以排版时把 class 改成 className，for 改成 htmlFor
//       <div className='app' style={{ color: 'red' }}>
//         <h1>标题</h1>
//         <ul>
//           <li>1</li>
//           <li>2</li>
//         </ul>
//         {/* <label htmlFor=""></label> */}
//         {/* 布局时使用本地图片必须先 import 引入 */}
//         <img src={logo} alt="" />
//       </div>
//     )
//   }
// }

// export default App
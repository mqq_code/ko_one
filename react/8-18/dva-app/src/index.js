import dva from 'dva';
import './index.css';
import router from './router';
import example from './models/example';
import user from './models/user';

// 1. Initialize
const app = dva();

// 2. Plugins
// app.use({});

// 3. Model
app.model(example);
app.model(user);

// 4. Router
app.router(router);

// 5. Start
app.start('#root');

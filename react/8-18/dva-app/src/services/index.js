
export const getlist = () => {
  return fetch('/api/list', {
    method: 'post'
  }).then(res => res.json())
}

export const getTest = () => {
  return fetch('/api/test', {
    method: 'get'
  }).then(res => res.json())
}
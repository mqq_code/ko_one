import React from 'react'
import style from './home.css'
import { NavLink } from 'dva/router'

const Home = (props) => {
  return (
    <div className={style.home}>
      <main>
        {props.children}
      </main>
      <footer>
        <NavLink to="/home">电影</NavLink>
        <NavLink to="/home/cinema">影院</NavLink>
        <NavLink to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home
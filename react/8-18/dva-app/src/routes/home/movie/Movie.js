import React, { useEffect } from 'react'
import { connect } from 'dva'


const Movie = (props) => {
  useEffect(() => {
    props.dispatch({
      type: 'example/setArr'
    })
  }, [])
  return (
    <div>
      {JSON.stringify(props.list)}
      <hr />
      <button onClick={() => {
        props.dispatch({
          type: 'example/add',
          payload: -1
        })
      }}>-</button>
      {props.count}
      <button onClick={() => {
        props.dispatch({
          type: 'example/add',
          payload: 1
        })
      }}>+</button>
    </div>
  )
}

const mapSate = state => {
  return {
    count: state.example.count,
    list: state.example.arr,
    username: state.user.username
  }
}

export default connect(mapSate)(Movie)
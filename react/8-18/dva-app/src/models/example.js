import { getlist } from '../services'

export default {

  // 命名空间
  namespace: 'example', 

  // 定义数据
  state: {
    count: 0,
    arr: []
  },

  // 处理异步
  effects: {
    *setArr(action, { call, put }) {
      const res = yield call(getlist)
      yield put({
        type: 'changeArr',
        payload: res.value
      })
    }
  },

  // 修改state数据
  reducers: {
    add(state, { payload }) {
      return { ...state, count: state.count + payload }
    },
    changeArr(state, { payload }) {
      return { ...state, arr: payload }
    }
  },

  subscriptions: {
  },

};

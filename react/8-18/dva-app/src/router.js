import React from 'react';
import { Router, Route, Switch, Redirect } from 'dva/router';
import Home from './routes/home/Home';
import Detail from './routes/detail/Detail';
import Login from './routes/login/Login';
import Movie from './routes/home/movie/Movie';
import Cinema from './routes/home/cinema/Cinema';
import Mine from './routes/home/mine/Mine';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/home" render={routeInfo => {
          return <Home {...routeInfo}>
            <Switch>
              <Route path="/home" exact component={Movie} />
              <Route path="/home/cinema" component={Cinema} />
              <Route path="/home/mine" component={Mine} />
            </Switch>
          </Home>
        }} />
        <Route path="/detail/:id" exact component={Detail} />
        <Route path="/login" exact component={Login} />
        <Redirect to="/home" />
      </Switch>
    </Router>
  );
}

export default RouterConfig;

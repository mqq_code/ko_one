import { defineConfig } from 'umi';

export default defineConfig({
  routes: [
    {
      exact: true,
      path: '/about/:id',
      component: '@/pages/about',
      title: '关于我们',
      // 高阶组件，跳转 about 页面时会把 component 放到 weappers 组件中的children里
      wrappers: ['@/wrappers/withAuth'],
    },
    {
      exact: true,
      path: '/login',
      component: '@/pages/login',
      title: '登陆',
    },
    {
      path: '/',
      component: '@/pages/index',
      title: '首页',
      routes: [
        {
          exact: true,
          path: '/movie',
          component: '@/pages/index/movie',
          title: '电影'
        },
        {
          exact: true,
          path: '/cinema',
          component: '@/pages/index/cinema',
          wrappers: ['@/wrappers/withAuth'], 
          title: '影院'
        },
        {
          exact: true,
          path: '/',
          redirect: '/movie'
        },
      ],
    },
  ]
}).routes


// @ts-nocheck
import React from 'react';
import { ApplyPluginsType } from '/Users/zhaoyaxiang/Desktop/终极一班/ko_one/react/8-18/umi-app/node_modules/umi/node_modules/@umijs/runtime';
import * as umiExports from './umiExports';
import { plugin } from './plugin';

export function getRoutes() {
  const routes = [
  {
    "path": "/about/:id",
    "exact": true,
    "component": require('@/pages/about/[id].tsx').default
  },
  {
    "path": "/login",
    "exact": true,
    "component": require('@/pages/login/index.tsx').default
  },
  {
    "path": "/",
    "routes": [
      {
        "path": "/index/cinema",
        "exact": true,
        "component": require('@/pages/index/cinema/index.tsx').default
      },
      {
        "path": "/index/movie",
        "exact": true,
        "component": require('@/pages/index/movie/index.tsx').default
      }
    ],
    "component": require('@/pages/index/_layout.tsx').default
  }
];

  // allow user to extend routes
  plugin.applyPlugins({
    key: 'patchRoutes',
    type: ApplyPluginsType.event,
    args: { routes },
  });

  return routes;
}

import styles from './index.less';
import { Link } from 'umi'

export default function IndexPage(props: any) {
  console.log(props)
  return (
    <div>
      <h1 className={styles.title}>Page index</h1>
      <Link to="/about/123">跳转about</Link> <br />
      <Link to="/">跳转movie</Link> <br />
      <Link to="/cinema">跳转cinema</Link>
      <hr />
      <main>
        {props.children}
      </main>
    </div>
  );
}

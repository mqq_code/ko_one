import { Redirect } from 'umi'


export default (props: any) => {
  let token = localStorage.getItem('token')
  if (!token) {
    return <Redirect to="/login" />
  }
  return props.children
}
import { createContext } from "react";

const count = createContext()

export const Provider = count.Provider
export const Consumer = count.Consumer
export default count

import axios from 'axios'

let host = '/mqq'
if (process.env.NODE_ENV === 'production') {
  host = 'http://ustbhuangyi.com'
}

export const getSeller = () => {
  return axios.get(`${host}/sell/api/seller`)
}

export const getGoods = () => {
  return axios.get(`${host}/sell/api/goods`)
}

export const getRatings = () => {
  return axios.get(`${host}/sell/api/ratings`)
}



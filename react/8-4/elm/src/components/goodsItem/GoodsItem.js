import React from 'react'
import style from './goodsItem.module.scss'
import { Consumer } from '../../context/count'

const GoodsItem = ({ info }) => {
  return (
    <Consumer>
      {value => (
        <dl className={style.foods}>
          <dt><img src={info.image} alt="" /></dt>
          <dd>
            <h4>{info.name}</h4>
            <div className={style.desc}>{info.description}</div>
            <p>
              <span>月售{info.sellCount}份</span>
              <span>好评率{info.rating}%</span>
            </p>
            <div className={style.price}>
              <b>¥{info.price}</b>
              {info.oldPrice && <s>¥{info.oldPrice}</s>}
            </div>
            <div className={style.btns}>
              {info.count > 0 &&
                <>
                  <button onClick={() => value.changeCount(info.name, -1)}>-</button>
                  <span>{info.count}</span>
                </>
              }
              <button onClick={() => value.changeCount(info.name, 1)}>+</button>
            </div>
          </dd>
        </dl>
      )}
    </Consumer>
    
  )
}

export default GoodsItem
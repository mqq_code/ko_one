import React from 'react'
// css module
import style from './header.module.scss'

const Header = ({ info, handleClick }) => {
  return (
    <header className={style.header} onClick={handleClick}>
      <div className={style.info}>
        <img src={info.avatar} alt="" />
        <div className={style['info-con']}>
          <h3>{info.name}</h3>
          <p>{info.description}/{info.deliveryTime}分钟送达</p>
          <p>{info.supports && info.supports[0].description}</p>
          <span>{info.supports && info.supports.length}个</span>
        </div>
      </div>
      <div className={style.notice}>{info.bulletin}</div>
      <div className={style.background}>
        <img src={info.avatar} alt="" />
      </div>
    </header>
  )
}

export default Header
import React, { Component } from 'react'
import style from './comment.module.scss'
import { getRatings } from '../../api'
import moment from 'moment'
import Star from '../star/Star'

class Comment extends Component {
  state = {
    commentList: [],
    curIndex: 0,
    navList: [
      {
        text: '全部',
        type: -1,
        list: []
      },
      {
        text: '满意',
        type: 0,
        list: []
      },
      {
        text: '不满意',
        type: 1,
        list: []
      }
    ]
  }
  filterComment = (list, type) => {
    if (type === -1) {
      return [...list]
    } else {
      return list.filter(v => v.rateType === type)
    }
  }
  componentDidMount() {
    getRatings().then(res => {
      const commentList = res.data.data
      this.setState({
        commentList,
        navList: this.state.navList.map(item => {
          return {
            ...item,
            list: this.filterComment(commentList, item.type)
          }
        })
      })
    })
  }
  changeIndex = i => {
    this.setState({
      curIndex: i
    })
  }
  render() {
    const { navList, curIndex } = this.state
    console.log(navList)
    return (
      <div className={style.comment}>
        <div style={{ display: 'flex', padding: '15px' }}>
          <span>服务态度</span>
          <div style={{ width: '120px' }}>
            <Star score={4} />
          </div>
        </div>
        <div className={style.commentNav}>
          {navList.map((item, i) =>
            <span
              className={[item.type === 1 ? style.gray : '', curIndex === i ? style.active : ''].join(' ')}
              key={item.type}
              onClick={() => this.changeIndex(i)}
            >{item.text} {item.list.length}</span>
          )}
        </div>
        <div className={style.list}>
          {navList[curIndex].list.map(item =>
            <dl key={item.rateTime}>
              <dt><img src={item.avatar} alt="" /></dt>
              <dd>
                <p>{item.username}</p>
                <div className={style.star}>
                  <div className={style['star-wrap']}>
                    <Star score={item.score} size={12} />
                  </div>
                  {item.deliveryTime}
                </div>
                <p>{item.text}</p>
                {item.recommend.length > 0 &&
                  <p className={style.tags}>{item.recommend.map(item => <span key={item}>{item}</span>)}</p>
                }
                <p className={style.rateTime}>{moment(item.rateTime).format('yyyy-MM-DD HH:mm')}</p>
              </dd>
            </dl>
          )}
        </div>
      </div>
    )
  }
}

export default Comment
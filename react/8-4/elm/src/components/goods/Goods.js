import React, { Component } from 'react'
import style from './goods.module.scss'
import GoodsItem from '../goodsItem/GoodsItem'
import Footer from '../footer/Footer'
class Goods extends Component {
  state = {
    activeIndex: 0
  }

  changeIndex = index => {
    this.setState({
      activeIndex: index
    })
  }
  itemClass = index => {
    return [style['nav-item'], this.state.activeIndex === index ? style.active : ''].join(' ')
  }

  render() {
    const { activeIndex } = this.state
    const { goodsList } = this.props
    return (
      <div className={style.goods}>
        <div className={style['goods-top']}>
          <div className={style.left}>
            {goodsList.map((item, index) =>
              <div
                className={this.itemClass(index)}
                key={item.name}
                onClick={() => this.changeIndex(index)}
              >
                <p>
                  {item.type !== -1 && <span className={`${style.icon} type${item.type}`}></span>}
                  <span>{item.name}</span>
                </p>
              </div>
            )}
          </div>
          <div className={style.right}>
            <h3>{goodsList[activeIndex] && goodsList[activeIndex].name}</h3>
            {goodsList[activeIndex] && goodsList[activeIndex].foods.map(item =>
              <GoodsItem key={item.name} info={item} />
            )}
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

export default Goods
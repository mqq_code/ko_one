import React, { Component } from 'react'
import style from './storeInfo.module.scss'

class StoreInfo extends Component {

  star = new Array(5).fill(0)

  render() {
    console.log(this.props.info)
    const { info, close } = this.props
    return (
      <div className={style.store}>
        <h3>{info.name}</h3>
        <div className={style.star}>
          {this.star.map((v, i) =>
            <span className={info.score >= i + 1 ? style.onstar : ''} key={i}></span>
          )}
        </div>
        <div className={style.title}>
          <div className={style.line}></div>
          <b>优惠信息</b>
          <div className={style.line}></div>
        </div>
        <ul>
          {info.supports && info.supports.map(item =>
            <li key={item.type} className={`type${item.type}`}>{item.description}</li>
          )}
        </ul>
        <div className={style.title}>
          <div className={style.line}></div>
          <b>商家公告</b>
          <div className={style.line}></div>
        </div>
        <div className={style.desc}>{info.bulletin}</div>
        <div className={style.close} onClick={close}>
          <svg t="1659580483523" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2260" width="48" height="48"><path d="M544.448 499.2l284.576-284.576a32 32 0 0 0-45.248-45.248L499.2 453.952 214.624 169.376a32 32 0 0 0-45.248 45.248l284.576 284.576-284.576 284.576a32 32 0 0 0 45.248 45.248l284.576-284.576 284.576 284.576a31.904 31.904 0 0 0 45.248 0 32 32 0 0 0 0-45.248L544.448 499.2z" p-id="2261" fill="#ffffff"></path></svg>
        </div>
      </div>
    )
  }
}
export default StoreInfo

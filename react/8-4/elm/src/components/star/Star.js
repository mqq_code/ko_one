import React from 'react'
import style from './star.module.scss'

const arr = new Array(5).fill(0)

const Star = ({ score, size = 20 }) => {
  return (
    <div className={style.star}>
      {arr.map((v, i) =>
        <span style={{
          width: size,
          height: size
        }} key={i} className={score >= i + 1 ? style.onstar : ''}></span>
      )}
    </div>
  )
}

export default Star
import React, { Component } from 'react'
import style from './cartList.module.scss'
import { Consumer } from '../../context/count'

class CartList extends Component {
  render() {
    const { clearCart } = this.props
    return (
      <Consumer>
        {({ cartList, changeCount,changeCart }) => {
          return (
            <div className={style.cart} onClick={changeCart}>
              <div className={style.con} onClick={e => e.stopPropagation()}>
                <div className={style.title}>
                  <b>购物车</b>
                  <span onClick={clearCart}>清空</span>
                </div>
                <ul>
                  {cartList.map(item =>
                    <li key={item.name}>
                      <h4>{item.name}</h4>
                      <p>¥{item.price}</p>
                      <div className={style.btn}>
                        <button onClick={() => changeCount(item.name, -1)}>-</button>
                        <span>{item.count}</span>
                        <button onClick={() => changeCount(item.name, 1)}>+</button>
                      </div>
                    </li>
                  )}
                </ul>
              </div>
            </div>
          )
        }}
      </Consumer>
    )
    
  }
}
export default CartList

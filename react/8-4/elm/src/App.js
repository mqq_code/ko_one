import React, { Component } from 'react'
import './App.scss'
import Header from './components/header/Header'
import StoreInfo from './components/storeInfo/StoreInfo'
import Goods from './components/goods/Goods'
import Comment from './components/comment/Comment'
import Store from './components/store/Store'
import CartList from './components/cartList/CartList'
import { getSeller, getGoods } from './api'
import { Provider } from './context/count'

export default class App extends Component {
  
  state = {
    info: {},
    showStoreInfo: false,
    showCart: false,
    activeIndex: 0,
    goodsList: [], // 商品列表
    cartList: [] // 购物车列表
  }

  changeIndex = index => {
    this.setState({
      activeIndex: index
    })
  }

  changeCart = () => {
    this.setState({
      showCart: !this.state.showCart
    })
  }

  // 切换商家信息的显示隐藏
  changeShowStore = () => {
    this.setState({
      showStoreInfo: !this.state.showStoreInfo
    })
  }
  // 修改商品数量
  changeCount = (name, n) => {
    // 拷贝数据
    const goodsList = [...this.state.goodsList]
    const cartList = [...this.state.cartList]
    // 存当前点击的食物
    let curFood = {}
    goodsList.forEach(item => {
      item.foods.forEach(food => {
        if (food.name === name) {
          if (food.count) {
            food.count += n
          } else {
            food.count = 1
          }
          curFood = food
        }
      })
    })
    // 根据点击的名称去购物车列表中查找所在的下标
    let index = cartList.findIndex(v => v.name === name)
    if (index > -1) {
      if (curFood.count <= 0) {
        cartList.splice(index, 1)
      } else {
        cartList.splice(index, 1, curFood)
      }
    } else {
      cartList.push(curFood)
    }
    this.setState({
      goodsList,
      cartList
    })
    // 如果购物车数据清空，并且弹窗正在显示就关闭弹窗
    if (cartList.length === 0 && this.state.showCart) {
      this.changeCart()
    }
  }
  // 清空购物车
  clearCart = () => {
    const goodsList = [...this.state.goodsList]
    goodsList.forEach(item => {
      item.foods.forEach(food => {
        if (food.count > 0) {
          food.count = 0
        }
      })
    })
    this.setState({
      goodsList,
      cartList: [],
      showCart: false
    })
  }
  
  componentDidMount() {
    getSeller().then(res => {
      this.setState({
        info: res.data.data
      })
    })
    getGoods().then(res => {
      this.setState({
        goodsList: res.data.data
      })
    })
  }

  navlist = ['商品', '评论', '商家']

  renderMain = () => {
    const { activeIndex } = this.state
    if (activeIndex === 0) {
      return (
        <Goods goodsList={this.state.goodsList} />
      )
    } else if (activeIndex === 1) {
      return <Comment />
    } else {
      return <Store />
    }
  }

  render() {
    const { info, showStoreInfo, activeIndex, showCart, cartList } = this.state
    return (
      <Provider value={{
        changeCount: this.changeCount,
        changeCart: this.changeCart,
        cartList
      }}>
        <div className='app'>
          <Header info={info} handleClick={this.changeShowStore} />
          <nav>
            {this.navlist.map((v, i) =>
              <span
                className={activeIndex === i ? 'nav-active' : ''}
                key={v}
                onClick={() => this.changeIndex(i)}
              >{v}</span>
            )}
            <div className="line" style={{
              transform: `translateX(${activeIndex * 100}%)`
            }}></div>
          </nav>
          <main>{this.renderMain()}</main>
          {showStoreInfo && <StoreInfo info={info} close={this.changeShowStore} />}
          {showCart &&
            <CartList clearCart={this.clearCart} />
          }
        </div>
      </Provider>
    )
  }
}

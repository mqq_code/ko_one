import { useState, useEffect, useRef  } from 'react'
import './App.css'



function App () {
 
  const title = useRef(null)

  useEffect(() => {
    console.log(title.current)
  }, [])

  return (
    <div className="App">
      <h1 ref={title}>开始学习hook</h1>

    </div>
  );
}

export default App;

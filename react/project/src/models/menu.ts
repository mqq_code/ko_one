import { Effect, Reducer, Subscription } from 'umi';
import { getMenulist } from '../services/api'

export interface IMenuItem {
  id: number;
  key: string;
  path: string;
  icon: string;
  title: string;
  children?: IMenuItem[]
}

export interface IMenuState {
  list: IMenuItem[]
}

export interface menuModelType {
  namespace: 'menu';
  state: IMenuState;
  effects: {
    getMenu: Effect;
  };
  reducers: {
    setMenu: Reducer<IMenuState>;
  };
}

const IndexModel: menuModelType = {
  namespace: 'menu',
  state: {
    list: []
  },
  effects: {
    *getMenu(action, { call, put }) {
      try {
        const res = yield call(getMenulist)
        yield put({
          type: 'setMenu',
          payload: res.values.list
        })
      } catch (e) {
        console.log(e)
      }
    }
  },
  reducers: {
    setMenu(state, action) {
      return {...state, list: action.payload}
    } 
  }
};

export default IndexModel;
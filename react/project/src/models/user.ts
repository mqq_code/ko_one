import { Effect, Reducer, Subscription } from 'umi';
import { getUserInfo } from '../services/api'

export interface IUserState {
  username: string;
  account: string;
  email: string;
  menulist: string[];
  role: string[];
}

export interface UserModelType {
  namespace: 'user';
  state: IUserState;
  effects: {
    getUser: Effect;
  };
  reducers: {
    setUserInfo: Reducer<IUserState>;
  };
}

const IndexModel: UserModelType = {
  namespace: 'user',
  state: {
    username: '',
    account: '',
    email: '',
    menulist: [],
    role: []
  },
  effects: {
    *getUser(action, { call, put }) {
      console.log('getUser')
      try {
        const res = yield call(getUserInfo)
        yield put({
          type: 'setUserInfo',
          payload: res.values
        })
      } catch(e) {
        console.log(e)
      }
    }
  },
  reducers: {
    setUserInfo(state, action) {
      return {...state, ...action.payload}
    } 
  }
};

export default IndexModel;
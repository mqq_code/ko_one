import { Spin } from 'antd';

const Loading: React.FC = () => {
  return (
    <div style={{ height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      <Spin tip="加载中..." />
    </div>
  )
}

export default Loading
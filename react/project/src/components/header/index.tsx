import { Layout, Avatar, Dropdown, Menu, message } from 'antd';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import { useSelector, IUserState, Link, history } from 'umi'
import { logout } from '@/services/api'
import Bread from '../bread'

interface IProps {
  collapsed: boolean;
  setCollapsed: (collapsed: boolean) => void;
}

const index: React.FC<IProps> = (props) => {

  const userInfo = useSelector<{ user: IUserState }, IUserState>(state => state.user)
  const menu = (
    <Menu
      onClick={(value) => {
        if (value.key === 'logout') {
          logout().then(res => {
            localStorage.removeItem('mqq-token')
            history.push('/login')
            message.success('退出登陆')
          })
        }
      }}
      items={[
        {
          key: '0',
          label: <Link to="/setting/setuser">个人设置</Link>,
        },
        {
          key: '1',
          label: <Link to="/setting/userinfo">个人中心</Link>,
        },
        {
          key: 'logout',
          label: '退出登陆',
        }
      ]}
    />
  )

  return (
    <Layout.Header style={{ padding: '0 20px', background: '#fff', display: 'flex', alignItems: 'center' }}>
      <span style={{ fontSize: 20 }} onClick={() => props.setCollapsed(!props.collapsed)}>
        {props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </span>
      <div style={{ flex: 1, marginLeft: 20 }}>
        <Bread />
      </div>
      <Dropdown overlay={menu}>
        <div>
          <Avatar style={{ color: '#f56a00', backgroundColor: '#fde3cf', marginRight: '5px' }}>{userInfo.account[0]}</Avatar>
          {userInfo.account}
        </div>
      </Dropdown>
    </Layout.Header>
  )
}

export default index
import { useState, useEffect, forwardRef, useImperativeHandle } from 'react'
import { message, Table } from 'antd'
import type { ColumnsType, TablePaginationConfig, TableProps } from 'antd/es/table';

interface IProps {
  columns: ColumnsType<any>;
  getListApi: (params?: any) => Promise<any>;
  rowKey?: string;
  pagination?: false | TablePaginationConfig | undefined;
  pagesize?: number;
  page?: number;
}

export interface ITableIns {
  getList: () => void;
  setQuery: (params: any) => void;
}
const index: React.ForwardRefRenderFunction<ITableIns | undefined, IProps & TableProps<any>> = ({
  columns,
  getListApi,
  pagination = {},
  rowKey = 'id',
  pagesize = 2,
  page = 1,
  ...tableProps
}, ref) => {
  const [list, setList] = useState<any[]>([])
  const [loading, setLoading] = useState(false)
  const [total, setTotal] = useState(0)
  const [query, setQuery] = useState({ page, pagesize })

  // 获取列表数据
  const getList = () => {
    setLoading(true)
    getListApi(query).then(res => {
      if (res.code === 200) {
        setList(res.values.list)
        setTotal(res.values.total)
      } else {
        message.error(res.msg)
      }
      setLoading(false)
    })
  }

  useEffect(() => {
    getList()
  }, [query])

  // 往父组件传过来的ref中添加方法
  useImperativeHandle(ref, () => {
    return {
      getList,
      setQuery: (newQuery) => {
        setQuery({...query, ...newQuery})
      }
    }
  }, [getList, setQuery])

  return (
    <div>
      <Table
        rowKey={rowKey}
        columns={columns}
        dataSource={list}
        loading={loading}
        pagination={
          pagination === false ?  pagination :
          {
            showSizeChanger: true,
            pageSizeOptions: [2, 3, 4, 5],
            current: query.page,
            pageSize: query.pagesize,
            total,
            onChange: (page, pagesize) => {
              setQuery({...query, page, pagesize})
            },
            ...pagination
          }
        }
        {...tableProps}
      />
    </div>
  )
}

export default forwardRef(index)


import { HomeOutlined } from '@ant-design/icons';
import { Breadcrumb, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { Link, useLocation, useDispatch, useSelector, IMenuState, IMenuItem } from 'umi'
import { renderIcon } from '@/config/menuConfig'

const Bread: React.FC = () => {
  const location = useLocation()
  const dispatch = useDispatch()
  const [breadList, setBread] = useState<IMenuItem[]>([])
  // 所有菜单
  const menulist = useSelector<{ menu: IMenuState }, IMenuItem[]>(state => state.menu.list)

  useEffect(() => {
    dispatch({
      type: 'menu/getMenu'
    })
  }, [])

  // 监听路由变化修改面包屑
  useEffect(() => {
    const pathlist: IMenuItem[] = []
    menulist.forEach(item => {
      // 判断当前路径是否包含此路径
      if (location.pathname.includes(item.path)) {
        pathlist.push(item)
        if (item.children) {
          item.children.forEach(val => {
            if (location.pathname.includes(val.path)) {
              pathlist.push(val)
            }
          })
        }
      }
    })
    setBread(pathlist)
  }, [location, menulist])

  return (
    <Breadcrumb>
      {location.pathname !== '/dashboard' &&
        <Breadcrumb.Item>
          <Link to="/"><HomeOutlined /> 首页</Link>
        </Breadcrumb.Item>
      }
      {breadList.map(item =>
        <Breadcrumb.Item key={item.key}>
          {item.path === location.pathname || !item.children ?
            <Space>
              {renderIcon(item.icon)}
              {item.title}
            </Space>
          :
            <Link to={item.children[0].path}>
              <Space>{renderIcon(item.icon)}{item.title}</Space>
            </Link>
          }
        </Breadcrumb.Item>
      )}
    </Breadcrumb>
  )
};

export default Bread;
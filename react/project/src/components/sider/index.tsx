import { Link, useLocation, useSelector, IUserState, IMenuState, IMenuItem } from 'umi'
import { Layout, Menu, message } from 'antd';
import { useState, useEffect, useMemo } from 'react'
import style from './index.scss'
import { renderIcon } from '@/config/menuConfig'
import { getMenulist } from '@/services/api'
interface IProps {
  collapsed: boolean;
}

// 格式化菜单
const formatMenu = (menulist: any[], authMenu: string[]) => {
  const res: any[] = []
  menulist.forEach(item => {
    const { key, icon, title, children } = item
    // 允许访问的菜单如果包涵此选项继续往下执行
    if (authMenu.includes(key)) {
      const newItem: any = {
        key,
        icon: renderIcon(icon),
        label: children ? title : <Link to={key}>{title}</Link>
      }
      if (children) newItem.children = formatMenu(children, authMenu)
      res.push(newItem)
    }
  })
  return res
}

const index: React.FC<IProps> = (props) => {
  const location = useLocation()
  const [openKeys, setOpenKeys] = useState<string[]>([])
  // 所有菜单
  const menuList = useSelector<{ menu: IMenuState }, IMenuItem[]>(state => state.menu.list)
  // 当前用户可以访问的菜单
  const authmenu = useSelector<{ user: IUserState }, string[]>(state => state.user.menulist)
  // 格式化当前用户可以访问的菜单数据
  const curMenu = useMemo(() => formatMenu(menuList, authmenu), [authmenu, menuList])

  // 进入页面自动展开对应的菜单项
  useEffect(() => {
    menuList.forEach(item => {
      if (item.children?.find(v => v.key === location.pathname)) {
        setOpenKeys([item.key])
      }
    })
  }, [location, menuList])


  return (
    <Layout.Sider style={{ boxShadow: 'rgb(29 35 41 / 5%) 2px 0px 8px 0px' }} theme="light" trigger={null} collapsible collapsed={props.collapsed}>
      <div className={style.logo}>
        <img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" alt="" />
        {!props.collapsed && <h2>管理系统</h2>}
      </div>
      <Menu
        theme="light"
        mode="inline"
        openKeys={openKeys}
        selectedKeys={[location.pathname]}
        items={curMenu}
        onOpenChange={val => setOpenKeys(val)}
      />
    </Layout.Sider>
  )
}

export default index

import { Redirect } from 'umi'

const auth: React.FC = (props) => {
  let token = localStorage.getItem('mqq-token')
  if (token) {
    return <>{props.children}</>
  } else {
    return <Redirect to="/login" />
  }
}

export default auth
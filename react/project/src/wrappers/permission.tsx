import { Button, Result } from 'antd';
import { useSelector, IUserState, IRouteComponentProps, history } from 'umi'
import Loading from '@/components/loading';

const permission: React.FC<IRouteComponentProps> = (props) => {
  const menulist = useSelector<{ user: IUserState }, IUserState['menulist']>(state => state.user.menulist)

  if (menulist.length === 0) {
    return <Loading />
  }

  if (menulist.includes(props.location.pathname)) {
    return <>{props.children}</>
  }

  return <Result
    status="403"
    title="403"
    subTitle="没有此页面的访问权限"
    extra={<Button type="primary" onClick={() => history.push('/')}>去首页</Button>}
  />
  
}

export default permission
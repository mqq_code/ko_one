import { Layout } from 'antd';
import React, { useState, useEffect } from 'react'
import Header from '@/components/header'
import Sider from '@/components/sider'
import LoadingCom from '@/components/loading'
import { useDispatch, useSelector, Loading, IUserState } from 'umi'
import './indexpage.less'

const { Content } = Layout;

const indexpage: React.FC = (props) => {
  const [collapsed, setCollapsed] = useState(false);
  const dispatch = useDispatch()
  // const loading = useSelector<{ loading: Loading }, boolean>(state => state.loading.models.user)

  useEffect(() => {
    console.log('进入首页')
    dispatch({
      type: 'user/getUser'
    })
  }, [])

  return (
    <Layout style={{ height: '100%' }}>
      <Sider collapsed={collapsed} />
      <Layout>
        <Header collapsed={collapsed} setCollapsed={setCollapsed} />
        <Content
          style={{
            padding: 24,
            minHeight: 280,
            overflow: 'auto'
          }}
        >
          {/* {loading ? <LoadingCom /> : props.children} */}
          {props.children}
        </Content>
      </Layout>
    </Layout>
  )
}

export default indexpage
interface Base {
  code: number;
  msg: string;
}

interface IRequestPage {
  page: number;
  pagesize: number;
  account?: string;
  status?: string;
  email?: string;
  adduser?: string;
}

interface Iuser {
  uid: number;
  username: string;
  avatar: string;
  password: string;
  expiration: number;
  account: string;
  email: string;
  status: number;
  addTime: string;
  lastOnlineTime: string;
  adduser: string;
  disabled: boolean;
  role: string[];
}
// 用户列表接口
interface IUserListResponse extends Base {
  values: {
    page?: number;
    pagesize?: number;
    total: number;
    totalPage: number;
    list: Iuser[]
  }
}
import request from './request'

export const getMenulist = () => {
  return request.get<any, any>('/api/permissions/menulist')
}

export const getUserlist = (params?: IRequestPage) => {
  return request.get<IRequestPage, IUserListResponse>('/api/user/list', {
    params
  })
}

export const editUser = (params: any = {}) => {
  return request.put<any, any>('/api/user/edit', params)
}

export const delUser = (uid: number) => {
  return request.delete<any, any>('/api/user/remove', {
    params: { uid }
  })
}

export const addUser = (params: any = {}) => {
  return request.post<any, any>('/api/user/add', params)
}

// 角色列表
export const getRoleList = (params: any = {}) => {
  return request.get<any, any>('/api/role/list', { params })
}
// 修改角色配置
export const setRoleMenu = (params: any = {}) => {
  return request.post<any, any>('/api/role/menu', params)
}

// 修改角色配置
export const getUserInfo = () => {
  return request.post<any, any>('/api/user/info')
}

// 退出登陆
export const logout = () => {
  return request.post<any, any>('/api/user/logout')
}

// 获取文章列表
export const getArticleList = (params: any = {}) => {
  return request.get<any, any>('/api/article/list', { params })
}

// 获取文章分类列表
export const getArticleType = (params: any = {}) => {
  return request.get<any, any>('/api/article/type/list', { params })
}
// 添加文章分类
export const addArticleType = (params: any = {}) => {
  return request.post<any, any>('/api/article/type/add', params)
}
export const addArticle = (params: any = {}) => {
  return request.post<any, any>('/api/article/add', params)
}



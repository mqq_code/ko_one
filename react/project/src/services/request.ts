import axios from 'axios'
import { message } from 'antd'
import { history } from 'umi'

const host = 'http://121.89.213.194:5000'

const instance = axios.create({
  baseURL: host
})

// 添加请求拦截，处理公共参数
instance.interceptors.request.use(config => {
  config.headers!.authorization = localStorage.getItem('mqq-token')!
  return config
}, err => Promise.reject(err))


// 添加响应拦截，统一处理公共错误信息，例如 401 登陆失效
instance.interceptors.response.use(response => {
  return response.data
}, err => {
  if (err.response.status === 401) {
    message.error('登陆信息失效，请重新登陆!')
    localStorage.removeItem('mqq-token')
    history.push('/login')
  } else {
    message.error(err.message)
  }
  return Promise.reject(err)
})


export default instance

import {
  HomeOutlined,
  ReadOutlined,
  FileSearchOutlined,
  FileAddOutlined,
  AppstoreAddOutlined,
  PlusSquareOutlined,
  AppstoreOutlined,
  ApartmentOutlined,
  TeamOutlined,
  MenuOutlined,
  UsergroupAddOutlined,
  SettingOutlined,
  UserOutlined,
  UserSwitchOutlined
} from '@ant-design/icons';


const menuIcon: any = {
  HomeOutlined,
  ReadOutlined,
  FileSearchOutlined,
  FileAddOutlined,
  AppstoreAddOutlined,
  PlusSquareOutlined,
  AppstoreOutlined,
  ApartmentOutlined,
  TeamOutlined,
  MenuOutlined,
  UsergroupAddOutlined,
  SettingOutlined,
  UserOutlined,
  UserSwitchOutlined
}


export const renderIcon = (icon: string) => {
  let Icon = menuIcon[icon]
  if (Icon) {
    return <Icon />
  }
  return null
}

export default menuIcon
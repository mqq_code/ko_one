import style from './login.scss'
import { LockOutlined, UserOutlined, MobileOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, Tabs, Col, Row, message } from 'antd';
import axios from 'axios'
import { history } from 'umi'
import { useState } from 'react'

const { TabPane } = Tabs;

interface IForm {
  username: string;
  password: string;
  remember: boolean;
  tel: string;
  verifyCode: string;
}

const index = () => {
  const [form] = Form.useForm<IForm>()
  const onFinish = () => {
    // 表单校验通过
    form.validateFields().then(value => {
      if (value.remember) {
        localStorage.setItem('username', value.username)
      } else {
        localStorage.removeItem('username')
      }
      // 调用登陆接口
      axios.post('http://121.89.213.194:5000/api/user/login', {
        username: value.username,
        password: value.password
      }).then(res => {
        if (res.data.code === 200) {
          message.success('登陆成功')
          localStorage.setItem('mqq-token', res.data.values.token)
          history.push('/')
        } else {
          message.error(res.data.msg)
        }
      }).catch(e => {
        message.error(e.message)
      })
    })

  };

  const [num, setNum] = useState(0)
  const getVerifyCode = () => {
    setNum(10)
    const timer = setInterval(() => {
      setNum(n => {
        if (n - 1 <= 0) {
          clearInterval(timer)
        }
        return n - 1
      })
    }, 1000)
  }

  const usernameRender = () => (
    <>
      <Form.Item
        name="username"
        rules={[{ required: true, message: '请输入用户名!' }]}
      >
        <Input size="large" prefix={<UserOutlined />} placeholder="用户名" />
      </Form.Item>
      <Form.Item
        name="password"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Input.Password
          size="large"
          prefix={<LockOutlined />}
          placeholder="密码"
        />
      </Form.Item>
      <Form.Item>
        <Form.Item name="remember" valuePropName="checked" noStyle>
          <Checkbox>记住账号</Checkbox>
        </Form.Item>
        <a style={{ float: 'right' }} href="">忘记密码</a>
      </Form.Item>
    </>
  )

  const telRender = () => (
    <>
      <Form.Item
        name="tel"
        rules={[
          { required: true, message: '请输入手机号!' },
          { pattern: /^1[3-9]\d{9}$/, message: '请输入正确的手机号!' }
        ]}
      >
        <Input size="large" prefix={<MobileOutlined />} placeholder="手机号" />
      </Form.Item>
      <Form.Item
        name="verifyCode"
        rules={[
          { required: true, message: '请输入验证码!' },
          { len: 6, message: '请输入正确的验证码!' }
        ]}
      >
        <Row gutter={15}>
          <Col span={14}><Input size="large" prefix={<LockOutlined />} placeholder="验证码"/></Col>
          <Col span={10}>
            <Button disabled={num > 0} block size="large" onClick={getVerifyCode}>{num <= 0 ? '获取验证码' : `${num}s后重试`}</Button>
          </Col>
        </Row>
      </Form.Item>
    </>
  ) 

  return (
    <div className={style.wrap}>
      <div className={style.content}>
        <div className={style.top}>
          <div className={style.logo}>
            <img src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg" alt="" />
            <h2>管理系统</h2>
          </div>
          <p>一个不成熟的后台管理系统</p>
        </div>
        <Form
          initialValues={{
            remember: !!localStorage.getItem('username'),
            username: localStorage.getItem('username')
          }}
          form={form}
        >
          <Tabs defaultActiveKey="1" centered destroyInactiveTabPane>
            <TabPane tab="账号密码登陆" key="1">
              {usernameRender()}
            </TabPane>
            <TabPane tab="手机号登陆" key="2">
              {telRender()}
            </TabPane>
          </Tabs>
          <Form.Item>
            <Button size="large" type="primary" onClick={onFinish} block>登陆</Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default index
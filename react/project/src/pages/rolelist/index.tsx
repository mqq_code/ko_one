import { useState, useEffect, useRef } from 'react'
import { message, Button, Drawer, Tree } from 'antd'
import type { DataNode, TreeProps } from 'antd/es/tree';
import type { ColumnsType } from 'antd/es/table';
import { getRoleList, getMenulist, setRoleMenu } from '@/services/api'
import PageList from '@/components/pageList'

interface IRole {
  id: number;
  menuList: string[];
  note: string;
  role: string;
  roleName: string;
}


const index = () => {

  const [visible, setVisible] = useState(false)
  const [editRole, setEditRole] = useState<any>({})
  const [defaultRree, setDefaultRree] = useState<{checked: string[], halfChecked: string[]}>({ checked: [], halfChecked: [] })
  const [treeData, setTreeData] = useState<DataNode[]>([]) // 菜单树形结构
  const [treeloading, setTreeloading] = useState(false)
  const tableIns = useRef<{ getList: () => void; aa: string }>()

  const columns: ColumnsType<IRole> = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '角色关键字',
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: '角色名称',
      dataIndex: 'roleName',
      key: 'roleName',
    },
    {
      title: '角色描述',
      dataIndex: 'note',
      key: 'note',
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => {
        return <Button onClick={() => {
          setVisible(true) // 展示弹窗
          setEditRole(record) // 存要修改的角色信息
          // 反显选中的tree
          const checkedKeys: {checked: string[], halfChecked: string[]} = {
            checked: [],
            halfChecked: []
          }
          treeData.forEach(item => {
            // 查找此项菜单是否存在当前角色中
            if (record.menuList.includes(item.key as string)) {
              // 判断此项菜单的子菜单是否在当前角色中
              if (item.children?.every(v => record.menuList.includes(v.key as string))) {
                checkedKeys.checked.push(item.key as string, ...item.children.map(v => v.key as string))
              } else {
                if (item.children) {
                  checkedKeys.halfChecked.push(item.key as string)
                  item.children.forEach(v => {
                    if (record.menuList.includes(v.key as string)) {
                      checkedKeys.checked.push(v.key as string)
                    }
                  })
                } else {
                  checkedKeys.checked.push(item.key as string)
                }
              }
            }
          })
          setDefaultRree(checkedKeys)
        }} type="link">分配角色</Button>
      }
    },
  ];

  // 选中菜单
  const onCheck: TreeProps['onCheck'] = (checkedKeys, info) => {
    setDefaultRree({
      checked: checkedKeys as string[],
      halfChecked: info.halfCheckedKeys as string[]
    })
  };
  // 确定修改角色菜单
  const confirm = () => {
    setTreeloading(true)
    setRoleMenu({
      id: editRole.id,
      menulist: [...defaultRree.checked, ...defaultRree.halfChecked]
    }).then(res => {
      if (res.code === 200) {
        message.success('修改成功')
        setVisible(false)
        tableIns.current?.getList()
      } else {
        message.error(res.msg)
      }
      setTreeloading(false)
    })
  }

  useEffect(() => {
    getMenulist().then(res => {
      setTreeData(res.values.list)
    })
  }, [])


  return (
    <div>
      <PageList ref={tableIns} columns={columns} getListApi={getRoleList} />
      <Drawer
        title="分配角色"
        placement="right"
        destroyOnClose
        visible={visible}
        onClose={() => setVisible(false)}
        footer={
          <Button
            type="primary"
            disabled={editRole.role === 'admin'}
            onClick={confirm}
            loading={treeloading}
          >确定</Button>
        }
      >
        <Tree
          defaultExpandAll
          checkable
          checkedKeys={defaultRree}
          selectable={false}
          onCheck={onCheck}
          treeData={treeData}
          disabled={editRole.role === 'admin' || treeloading}
        />
      </Drawer>
    </div>
  )
}

export default index
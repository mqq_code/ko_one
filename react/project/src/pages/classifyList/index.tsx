import type { ColumnsType } from 'antd/es/table';
import { getArticleType } from '@/services/api'
import PageList from '@/components/pageList'

const index = () => {

  const columns: ColumnsType<any> = [
    {
      title: '分类名称',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: '分类关键字',
      dataIndex: 'keyCode',
      key: 'keyCode',
    },
    {
      title: '文章数量',
      dataIndex: 'count',
      key: 'count',
    }
  ];

  return (
    <PageList
      columns={columns}
      getListApi={getArticleType}
      rowKey="keyCode"
      pagesize={5}
      pagination={{
        pageSizeOptions: [2, 6, 10]
      }}
    />
  )
}

export default index
import { Form, Button, Input, Select, Row, Col, Space } from 'antd'
import { DownOutlined, UpOutlined } from '@ant-design/icons'
import { useState } from 'react'

interface IProps {
  search: (params: any) => void;
  reset: () => void;
}

const Search: React.FC<IProps> = (props) => {
  const [flag, setFlag] = useState(false)

  return (
    <Form labelCol={{ span: 6 }} onFinish={props.search} initialValues={{ status: '' }}>
      <Row gutter={20}>
        <Col span={8}>
          <Form.Item label="姓名/账号" name="account">
            <Input placeholder="请输入姓名或者账号" />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="启用状态" name="status">
            <Select>
              <Select.Option value="">全部</Select.Option>
              <Select.Option value={1}>启用</Select.Option>
              <Select.Option value={0}>禁用</Select.Option>
            </Select>
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="邮箱" name="email">
            <Input placeholder="请输入邮箱" />
          </Form.Item>
        </Col>
      </Row>
      {flag && 
        <Row gutter={20}>
          <Col span={8}>
            <Form.Item label="创建人" name="adduser">
              <Input placeholder="创建人" />
            </Form.Item>
          </Col>
        </Row>
      }
      <Row>
        <Col>
          <Space>
            <Button type="primary" htmlType="submit">搜索</Button>
            <Button htmlType="reset" onClick={() => props.reset()}>重置</Button>
            <Button
              onClick={() => setFlag(!flag)}
              icon={flag ? <UpOutlined /> : <DownOutlined /> }
              type="link"
            >{flag ? '收起' : '展开'}</Button>
          </Space>
        </Col>
      </Row>
    </Form>
  )
}

export default Search
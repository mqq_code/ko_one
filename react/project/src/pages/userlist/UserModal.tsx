import { message, Modal, Form, Input, Radio } from 'antd'
import { useState, useEffect, forwardRef, useImperativeHandle } from 'react'
import { addUser } from '@/services/api'

interface IProps {
  visible: boolean;
  modalType: number;
  uid: number;
  hide: () => void;
  edit: (params: any) => Promise<any>;
  getList: () => void;
}

const UserModal: React.ForwardRefRenderFunction<any, IProps> = ({
  visible,
  modalType,
  uid,
  hide,
  edit, 
  getList
}, ref) => {
  const [confirmLoading, setConfirmLoading] = useState(false);
  const modalTitle = ['添加用户', '编辑用户']

  // 获取表单实例
  const [form] = Form.useForm()
  // 关闭表单清空表单的数据
  useEffect(() => {
    if (!visible) {
      form.resetFields()
    }
  }, [visible])

  useImperativeHandle(ref, () => form)

  return (
    <Modal
      title={modalTitle[modalType]}
      visible={visible}
      onOk={() => {
        // 触发校验
        form.validateFields().then(async values => {
          setConfirmLoading(true)
          if (modalType === 1) {
            // 调用编辑接口
            await edit({ ...values, uid: uid })
            setConfirmLoading(false)
            hide()
          } else {
            // 调用添加接口
            const res = await addUser(values)
            if (res.code === 200) {
              message.success('添加成功')
              getList() // 添加完成重新获取列表数据
              hide()
            } else {
              message.error(res.msg)
            }
            setConfirmLoading(false)
          }
        })
      }}
      confirmLoading={confirmLoading}
      onCancel={hide}
      cancelText="取消"
      okText="确定"
    >
      <Form form={form}>
        <Form.Item
          label="姓名"
          name="account"
          rules={[{ required: true, message: '请输入姓名!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="账号"
          name="username"
          rules={[{ required: true, message: '请输入账号!' }]}
        >
          <Input disabled={modalType === 1} />
        </Form.Item>

        {modalType === 0 &&
          <Form.Item
            label="密码"
            name="password"
            rules={[{ required: true, message: '请输入密码!' }]}
          >
            <Input.Password />
          </Form.Item>
        }

        <Form.Item
          label="邮箱"
          name="email"
          rules={[
            { required: true, message: '请输入邮箱!' },
            { type: 'email', message: '邮箱格式不正确!' }
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="启用状态"
          name="status"
          rules={[{ required: true, message: '请选择状态!' }]}
        >
          <Radio.Group>
            <Radio value={0}>禁用</Radio>
            <Radio value={1}>启用</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  )
}

export default forwardRef(UserModal)
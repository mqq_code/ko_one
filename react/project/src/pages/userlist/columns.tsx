import moment from 'moment'
import type { ColumnsType } from 'antd/es/table';
import { Button, Space, Switch, Popconfirm } from 'antd'

const getColumns = (
  edit: (params: any) => void,
  del: (uid: number) => void,
  showEditModal: (record: Iuser) => void,
  showRole: (record: Iuser) => void
) => {
  const columns: ColumnsType<Iuser> = [
    {
      title: '姓名',
      dataIndex: 'account',
      key: 'account',
      width: 100,
      render: _ => <b>{_}</b>
    },
    {
      title: '账号',
      dataIndex: 'username',
      key: 'username',
      width: 100
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
      width: 200
    },
    {
      title: '是否启用',
      dataIndex: 'status',
      key: 'status',
      width: 100,
      render: (status, record) => (
        <Switch
          onChange={(checked) => {
            edit({
              uid: record.uid,
              status: Number(checked)
            })
          }}
          disabled={record.disabled}
          checked={!!status}
          checkedChildren="开启"
          unCheckedChildren="关闭"
        ></Switch>
      )
    },
    {
      title: '创建人',
      dataIndex: 'adduser',
      key: 'adduser',
      width: 200
    },
    {
      title: '创建时间',
      dataIndex: 'addTime',
      key: 'addTime',
      width: 200,
      render: addTime => moment(addTime).format('YYYY-MM-DD HH:mm:ss') 
    },
    {
      title: '最后登陆时间',
      dataIndex: 'lastOnlineTime',
      key: 'lastOnlineTime',
      width: 200,
      render: lastOnlineTime => lastOnlineTime ? moment(lastOnlineTime).format('YYYY-MM-DD HH:mm:ss') : '-'
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      width: 300,
      render: (_, record) => <Space>
        <Button
          disabled={record.disabled}
          type="primary"
          onClick={() => showRole(record)}
        >分配角色</Button>
        <Button
          disabled={record.disabled}
          type="ghost"
          onClick={() => {
            showEditModal(record)
          }}
        >编辑</Button>
        <Popconfirm
          title={<>确定删除<b style={{color: 'red'}}>{record.account}</b>吗？</>}
          okText="删除"
          cancelText="取消"
          okType="danger"
          onConfirm={() => del(record.uid)}
          disabled={record.disabled}
        >
          <Button disabled={record.disabled} type="primary" danger>删除</Button>
        </Popconfirm>
      </Space>,
    },
  ];
  return columns
}

export default getColumns
import { Button, message, FormInstance, Modal, Select, Form } from 'antd'
import { getUserlist, editUser, delUser, getRoleList } from '@/services/api'
import { useState, useRef } from 'react';
import getColumns from './columns';
import UserModal from './UserModal';
import Search from './Search';
import PageList, { ITableIns } from '@/components/pageList';

const index: React.FC = () => {
  const [rolelist, setRoleList] = useState<any[]>([]);
  const [visibleRole, setVisibleRole] = useState(false); // 控制分配角色弹窗
  const [visible, setVisible] = useState(false); // 控制添加弹窗显示隐藏
  const [modalType, setModalType] = useState(0); // 弹窗类型，添加还是编辑
  const [uid, setUid] = useState<number | null>(null) // 编辑的uid
  const modalForm = useRef<FormInstance<any>>() // 定义ref变量获取子组件的数据
  const [form] = Form.useForm<{ selectRole: string[] }>() // 跟配角色表单
  const tableIns = useRef<ITableIns>()
  // 修改数据
  const edit = async (params: any) => {
    const res = await editUser(params)
    if (res.code === 200) {
      message.success('修改成功')
      tableIns.current?.getList()
    } else {
      message.error(res.msg)
    }
  }
  // 删除数据
  const del = (uid: number) => {
    delUser(uid).then(res => {
      if (res.code === 200) {
        message.success('删除成功')
        tableIns.current?.getList()
      } else {
        message.error(res.msg)
      }
    })
  }
  // 点击编辑按钮
  const showEditModal = (record: Iuser) => {
    setVisible(true)
    setModalType(1)
    // 使用ref调用子组件中的方法
    modalForm.current?.setFieldsValue({ ...record })
    // 存uid
    setUid(record.uid)
  }
  // 分配角色
  const showRole = (record: Iuser) => {
    form.setFieldValue('role', record.role)
    setUid(record.uid)
    setVisibleRole(true)
    // 获取所有角色列表
    getRoleList().then(res => {
      setRoleList(res.values.list)
    })
  }
  // table的列数据
  const columns = getColumns(edit, del, showEditModal, showRole)

  const search = (values: any) => {
    tableIns.current?.setQuery({
      page: 1,
      ...values
    })
  }
  const reset = () => {
    tableIns.current?.setQuery({
      page: 1,
      status: '',
      account: '',
      email: '',
      adduser: ''
    })
  }

  return (
    <div>
      <Search search={search} reset={reset} />
      <div style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: 20 }}>
        <Button type="primary" onClick={() => {
          setVisible(true)
          setModalType(0)
        }}>添加用户</Button>
      </div>
      <PageList
        rowKey="uid"
        scroll={{ x: 1000 }}
        columns={columns}
        getListApi={getUserlist}
        ref={tableIns}
      />
      <UserModal
        visible={visible}
        modalType={modalType}
        uid={uid!}
        hide={() => setVisible(false)}
        getList={() => {
          tableIns.current?.getList()
        }}
        edit={edit}
        ref={modalForm}
      />
      <Modal
        destroyOnClose
        visible={visibleRole}
        title="分配角色"
        okText="确定"
        cancelText="取消"
        onOk={() => {
          // 校验表单
          form.validateFields().then(values => {
            // 校验通过调用编辑接口
            edit({ ...values, uid }).then(res => {
              setVisibleRole(false)
            })
          })
        }}
        onCancel={() => setVisibleRole(false)}
      >
        <Form form={form}>
          <Form.Item
            name="role"
            rules={[{ required: true, message: '请选择角色!' }]}
          >
            <Select
              mode="multiple"
              placeholder="请选择角色"
              style={{ width: '100%' }}
            >
              {rolelist.map(item =>
                <Select.Option key={item.id} value={item.role}>{item.roleName}</Select.Option>
              )}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default index
import React, { useEffect, useState } from 'react'
import { Form, Input, Button, message, Select } from 'antd'
import { getArticleType, addArticle } from '@/services/api'
import { history } from 'umi'
// 引入编辑器组件
import BraftEditor from 'braft-editor'
// 引入编辑器样式
import 'braft-editor/dist/index.css'

const index = () => {
  const [options, setOptions] = useState<any[]>([])
  const [value, setValue] = useState(BraftEditor.createEditorState(null));

  const onFinish = async (values: any) => {
    const params = {
      ...values,
      content: values.content.toHTML() // 把富文本的内容转成html字符串格式
    }
    try {
      const res = await addArticle(params)
      if (res.code === 200) {
        message.success('添加成功')
        history.push('/article/list')
      } else {
        message.error(res.msg)
      }
    } catch(e) {}
  }

  useEffect(() => {
    getArticleType().then(res => {
      setOptions(res.values.list)
    })
  }, [])

  return (
    <div style={{ background: '#fff', minHeight: '100%', padding: 20, borderRadius: 10 }}>
      <Form
        layout="vertical"
        onFinish={onFinish}
      >
        <Form.Item
          name="title"
          label="文章标题"
          style={{ width: 400 }}
          rules={[{ required: true, message: '请输入文章标题!' }]}
        >
          <Input placeholder="请输入文章标题" />
        </Form.Item>
        <Form.Item
          name="keyCode"
          label="文章类型"
          style={{ width: 400 }}
          rules={[{ required: true, message: '请选择文章类型!' }]}
        >
          <Select placeholder="选择文章类型" mode="multiple">
            {options.map((item, index) =>
              <Select.Option key={index} value={item.keyCode}>{item.title}</Select.Option>
            )}
          </Select>
        </Form.Item>
        <Form.Item
          name="desc"
          label="文章简介"
          style={{ width: 400 }}
          rules={[{ required: true, message: '请输入文章介绍!' }]}
        >
          <Input.TextArea placeholder="简介" />
        </Form.Item>
        <Form.Item
          name="content"
          label="文章内容"
          rules={[
            {
              validator: (rule, value) => {
                if (!value || value.isEmpty()) {
                  return Promise.reject(new Error('请输入文章内容'))
                }
                return Promise.resolve()
              }
            },
          ]}
        >
          <BraftEditor style={{ border: '1px solid #d9d9d9' }} value={value} onChange={setValue} />
        </Form.Item>
        <Form.Item>
          <Button htmlType="submit" type="primary">提交</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default index
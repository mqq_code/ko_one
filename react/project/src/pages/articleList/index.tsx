import { Button, Tag } from 'antd'
import type { ColumnsType } from 'antd/es/table';
import { getArticleList } from '@/services/api'
import PageList from '@/components/pageList'

const index = () => {

  const columns: ColumnsType<any> = [
    {
      title: '文章标题',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: '文章类型',
      dataIndex: 'keyCode',
      key: 'keyCode',
      render: keyCode => {
        return keyCode.map((v: any) => <Tag key={v.keyCode} color={v.color}>{v.title}</Tag>)
      }
    },
    {
      title: '文章简介',
      dataIndex: 'desc',
      key: 'desc',
    },
    {
      title: '发布人',
      dataIndex: 'author',
      key: 'author',
    },
    {
      title: '发布时间',
      dataIndex: 'createTime',
      key: 'createTime',
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => {
        return <Button>预览</Button>
      }
    },
  ];

  return (
    <PageList
      columns={columns}
      getListApi={getArticleList}
      pagination={{
        showSizeChanger: false
      }}
    />
  )
}

export default index
import type { ColumnsType } from 'antd/es/table';
import { renderIcon } from '@/config/menuConfig'
import { getMenulist } from '@/services/api'
import PageList from '@/components/pageList'

const columns: ColumnsType<any> = [
  {
    title: '菜单id',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: '菜单标题',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: '菜单路径',
    dataIndex: 'path',
    key: 'path',
  },
  {
    title: '菜单图标',
    dataIndex: 'icon',
    key: 'icon',
    render: icon => renderIcon(icon)
  },
];

const index = () => {

  return (
    <PageList
      columns={columns}
      getListApi={getMenulist}
      pagination={false}
    />
  )
}

export default index
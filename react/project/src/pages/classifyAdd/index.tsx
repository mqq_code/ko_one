import React from 'react'
import { Form, Input, Button, message } from 'antd'
import { addArticleType } from '@/services/api'
import { history } from 'umi'

const index = () => {

  const onFinish = async (values: any) => {
    try {
      const res = await addArticleType(values)
      if (res.code === 200) {
        message.success('添加成功')
        history.push('/classify/list')
      } else {
        message.error(res.msg)
      }
    } catch(e) {}
  }

  return (
    <div style={{ background: '#fff', minHeight: '100%', padding: 20, borderRadius: 10 }}>
      <Form labelCol={{ span: 8 }} style={{ width: 400 }} onFinish={onFinish}>
        <Form.Item name="title" label="添加分类" rules={[{ required: true, message: '请输入分类名称!' }]}>
          <Input placeholder="添加分类名称" />
        </Form.Item>
        <Form.Item name="keyCode" label="添加关键字" rules={[{ required: true, message: '请输入分类关键字!' }]}>
          <Input placeholder="添加分类关键字" />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button htmlType="submit" type="primary">提交</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default index
import { defineConfig } from 'umi';

const config = defineConfig({
  routes: [
    {
      exact: true,
      path: '/login',
      component: '@/pages/login'
    },
    {
      path: '/',
      component: '@/layout/indexpage',
      // 使用高阶组件拦截登陆
      wrappers: ['@/wrappers/auth'],
      routes: [
        {
          exact: true,
          path: '/',
          redirect: '/dashboard',
        },
        {
          exact: true,
          path: '/dashboard',
          component: '@/pages/dashboard',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/article/list',
          component: '@/pages/articleList',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/article/add',
          component: '@/pages/articleAdd',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/classify/add',
          component: '@/pages/classifyAdd',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/classify/list',
          component: '@/pages/classifyList',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          title: '用户管理',
          path: '/auth/userlist',
          component: '@/pages/userlist',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/auth/menulist',
          component: '@/pages/menulist',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/auth/rolelist',
          component: '@/pages/rolelist',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/setting/userinfo',
          component: '@/pages/userinfo',
          wrappers: ['@/wrappers/permission']
        },
        {
          exact: true,
          path: '/setting/setuser',
          component: '@/pages/setuser',
          wrappers: ['@/wrappers/permission']
        },
      ]
    },
  ]
})

export default config.routes
import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes,
  fastRefresh: {},
  mfsu: {},
  dva: {
    immer: true,
    hmr: false,
  },
  dynamicImport: {
    loading: '@/components/loading',
  }
});

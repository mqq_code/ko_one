import React, { Component, PureComponent } from 'react'

// PureComponent: 性能优化，内部实现 shouldComponentUpdate，会对组件的 props 和 state 进行浅比较
class Test extends PureComponent {

  // props更新执行此函数, 当组件中的state依赖于props的时候可以在此函数中改变state
  // componentWillReceiveProps (nextProps) {
  //   console.log('最新的props', nextProps)
  //   if (nextProps.count === 10) {
  //     this.setState({
  //       title: this.state.title + ' 10'
  //     })
  //   }
  // }

  // 优化性能，减少不必要的渲染
  // shouldComponentUpdate (nextProps, nextState) {
  //   // nextProps: 最新的props
  //   // nextState：最新的state
  //   // console.log(nextProps, nextState)
  //   // console.log(this.props, this.state)
  //   if (this.state.title === nextState.title) {
  //     return false
  //   }
  //   return true
  // }

  state = {
    title: {
      name: '123'
    }
  }

  // 静态方法: 当组件中的state依赖于props的时候可以在此函数中改变state
  static getDerivedStateFromProps(props, state) {
    console.log('getDerivedStateFromProps', props, state)
    // 返回的对象会和state进行合并
    if (props.count !== 10) return null
    return {
      title: {
        name: state.title.name + '10'
      }
    }
  }

  // 执行完render函数，dom渲染到页面之前执行
  getSnapshotBeforeUpdate(prevProps, prevState) {
    return 10000
  }
  // 新的dom渲染完成
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log(snapshot)
  }


  render() {
    console.log('渲染 Test 组件', this.props)
    return <div>
      <h2>{this.state.title.name}</h2>
      <input type="text" value={this.state.title.name} onChange={e => {
        this.setState({
          title: {...this.state.title, name: e.target.value}
        }, () => {
          console.log(this.state.title)
        })
      }} />
    </div>
  }
}


class App extends Component {
  // 初始化阶段
  constructor (props) {
    super(props)
    console.log('初始化阶段 ===》 constructor')
  }

  // 挂载之前: componentWillMount
  componentWillMount () {
    console.log('挂载之前  ==> componentWillMount')
  }

  // 挂载成功: componentDidMount, 请求接口，操作dom
  componentDidMount () {
    console.log('挂载成功  ==> componentDidMount')
  }

  // 组件更新之前
  componentWillUpdate () {
    console.log('组件更新之前  ==> componentWillUpdate', this.state.count)
  }

  // 组件更新之后
  componentDidUpdate () {
    console.log('组件更新之后  ==> componentDidUpdate', this.state.count)
  }

  // 组件销毁之前: 清除异步任务，例如定时器，事件监听
  componentWillUnmount () {
    console.log('组件销毁之前 ====> componentWillUnmount')
  }

  state = {
    count: 0,
    abc: 1000
  }

  // shouldComponentUpdate () {
  //   return false
  // }

  // 使用箭头函数绑定this指向，始终指向App组件
  sub = () => {
    this.setState({
      count: this.state.count - 1
    })
  }

  add = () => {
    this.setState({
      count: this.state.count + 1
    })
    // this.state.count += 1
    // // 强制更新，跳过shouldComponentUpdate的校验
    // this.forceUpdate()
  }

  // 渲染组件
  render() {
    console.log('开始渲染 ===> render')
    return (
      <div className='app'>
        <Test count={this.state.count} />
        <button onClick={this.sub}>-</button>
        {this.state.count}
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}
export default App




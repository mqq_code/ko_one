import React, { Component, createRef } from 'react'
import './app.scss'
class App extends Component {

  state = {
    list: []
  }

  appRef = createRef()

  getSnapshotBeforeUpdate() {
    return this.appRef.current.scrollTop
  }
  componentDidUpdate(prevprops, prevState, num) {
    this.appRef.current.scrollTop = num + 50
  }

  componentDidMount () {
    // setInterval(() => {
    //   this.setState({
    //     list: [...this.state.list, {
    //       id: Date.now(),
    //       text: this.state.list.length
    //     }]
    //   })
    // }, 1000);
    document.addEventListener('click', (e) => {
      console.log('原生事件', e)
    })
  }
  add = (e) => {
    console.log('react合成事件', e)
    console.log('在合成事件中获取原生的事件对象', e.nativeEvent)
  }

  render() {
    return (
      <div className='app' ref={this.appRef}>
        <button onClick={this.add}>+</button>
        {this.state.list.map(item => <p key={item.id}>{item.text}</p>)}
      </div>
    )
  }
}

export default App


import React from 'react'; // 创建react元素，组件等核心功能
import ReactDOM from 'react-dom/client'; // dom操作相关的内容
import App from './App'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App title="标题" num="1000" />)

import React, { Component } from 'react'

class App extends Component {

  constructor (props) {
    super(props)
    // 使用bind函数改变this指向，始终指向App组件
    this.add = this.add.bind(this)
  }

  state = {
    count: 0
  }

  // 使用箭头函数绑定this指向，始终指向App组件
  sub = () => {
    console.log(this)
  }

  add() {
    console.log(this)
  }

  render() {
    return (
      <div className='app'>
        <button onClick={this.sub}>-</button>
        {this.state.count}
        {/* <button onClick={() => this.add()}>+</button> */}
        {/* <button onClick={this.add.bind(this)}>+</button> */}
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}
export default App




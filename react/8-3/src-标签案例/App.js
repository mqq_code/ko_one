import React, { Component } from 'react'
import Header from './components/Header'
import Tag from './components/Tag'
import './app.scss'

class App extends Component {
  state = {
    current: '#F95',
    list: []
  }

  changeCurrent = color => {
    this.setState({
      current: color
    })
  }
  keyDown = e => {
    if (e.target.value.trim('') && e.keyCode === 13) {
      this.setState({
        list: [...this.state.list, {
          id: Date.now(),
          text: e.target.value.trim('')
        }]
      })
      e.target.value = ''
    }
  }
  removeTag = id => {
    this.setState({
      list: this.state.list.filter(item => item.id !== id)
    })
  }

  render() {
    const { current, list } = this.state
    return (
      <div className='app'>
        <Header current={current} changeCurrent={this.changeCurrent} />
        <h1><span style={{color: current}}>tags.</span>css</h1>
        <h3>按回车确认</h3>
        <div className="con">
          {list.map(item => (
            <Tag key={item.id} text={item.text} id={item.id} color={current} removeTag={this.removeTag} />
          ))}
          <input onKeyDown={this.keyDown} type="text" placeholder='add tag' />
        </div>
      </div>
    )
  }
}
export default App
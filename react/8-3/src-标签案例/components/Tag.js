import React from 'react'

const Tag = (props) => {
  return (
    <div className='tag' style={{background: props.color}}>
      <span onClick={() => props.removeTag(props.id)}>x</span>
      {props.text}
    </div>
  )
}

export default Tag
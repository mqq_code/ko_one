
import React from 'react'

const color = ['#F95', '#6AD', '#E77', '#6C7', '#AAA']

const Header = (props) => {
  return (
    <header>Tag Color: 
      {color.map(color => (
        <span
          className={props.current === color ? 'active' : ''}
          key={color}
          style={{ background: color }}
          onClick={() => props.changeCurrent(color)}
        ></span>
      ))}
    </header>
  )
}

export default Header

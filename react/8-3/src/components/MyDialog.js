import React, { Component, Children } from 'react'

console.log(Children)

class MyDialog extends Component {
  render() {
    // children的类型不确定，数量为0时 undefined, 数量为1是一个对象，两个以上时数组
    console.log(this.props.children)
    return (
      <div className='dialog'>
        <div className="con">
          <div className="dialog-header">
            {this.props.header}
          </div>
          <div className="dialog-con">
            {Children.map(this.props.children, (item, index) => (
              <div key={index} style={{color: 'red'}}>{item}</div>
            ))}
          </div>
          <div className="dialog-footer">
            {this.props.footer}
          </div>
        </div>
      </div>
    )
  }
}

export default MyDialog
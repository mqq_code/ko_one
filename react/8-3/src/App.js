import React, { Component } from 'react'
import MyDialog from './components/MyDialog'
import './app.scss'
import axios from 'axios'

class App extends Component {
  state = {
    show: false
  }

  componentDidMount () {
    axios.get('/mqq/sell/api/seller').then(res => {
      console.log(res.data)
    })
  }

  changeShow = () => {
    this.setState({
      show: !this.state.show
    })
  }

  dialogHeader = () => {
    return (
      <header>弹窗头部</header>
    )
  }

  render() {
    return (
      <div>
        <button onClick={this.changeShow}>展示弹窗</button>
        {this.state.show &&
          <MyDialog
            header={this.dialogHeader()}
            footer={
              <footer>
                <button>确定</button>
                <button>取消</button>
              </footer>
            }
          >
            <h2>abc</h2>
            {/* <ul>
              <li>1</li>
              <li>2</li>
              <li>3</li>
            </ul> */}
          </MyDialog>
        }
      </div>
    )
  }
}

export default App

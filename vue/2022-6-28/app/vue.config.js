const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {
      devServer.app.post('/music/list', (_, response) => {
        const data = require('./data/data.json')
        response.send({
          code: 1,
          msg: '成功',
          values: data
        })
      })
      return middlewares
    }
  }
})

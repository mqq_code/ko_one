import axios from 'axios'

export default {
  data () {
    return {
      loading: false,
      list: [],
      query: {
        offset: 0,
        limit: 10
      }
    }
  },
  methods: {
    async getList () {
      this.loading = true
      const res = await axios.get(`https://zyxcl-music-api.vercel.app${this.fetchUrl}`, {
        params: {
          ...this.query
        }
      })
      this.loading = false
      this.list = this.list.concat(res.data[this.resKey]) // [...this.list, ...res.data[this.resKey]]
    },
    scrollFn (e) {
      const { scrollTop, clientHeight, scrollHeight } = e.target
      if (scrollTop + clientHeight >= scrollHeight) {
        if (this.loading) return
        this.query.offset += this.query.limit
        this.getList()
      }
    }
  },
  computed: {},
  watch: {},
  created () {
    this.getList()
  },
  mounted () {}
}

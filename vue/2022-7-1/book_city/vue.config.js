const { defineConfig } = require('@vue/cli-service')
const data = require('./data/home.json')
const banner = data.items[0]
const home = data.items.slice(1, data.items.length - 1)
const express = require('express')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.get('/banner', (req, res) => {
        res.send(banner)
      })
      devserver.app.get('/home/list', (req, res) => {
        res.send(home)
      })
      devserver.app.get('/book/detail', (req, res) => {
        const { id } = req.query
        let book = null
        home.forEach(item => {
          item.data.data.forEach(val => {
            if (val.fiction_id === id * 1) {
              book = val
            }
          })
        })
        if (book) {
          res.send({
            code: 1,
            msg: '成功',
            values: book
          })
        } else {
          res.send({
            code: 0,
            msg: '参数错误'
          })
        }
      })
      const users = [
        {
          username: '王小玮',
          password: '123',
          token: 'kadjlajdsijajlkasdljadlkkl'
        },
        {
          username: '萱萱',
          password: '456',
          token: 'bvcxjgisfdjkiesdalskdjl'
        }
      ]
      // express 实例获取 post 请求 json 格式的参数
      devserver.app.use(express.json())
      devserver.app.post('/user/login', (req, res) => {
        const { username, password } = req.body
        const user = users.find(item => item.username === username && item.password === password)
        if (user) {
          res.send({
            code: 1,
            msg: '成功',
            token: user.token
          })
        } else {
          res.send({
            code: 0,
            msg: '账号或者密码错误'
          })
        }
      })
      devserver.app.get('/book/search', (req, res) => {
        const { keyword } = req.query
        let books = []
        home.forEach(item => {
          item.data.data.forEach(val => {
            if ((val.title && val.title.includes(keyword)) || (val.authors && val.authors.includes(keyword))) {
              books.push(val)
            }
          })
        })
        res.send({
          code: 1,
          msg: '成功',
          values: books
        })
      })

      return middlewares
    }
  }
})

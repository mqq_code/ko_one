import fail from '@/assets/fail.jpeg'
import loading from '@/assets/loading.gif'

export default {
  bind (el, binding) {
    // console.log('bind执行了', el)
    el.src = loading
    const image = new Image()
    image.src = binding.value
    image.onload = () => {
      el.src = binding.value
    }
    image.onerror = () => {
      el.src = fail
    }
  },
  // inserted (el, binding) {
  //   console.log('inserted', el.parentNode)
  // }
  update (el, binding) {
    if (binding.value === binding.oldValue) return
    // console.log('指令的数据更新了', binding)
    el.src = loading
    const image = new Image()
    image.src = binding.value
    image.onload = () => {
      el.src = binding.value
    }
    image.onerror = () => {
      el.src = fail
    }
  },
  unbind () { // 清除异步任务
    console.log('v-img的元素销毁了')
  }
}

import img from './img'
import focus from './focus'

const directives = {
  img,
  focus
}

export default function (Vue) {
  // 遍历所有的自定义指令注册
  Object.keys(directives).forEach(key => {
    Vue.directive(key, directives[key])
  })
}

// export default {
//   install (Vue) {
//     // 遍历所有的自定义指令注册
//     Object.keys(directives).forEach(key => {
//       Vue.directive(key, directives[key])
//     })
//   }
// }

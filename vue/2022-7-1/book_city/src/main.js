import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import directives from './directives'
import VConsole from 'vconsole'
import toast from './components/toast'

const vConsole = new VConsole()
console.log(vConsole)
Vue.config.productionTip = false
Vue.use(directives)
Vue.use(toast)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

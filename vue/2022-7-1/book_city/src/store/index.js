import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showAd: true,
    bookCase: []
  },
  mutations: {
    closeAd (state) {
      state.showAd = false
    },
    addCase (state, payload) {
      const index = state.bookCase.findIndex(v => v.fiction_id === payload.fiction_id)
      if (index > -1) {
        state.bookCase.splice(index, 1)
      } else {
        state.bookCase.unshift(payload)
      }
    },
    changeChecked (state, id) {
      state.bookCase.forEach(item => {
        if (item.fiction_id === id) {
          Vue.set(item, 'checked', !item.checked)
        }
      })
    },
    cancelChecked (state) {
      state.bookCase.forEach(item => {
        Vue.set(item, 'checked', false)
      })
    },
    removeBook (state) {
      state.bookCase = state.bookCase.filter(v => !v.checked)
    }
  }
})

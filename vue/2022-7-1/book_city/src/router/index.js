import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import Detail from '../views/detail/Detail'
import Login from '../views/login/Login'
import Read from '../views/read/Read'
import Mine from '../views/mine/Mine'
import Search from '../views/search/Search'
import Notfound from '../views/404'
// 二级路由
import BookCity from '../views/home/book_city/BookCity'
import BookCase from '../views/home/book_case/BookCase'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/bookcity',
    children: [
      {
        path: '/bookcity',
        name: 'bookcity',
        component: BookCity
      },
      {
        path: '/bookcase',
        name: 'bookcase',
        component: BookCase
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: Detail
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/mine',
    name: 'mine',
    component: Mine,
    meta: {
      isRequiredAuth: true
    }
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  },
  {
    path: '/read',
    name: 'read',
    component: Read,
    meta: {
      isRequiredAuth: true
    }
  },
  {
    path: '*',
    component: Notfound
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.isRequiredAuth) {
    const token = localStorage.getItem('token')
    if (!token) {
      console.log(to.fullPath)
      return next({
        path: '/login',
        query: {
          toPath: to.fullPath
        }
      })
    }
  }
  next()
})

export default router

const { defineConfig } = require('@vue/cli-service')
const food = require('./data/food.json')
const milk = require('./data/milk.json')
const vegetable = require('./data/vegetable.json')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.get('/food/list', (req, res) => {
        res.send({
          code: 1,
          msg: '成功',
          values: [
            {
              title: '主食',
              list: food
            },
            {
              title: '蔬菜',
              list: vegetable
            },
            {
              title: '蛋奶豆制品',
              list: milk
            }
          ]
        })
      })
      return middlewares
    }
  }
})

import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    foodList: []
  },
  mutations: { // 修改数据的唯一方式，必须是同步函数
    setFoodList (state, payload) {
      state.foodList = payload
    }
  },
  actions: { // 处理异步
    getFoodList (context, payload) {
      // payload: 组件中传过来的参数
      axios.get('/food/list').then(res => {
        // context: 类似组件中的 this.$store 对象，可以发起dispatch和commit，可以获取state数据
        // 触发mutations函数
        context.commit('setFoodList', res.data.values)
      })
    }
  },
  getters: { // 类似组件中的 computed
    allList (state) {
      let arr = []
      state.foodList.forEach(item => {
        arr = arr.concat(item.list)
      })
      return arr
    }
  },
  plugins: [logger()]
})

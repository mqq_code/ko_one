import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    curIndex: 0, // 首页选中的下标
    foodList: [], // 所有食物列表
    selectFood: [] // 选中的食物列表
  },
  mutations: {
    setFoodList (state, payload) {
      state.foodList = payload
    },
    setCurIndex (state, index) {
      state.curIndex = index
    },
    changeChecked (state, food) {
      Vue.set(food, 'checked', !food.checked)
      if (food.checked) {
        // 选中添加到 selectFood
        state.selectFood.push(food)
      } else {
        // 取消选中从 selectFood 中删除
        state.selectFood = state.selectFood.filter(v => v.name !== food.name)
      }
    }
  },
  plugins: [logger()]
})

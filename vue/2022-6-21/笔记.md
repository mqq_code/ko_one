## Vue: 数据驱动视图改变

### 声明式渲染
```jsx
// {{}} 内只能解析表达式，不能解析语句(例如if、for、while、let...)
<div class="app">
  <h1>{{ title }}</h1>
</div>
let vm = new Vue({
  el: '.app', // 挂载vue的元素
  data: { // 定义数据
    title: '从今天开始学习vue'
  }
})
``` 

### 指令(以 v- 开头的元素的属性)
1. 条件判断指令： v-if、v-else-if、v-else 添加删除dom元素
2. 条件判断：v-show 修改样式，添加删除 display:none
3. 渲染：v-for="(item, index) in arr" 可以遍历数组、对象、字符串、数字
- **注意：不要通过数组的下标修改数据，因为vue监听不到，必须通过数组的方法修改数据，因为vue重写了数组的方法**
4. 渲染内容：v-text、v-html 功能类似原生js中的 innerText和innerHTML
5. 绑定事件：v-on简写@ `<button @:click="函数">按钮</button>`
6. 绑定属性：v-bind简写: `<button :disabled="count > 0">按钮</button>`
7. 表单双向绑定：v-model `<input v-model="title"/>`
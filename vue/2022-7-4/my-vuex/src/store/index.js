import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存数据
  state: {
    list: [], // 所有歌手
    like: [], // 收藏的歌手
    username: '小明',
    age: 100,
    sex: '男',
    hobby: ['打篮球', '保龄球', '高尔夫球']
  },
  // 修改state数据的唯一方法，是一个同步函数
  mutations: {
    setLike (state, payload) {
      // state: 数据， payload: 组件传过来的参数
      state.list.forEach(item => {
        if (item.id === payload.id) {
          // 给 item 添加新的响应式属性
          Vue.set(item, 'isSelected', !item.isSelected)
          if (item.isSelected) {
            state.like.push(payload)
          } else {
            state.like = state.like.filter(v => v.id !== payload.id)
          }
        }
      })
    },
    setList (state, payload) {
      state.list = payload
    }
  }
})

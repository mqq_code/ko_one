const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.get('/singer/list', (req, res) => {
        res.send({
          code: 1,
          msg: '成功',
          values: [
            {
              id: 0,
              text: '周杰伦'
            },
            {
              id: 1,
              text: '许嵩'
            },
            {
              id: 2,
              text: '邓紫棋'
            },
            {
              id: 3,
              text: '王心凌'
            },
            {
              id: 4,
              text: '钟欣潼'
            },
            {
              id: 5,
              text: '蔡卓妍'
            }
          ]
        })
      })
      return middlewares
    }
  }
})

import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import Login from '../views/login/Login'
import FirstPage from '../views/home/fistPage/FirstPage'
import Other from '../views/home/other/Other'
import City from '../views/home/fistPage/city/City'
import Recommend from '../views/home/fistPage/recommend/Recommend'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/first',
    meta: {
      isRequiredAuth: true
    },
    children: [
      {
        path: '/first',
        name: 'firstpage',
        component: FirstPage,
        children: [
          {
            path: '/first',
            name: 'recommend',
            component: Recommend
          },
          {
            path: '/first/city',
            name: 'city',
            component: City
          }
        ]
      },
      {
        path: '/home/:id',
        name: 'other',
        component: Other
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // to.matched: 当前目标路由的所有层级
  if (to.matched.some(t => t.meta.isRequiredAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next('/login')
    }
  }
  next()
})

export default router

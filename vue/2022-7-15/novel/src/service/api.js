import request from './request'

let host = '/api'
// 打包时根据环境变量判断请求的接口域名
if (process.env.NODE_ENV === 'production') {
  host = 'http://api.zhuishushenqi.com'
}

// 获取轮播图接口
export const getBanner = (params = {}) => {
  return request.get(`${host}/recommendPage/node/spread/575f74f27a4a60dc78a435a3?pl=ios`, {
    params
  })
}

// 获取书单接口
export const getBookList = (params = {}) => {
  return request.get(`${host}/book-list`, { params })
}

// 热搜接口
export const getSearchHotwords = () => {
  return request.get(`${host}/book/search-hotwords`)
}

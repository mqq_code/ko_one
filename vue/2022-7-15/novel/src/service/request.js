import { Toast } from 'vant'
import axios from 'axios'
// 二次封装axios，统一处理公共参数，统一处理错误请求

// 创建axios实例对象
const instance = axios.create()

// toast实例对象
let toast = null

// 添加请求拦截：给所有接口添加loading，给所有接口添加公共参数
instance.interceptors.request.use(config => {
  toast = Toast.loading({
    duration: 0, // 持续展示 toast
    forbidClick: true,
    message: 'loading'
  })
  return config
}, err => {
  return Promise.reject(err)
})

// 添加响应拦截：统一处理错误信息
instance.interceptors.response.use(response => {
  toast && toast.clear()
  console.log(response)
  return response.data
}, err => {
  return Promise.reject(err)
})

export default instance

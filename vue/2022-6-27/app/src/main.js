import Vue from 'vue'
import App from './App'

new Vue({
  render: h => h(App) // 渲染根组件到 #app 的位置
}).$mount('#app')

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    memoList: []
  },
  mutations: {
    addMemo (state, payload) {
      state.memoList.unshift({
        ...payload,
        id: Date.now(),
        time: Date.now()
      })
    },
    remove (state, index) {
      state.memoList.splice(index, 1)
    },
    save (state, { index, brokerageBack, myMoneyBack }) {
      const oldInfo = { ...state.memoList[index] }
      state.memoList.splice(index, 1, {
        ...oldInfo,
        brokerageBack,
        myMoneyBack
      })
    }
  },
  getters: {
    money (state) {
      const m = {
        myback: 0, // 本金已返
        my: 0, // 本金未返
        brokerage: 0, // 佣金未返
        brokerageBack: 0 // 佣金已返
      }
      state.memoList.forEach(item => {
        if (item.myMoneyBack) {
          m.myback += item.myMoney
        } else {
          m.my += item.myMoney
        }
        if (item.brokerageBack) {
          m.brokerageBack += item.brokerage
        } else {
          m.brokerage += item.brokerage
        }
      })
      return m
    }
  },
  actions: {
  },
  modules: {
  }
})

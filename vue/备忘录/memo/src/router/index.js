import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import Memo from '../views/home/memo/Memo'
import Create from '../views/home/create/Create'
import Mine from '../views/home/mine/Mine'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/memo',
    children: [
      {
        path: '/memo',
        name: 'memo',
        component: Memo
      },
      {
        path: '/create',
        name: 'create',
        component: Create,
        meta: {
          isAuth: true
        }
      },
      {
        path: '/mine',
        name: 'mine',
        component: Mine,
        meta: {
          isAuth: true
        }
      }
    ]
  },
  {
    path: '/detail/:id',
    name: 'detail',
    // 组件懒加载的原理：webpack拆包，把懒加载的组件单独打包成一个js文件，当调用该组件时动态加载此js文件
    component: () => import('../views/detail/Detail')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/Login')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // 判断当前路由的所有层级只要有其中一层为true就表示需要拦截登陆
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next({
        path: '/login',
        query: {
          fromPath: to.fullPath
        }
      })
    }
  }
  next()
})

export default router

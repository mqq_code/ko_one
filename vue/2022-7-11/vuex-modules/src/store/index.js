import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
import todo from './modules/todo'
import goods from './modules/goods'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    rootTitle: '大仓库'
  },
  mutations: {
    addTodo () {
      console.log('大仓库的addTodo')
    }
  },
  modules: {
    todo,
    goods
  },
  plugins: [logger()]
})

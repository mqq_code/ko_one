
export default {
  // 开启命名空间，使仓库的 mutations 和 actions 只在此模块内注册
  namespaced: true,
  state: () => ({
    todoList: [] // todolist
  }),
  mutations: {
    addTodo (state, payload) {
      state.todoList.unshift(payload)
      console.log('todo仓库的addTodo')
    },
    changeChecked (state, id) {
      const index = state.todoList.findIndex(v => v.id === id)
      state.todoList[index].checked = !state.todoList[index].checked
    }
  }
}

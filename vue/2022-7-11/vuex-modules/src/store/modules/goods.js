import axios from 'axios'

export default {
  // // 开启命名空间，使仓库的 mutations 和 actions 只在此模块内注册
  namespaced: true,
  state: () => ({
    goodsList: [] // 商品列表
  }),
  mutations: {
    setGoodsList (state, payload) {
      state.goodsList = payload
    },
    changeCount (state, payload) {
      const index = state.goodsList.findIndex(v => v.id === payload.id)
      state.goodsList[index].count += payload.num
    }
  },
  actions: {
    async getGoods (context, params) {
      const res = await axios.get('/goods/list', { params })
      context.commit('setGoodsList', res.data.values)
      return res
    }
  },
  getters: {
    total (state) {
      return state.goodsList.reduce((prev, val) => {
        return prev + val.count * val.price
      }, 0)
    }
  }
}

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.get('/goods/list', (req, res) => {
        res.send({
          code: 1,
          msg: '成功',
          values: [
            {
              title: '苹果',
              price: 20,
              count: 0,
              id: 0
            },
            {
              title: '香蕉',
              price: 15,
              count: 0,
              id: 1
            },
            {
              title: '大鸭梨',
              price: 20,
              count: 0,
              id: 2
            },
            {
              title: '阳光玫瑰水晶葡萄',
              price: 200,
              count: 0,
              id: 3
            },
            {
              title: '榴莲',
              price: 100,
              count: 0,
              id: 4
            },
            {
              title: '菠萝蜜',
              price: 30,
              count: 0,
              id: 5
            }
          ]
        })
      })
      return middlewares
    }
  }
})

class Router {
  constructor (params) {
    this.routes = params.routes
    this.routerView = document.querySelector('.router-view')
    this.bindEvent()
    this.update()
    window.addEventListener('hashchange', () => {
      // 监听 # 后的内容变化，展示对应的 component
      this.update()
    })
  }
  update () {
    const path = location.hash.slice(1) // 获取 hash 内容
    const item = this.routes.find(v => v.path === path) // 根据当前hash内容去路由表中查找对应的组件
    this.routerView.innerHTML = item.component // 渲染组件
  }
  bindEvent () {
    const links = [...document.querySelectorAll('.router-link')]
    links.forEach(a => {
      a.addEventListener('click', () => {
        const to = a.getAttribute('data-to')
        location.assign('#' + to)
      })
    })
  }
}
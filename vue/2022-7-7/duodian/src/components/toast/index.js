import Vue from 'vue'
import Toast from './Toast'

const ToastConstructor = Vue.extend(Toast) // 获取Toast组件的构造函数
export const toast = function (msg) {
  const taost = new ToastConstructor() // 创建toast组件
  taost.msg = msg // 把参数传给toast组件
  const toastWrap = document.createElement('div')
  toastWrap.className = 'toast-wrap'
  document.body.appendChild(toastWrap)
  taost.$mount('.toast-wrap')
  setTimeout(() => {
    taost.$destroy() // 销毁组件
    taost.$el.remove() // 删除组件元素
  }, 1000)
}

export default {
  install (Vue) {
    Vue.prototype.$toast = toast
  }
}

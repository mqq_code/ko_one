import Vue from 'vue'
import Confirm from './Confirm'

const ConfirmConstructor = Vue.extend(Confirm) // 获取Confirm组件的构造函数

export const confirm = function ({ msg, ok }) {
  const ins = new ConfirmConstructor() // 创建Confirm组件
  ins.msg = msg // 把参数传给Confirm组件
  ins.handleOk = ok
  const ConfirmWrap = document.createElement('div')
  ConfirmWrap.className = 'Confirm-wrap'
  document.body.appendChild(ConfirmWrap)
  ins.$mount('.Confirm-wrap')
}

export default {
  install (Vue) {
    Vue.prototype.$confirm = confirm
  }
}

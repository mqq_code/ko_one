import Vue from 'vue'
import Vuex from 'vuex'
import logger from 'vuex/dist/logger'
import axios from 'axios'
// import VuexPersistence from 'vuex-persist'

// const vuexLocal = new VuexPersistence({
//   storage: window.localStorage
// })

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    addressList: [], // 地址列表
    curAddress: {}, // 选中的地址
    wareList: [], // 商品列表
    pageInfo: { // 商品分页
      pageCount: null,
      pageNum: 0,
      pageSize: 33,
      total: null
    },
    cartList: [] // 购物车列表
  },
  mutations: {
    // 添加地址
    addAddress (state, payload) {
      state.addressList.unshift({
        ...payload,
        fullAddress: `${payload.city} ${payload.detail}`,
        id: Date.now()
      })
    },
    // 修改地址
    editAddress (state, payload) {
      // 编辑地址，根据传入的id查找对应的下标，替换数据
      const index = state.addressList.findIndex(v => v.id === payload.id)
      const newAddress = {
        ...payload,
        fullAddress: `${payload.city} ${payload.detail}`
      }
      // 修改地址列表中的数据
      state.addressList.splice(index, 1, newAddress)
      // 如果修改的是当前选中的地址，修改 curAddress
      if (state.curAddress.id === newAddress.id) {
        state.curAddress = { ...newAddress }
      }
    },
    // 设置当前地址
    setAddress (state, payload) {
      state.curAddress = { ...payload }
    },
    // 删除地址
    remvoe (state, id) {
      state.addressList = state.addressList.filter(v => v.id !== id)
    },
    // 设置商品列表
    setWareList (state, payload) {
      state.wareList = payload
    },
    // 修改分页
    setPageInfo (state, payload) {
      state.pageInfo = payload
    },
    // 往购物车添加商品
    addCartList (state, payload) {
      const index = state.cartList.findIndex(v => v.wareId * 1 === payload.wareId * 1)
      if (index > -1) {
        state.cartList[index].count++
      } else {
        const item = state.wareList.find(v => v.wareId * 1 === payload.wareId * 1)
        state.cartList.push({
          ...item,
          count: 1,
          checked: true
        })
      }
    },
    // 修改购物车商品数量
    changeCount (state, { wareId, num }) {
      const index = state.cartList.findIndex(v => v.wareId === wareId)
      state.cartList[index].count += num
      if (state.cartList[index].count <= 0) {
        state.cartList.splice(index, 1)
      }
    },
    // 修改商品选中状态
    changeChecked (state, wareId) {
      const index = state.cartList.findIndex(v => v.wareId === wareId)
      state.cartList[index].checked = !state.cartList[index].checked
    },
    // 全选
    changeAllChecked (state, checked) {
      state.cartList.forEach(item => {
        item.checked = checked
      })
    }
  },
  actions: {
    // 获取商品列表
    async getList ({ getters, state, commit }) {
      if (getters.finish) return
      const res = await axios({
        url: '/goods/mp/search/wareSearch',
        method: 'post',
        data: {
          param: {
            venderId: 1,
            storeId: 12527,
            businessCode: 1,
            from: 1,
            categoryType: 1,
            pageNum: state.pageInfo.pageNum + 1,
            pageSize: state.pageInfo.pageSize,
            categoryId: '11340',
            categoryLevel: 1
          }
        },
        // 调用接口之前会先执行此数组中的所有函数
        transformRequest: [
          function (data) {
            // data: 接口传给后端的数据
            // 把data转换格式城 => 'param={venderId: 1,storeId: 12527,businessCode: 1,from: 1,categoryType: 1,pageNum: 1,pageSize: 20,categoryId: "11340",categoryLevel: 1}&a=100&b=200'
            let str = ''
            Object.keys(data).forEach((key) => {
              str += `${key}=${JSON.stringify(data[key])}&`
            })
            str = str.slice(0, -1)
            return str
          }
        ],
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
        }
      })
      commit('setPageInfo', res.data.data.pageInfo)
      commit('setWareList', [...state.wareList, ...res.data.data.wareList])
    }
  },
  getters: {
    finish (state) { // 数据是否加载完成
      return state.pageInfo.pageCount !== null && state.pageInfo.pageNum >= state.pageInfo.pageCount
    },
    total (state) {
      const total = {
        count: 0,
        price: 0
      }
      state.cartList.forEach(item => {
        if (item.checked) {
          total.count += item.count
          total.price += item.count * item.warePrice
        }
      })
      return total
    }
  },
  plugins: [logger()]
  // plugins: [logger(), vuexLocal.plugin]
})

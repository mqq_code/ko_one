import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/home/Index'
import GoHome from '@/views/home/gohome/Index'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/gohome',
    children: [
      {
        path: '/gohome',
        name: 'gohome',
        component: GoHome
      },
      {
        path: '/cart',
        name: 'cart',
        component: () => import('@/views/home/cart/Index')
      },
      {
        path: '/mine',
        name: 'mine',
        component: () => import('@/views/home/mine/Index')
      }
    ]
  },
  {
    path: '/detail/:storeId/:skuId',
    name: 'detail',
    component: () => import('@/views/detail/Index')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/Index')
  },
  {
    path: '/address',
    name: 'address',
    component: () => import('@/views/address/Index')
  },
  {
    path: '/edit',
    name: 'edit',
    component: () => import('@/views/editAddress/Index')
  },
  {
    path: '*',
    component: () => import('@/views/404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/api': {
        target: 'https://wxcmsapi.dmall.com',
        pathRewrite: { '^/api': '' },
        secure: false,
        changeOrigin: true
      },
      '/goods': {
        target: 'https://searchgw.dmall.com',
        pathRewrite: { '^/goods': '' },
        secure: false,
        changeOrigin: true
      }
    }
  }
})

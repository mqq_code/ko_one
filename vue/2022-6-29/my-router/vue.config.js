const { defineConfig } = require('@vue/cli-service')
const hot = require('./data/data.json')
const coming = require('./data/coming.json')
const city = require('./data/city.json')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {
      devServer.app.get('/hot', (req, res) => {
        const { pageNum, pageSize } = req.query
        if (!pageNum || !pageSize) {
          return res.send({
            code: 0,
            msg: '参数错误'
          })
        }
        res.send({
          code: 1,
          msg: '成功',
          values: hot.slice((pageNum - 1) * pageSize, pageNum * pageSize ),
          total: hot.length
        })
      })
      devServer.app.get('/movie/detail', (req, res) => {
        const { filmId } = req.query
        if (!filmId) {
          return res.send({
            code: 0,
            msg: '参数错误'
          })
        }
        let data = [...hot, ...coming]
        let info = data.find(item => item.filmId === filmId * 1)
        if (info) {
          res.send({
            code: 1,
            msg: '成功',
            values: info
          })
        } else {
          res.send({
            code: 0,
            msg: '参数错误'
          })
        }
      })
      devServer.app.get('/coming', (req, res) => {
        const { pageNum, pageSize } = req.query
        if (!pageNum || !pageSize) {
          return res.send({
            code: 0,
            msg: '参数错误'
          })
        }
        res.send({
          code: 1,
          msg: '成功',
          values: coming.slice((pageNum - 1) * pageSize, pageNum * pageSize ),
          total: coming.length
        })
      })
      devServer.app.get('/city/list', (req, res) => {
        res.send(city)
      })


      return middlewares;
    },
  }
})

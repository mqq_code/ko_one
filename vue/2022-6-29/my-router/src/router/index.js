import VueRouter from 'vue-router'
import Vue from 'vue'
import Home from '../page/home/Home'
// import Login from '../page/login/Login'
// import City from '../page/city/City'
// import Detail from '../page/detail/Detail'
// import Notfound from '../page/404'
// 二级
import Movie from '../page/home/movie/Movie'
import News from '../page/home/news/News'
import Cinema from '../page/home/cinema/Cinema'
// import Mine from '../page/home/mine/Mine'
// 三级
import Hot from '../page/home/movie/hot/Hot'
// import Coming from '../page/home/movie/coming/Coming'

// 给vue安装路由的功能
Vue.use(VueRouter)

// 创建路由实例对象
const router = new VueRouter({
  // history原理：利用 h5 pushState 和 popState 往历史栈中存记录实现
  mode: 'history',
  // 配置路由地址和组件的对应关系
  routes: [
    {
      path: '/home',
      component: Home,
      name: 'home',
      redirect: '/home/movie', // 重定向
      meta: { // 路由元信息
        title: '首页'
      },
      children: [
        {
          path: '/home/movie',
          component: Movie,
          name: 'movie',
          meta: {
            title: '电影'
          },
          children: [
            {
              path: '/home/movie',
              name: 'hot',
              component: Hot,
              meta: {
                title: '正在热映'
              }
            },
            {
              path: '/home/movie/coming',
              name: 'coming',
              meta: {
                title: '即将上映'
              },
              // 路由懒加载
              component: () => import('../page/home/movie/coming/Coming')
            }
          ]
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          meta: {
            title: '影院',
            isAtuh: true
          },
          component: Cinema // () => import('../page/home/cinema/Cinema')
        },
        {
          path: '/home/news',
          name: 'news',
          meta: {
            title: '资讯',
            isAtuh: true
          },
          component: News // () => import('../page/home/news/News')
        },
        {
          path: '/home/mine',
          name: 'mine',
          meta: {
            title: '个人中心',
            isAtuh: true
          },
          component: () => import('../page/home/mine/Mine')
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        title: '登陆'
      },
      component: () => import('../page/login/Login')
    },
    {
      path: '/city',
      name: 'city',
      meta: {
        title: '选择城市'
      },
      component: () => import('../page/city/City')
    },
    {
      // 动态路由
      path: '/detail/:filmId/:user/:time',
      name: 'detail',
      meta: {
        title: '详情'
      },
      component: () => import('../page/detail/Detail')
      // 路由独享守卫
      // beforeEnter: (to, from, next) => {
      //   next('/login')
      // }
    },
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '*',
      component: () => import('../page/404')
    }
  ]
})

// 需要拦截的路由名称
// const isAuth = ['mine', 'cinema', 'coming', 'news']
// 定义全局路由前置守卫
// router.beforeEach((to, from, next) => {
//   console.log('to', to)
//   document.title = to.meta.title
//   // 根据路由元信息判断当前路由是否需要登陆
//   if (to.meta.isAtuh) {
//     const token = localStorage.getItem('token')
//     if (!token) {
//       next('/login')
//       return
//     }
//   }
//   next()
// })

export default router

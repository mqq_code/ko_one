import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.prototype.$bus = {}

new Vue({
  router, // 把路由实例对象添加到vue中
  render: h => h(App)
}).$mount('#app')

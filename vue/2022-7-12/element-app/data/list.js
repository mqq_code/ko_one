const Mock = require('mockjs')
const fs = require('fs')

const data = Mock.mock({
  "list|1000": [{
    "id": "@id",
    "index|+1": 1,
    "name": "@cname",
    "region": "@region",
    "province": "@province",
    "city": "@city",
    "county": "@county",
    "age|18-40": 18,
    "sex|0-1": 1,
    "num|0-100": 100
  }]
})
console.log(data.list)

fs.writeFileSync('./list.json', JSON.stringify(data.list))
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home'
import Dashboard from '../views/home/dashboard/Dashboard'
import List from '../views/home/list/List'
import Login from '../views/login/Login'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    children: [
      {
        path: '/',
        name: 'dashboard',
        component: Dashboard
      },
      {
        path: '/list',
        name: 'list',
        component: List
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

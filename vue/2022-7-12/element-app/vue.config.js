const { defineConfig } = require('@vue/cli-service')
const data = require('./data/list.json')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.get('/first/list', (req, res) => {
        res.send({
          code: 1,
          msg: '成功',
          values: [
            {
              name: '吃饭',
              data: [120, 132, 101, 134, 90, 230, 210] // 本周每天吃饭的开销
            },
            {
              name: '打游戏',
              data: [220, 182, 191, 234, 290, 330, 310]
            },
            {
              name: '健身',
              data: [150, 232, 201, 154, 190, 330, 410]
            },
            {
              name: '购物',
              data: [320, 332, 301, 334, 390, 330, 320]
            },
            {
              name: '出行',
              data: [820, 932, 901, 934, 1290, 1330, 1320]
            }
          ]
        })
      })
      devserver.app.get('/second/list', (req, res) => {
        res.send({
          code: 1,
          msg: '成功',
          values: [820, 932, 901, 934, 1290, 1330, 1320] // 本周每天的总消费
        })
      })
      devserver.app.get('/api/list', (req, res) => {
        const { pageSize, pageNum, keywords } = req.query
        let values = []
        let list = [...data]
        if (keywords) {
          list = data.filter(v => v.name.includes(keywords))
        }
        if (isNaN(pageNum) || isNaN(pageSize)) {
          values = list
        } else {
          values = list.slice(pageSize * pageNum - pageSize, pageNum * pageSize)
        }
        res.send({
          code: 1,
          msg: '成功',
          values: {
            list: values,
            pageSize,
            pageNum,
            totalPage: Math.ceil(list.length / pageSize),
            total: list.length
          }
        })
      })
      devserver.app.get('/api/del', (req, res) => {
        const { ids } = req.query
        let idsArr = ids.split(',')
        let flag = idsArr.every(id => data.find(v => v.id === id))
        if (!flag) {
          res.send({
            code: -1,
            msg: '参数错误'
          })
          return
        }
        idsArr.forEach(id => {
          const index = data.findIndex(v => v.id + '' === id + '')
          data.splice(index, 1)
        })
        res.send({
          code: 1,
          msg: '成功'
        })
      })
      devserver.app.get('/api/add', (req, res) => {
        const { name, age, num, sex } = req.query
        if (!name || !age || !num || !sex) {
          res.send({
            code: -1,
            msg: '参数错误'
          })
          return
        }
        data.push({
          ...req.query,
          age: age * 1,
          sex: sex * 1,
          num: num * 1,
          id: Date.now(),
          index: data[data.length - 1].index + 1
        })
        res.send({
          code: 1,
          msg: '成功'
        })
      })
      devserver.app.get('/api/edit', (req, res) => {
        const { id } = req.query
        if (!id) {
          res.send({
            code: -1,
            msg: '参数错误'
          })
          return
        }
        const index = data.findIndex(v => v.id + '' === id + '')
        data.splice(index, 1, {
          ...req.query,
          age: req.query.age * 1,
          sex: req.query.sex * 1,
          num: req.query.num * 1,
        })
        res.send({
          code: 1,
          msg: '成功'
        })
      })

      return middlewares
    }
  }
})

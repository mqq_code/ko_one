## git 版本管理系统

### 安装：
- windows淘宝镜像下载地址：https://npm.taobao.org/mirrors/git-for-windows/
- mac：xcode | brew安装

### 配置用户信息
```shell
git config --global user.name "你的名字"
git config --global user.email "你的邮箱"
git config -l // 查看配置
```
### 初始化仓库
1. 运行 `git init` 初始化仓库，生成.git隐藏目录
2. .git 目录储存git修改记录
3. 删除 .git后，该目录不再被git管理

### git常用命令
1. `git status` 查看仓库状态
2. `git log` 查看修改记录
3. `git add <文件路径>` 把工作区的修改放到暂存区，使文件被git管理
- `git add .` 把当前目录下的所有修改都放到暂存区
4. `git commit -m"内容描述"` 把暂存区的文件提交到git本地仓库
- `git commit -am"内容描述"` git add + git commit -m 两条命令的简写
- 注意：新创建的文件必须先`git add` 然后再 `git commit -m`
5. `git diff` 查看当前工作区的修改内容
6. `git checkout -- <文件路径>` 放弃工作区的修改 **谨慎使用**
7. `git reset HEAD <文件>` 把文件从暂存区放回到工作区
8. `git reset --hard commit号` 回退版本 **谨慎使用**
9. `git reflog` 查看所有的操作记录，包括提交和回滚记录
10. `git revert commit号` 撤销中间某次操作，生成一个新的commit号 **面试重点**

### 连接远程仓库
1. 创建ssh key `ssh-keygen -t rsa -C "你的邮箱" `
2. 把 /.ssh/id_rsa.pub 文件的内容添加到远程仓库的公钥中

### 已有本地仓库，连接远程仓库
1. `git remote add origin 远程仓库地址` 关联本地仓库和远程仓库
2. `git remote -v` 查看本地仓库关联的远程仓库地址
3. `git push -u origin master` 把本地仓库的内容推送到远程仓库

### 直接从远程仓库拉取代码
1. `git clone 仓库地址`
2. `git push` 推送本地仓库代码到远程
3. `git pull` 获取远程仓库最新代码到本地

### 分支管理
1. `git checkout -b <分支名>` 创建分支
2. `git branch` 查看本地分支
-  `git branch -a` 查看本地和远程的所有分支
3. `git checkout <分支名>` 切换分支
4. `git push -u origin <分支名>` 新建的分支第一次推送到远程分支简历关联
5. `git branch -d <分支名>` 删除本地已经合并过的分支
6. `git branch -D <分支名>` 删除本地未被合并过的分支
7. `git merge <分支名>` 把该分支合并到当前分支
8. `git fetch` 同步远程仓库的更新到本地
- **面试常问：如何切换到一个跟本地没有关联的远程分支**
- `git fetch`
- `git checkout <分支名>`
9. `git pull` 相当于 git fetch + git merge
- `git fetch` 更新远程仓库的数据到本地
- `git merge origin/分支` 合并远程分支的代码到本地
10. `git push -f` 强制push **谨慎使用**


### 配置git命令别名
1. `git config --global alias.st status` git st ===> git status
2. `git config --global alias.ci commit` git ci ===> git commit
3. `git config --global alias.co checkout` git co ===> git checkout
4. `git config --global alias.br branch` git br ===> git branch


### 添加忽略文件
1. 创建 .gitignore 文件，配置要git忽略的文件和文件夹
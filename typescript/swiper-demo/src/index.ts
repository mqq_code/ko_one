import './scss/index.scss'
import Swiper from './swiper'
import axios from 'axios'


interface IBanner {
  imageUrl: string;
  targetId: number;
}
interface IBannerRes {
  code: number;
  banners: IBanner[]
}
const swiperWrapper = document.querySelector('.swiper-wrapper') as HTMLDivElement
axios.post<IBannerRes>('https://zyxcl-music-api.vercel.app/banner').then(res => {
  swiperWrapper.innerHTML = res.data.banners.map(v => `
    <div class="swiper-slide">
      <img src="${v.imageUrl}" />
    </div>
  `).join('')

  // 实例化轮播图
  new Swiper({
    el: document.querySelector('.swiper-container') as HTMLDivElement,
    activeIndex: 2,
    autoplay: {
      delay: 3000, // 每隔多长时间自动轮播
      duration: 500, // 过渡动画时间
    },
    nextEl: document.querySelector('.swiper-next') as HTMLDivElement,
    prevEl: document.querySelector('.swiper-prev') as HTMLDivElement,
    paginationEl: document.querySelector('.swiper-pagination') as HTMLDivElement,
  })
})
import './swiper.scss'

interface IAutoPlay {
  duration?: number;
  delay?: number;
}

interface IOptions {
  el: HTMLElement;
  activeIndex?: number;
  autoplay?: IAutoPlay | boolean;
  nextEl?: HTMLElement;
  prevEl?: HTMLElement;
  paginationEl?: HTMLElement;
}


class Swiper {
  container: IOptions['el'] // 要轮播的元素
  wrapper: IOptions['el'] // 轮播内容父级
  slide: Element[] // 轮播的元素列表
  activeIndex: number = 0 // 当前高亮下标
  autoplay: IOptions['autoplay'] // 自动轮播
  nextEl?: HTMLElement;
  prevEl?: HTMLElement;
  paginationEl?: HTMLElement;
  paginationChild?: HTMLSpanElement[]
  autoTimer?: NodeJS.Timer
  constructor ({ el, activeIndex, autoplay, nextEl, prevEl, paginationEl }: IOptions) {
    this.container = el
    this.wrapper = this.container.querySelector('.swiper-wrapper') as HTMLElement
    this.slide = [...this.wrapper.children]
    if (activeIndex && activeIndex >= 0 && activeIndex < this.slide.length) {
      this.activeIndex = activeIndex
    }
    this.nextEl = nextEl
    this.prevEl = prevEl
    this.paginationEl = paginationEl
    // 自动轮播
    this.autoplay = autoplay
    if (this.autoplay) {
      if (typeof this.autoplay === 'boolean') {
        this.autoplay = { 
          duration: 1000,
          delay: 3000
        }
      }
    }
    // 初始化
    this.init()
  }
  init () {
    // 添加初始高亮元素
    this.slide[this.activeIndex].classList.add('swiper-active')
    // 设置轮播时动画时间
    this.slide.forEach(slide => {
      const duration = this.autoplay ? (this.autoplay as IAutoPlay).duration : 1000
      ;(slide as HTMLElement).style.transitionDuration = duration + 'ms'
    })
    // 自动轮播
    if (this.autoplay) {
      this.autoplayFn()
    }
    if (this.paginationEl) {
      this.renderPagination()
    }
    this.bindEvent()
  }
  change (index: number) {
    this.activeIndex = index
    if (index < 0) this.activeIndex = this.slide.length - 1
    if (index >= this.slide.length) this.activeIndex = 0
    // 修改轮播图高亮
    document.querySelector('.swiper-active')?.classList.remove('swiper-active')
    this.slide[this.activeIndex].classList.add('swiper-active')
    // 修改分页器高亮
    if (this.paginationEl) {
      this.paginationEl.querySelector('.active')?.classList.remove('active')
      this.paginationChild![this.activeIndex].classList.add('active')
    }
  }
  renderPagination () {
    this.paginationEl!.innerHTML = this.slide.map((v, i) => `<span class="${this.activeIndex === i ? 'active' : ''}"></span>`).join('')
    this.paginationChild = [...this.paginationEl!.children] as HTMLSpanElement[]
  }
  autoplayFn () {
    this.autoTimer = setInterval(() => {
      this.change(this.activeIndex + 1)
    }, (this.autoplay as IAutoPlay)?.delay || 1000)
  }
  bindEvent () {
    this.nextEl?.addEventListener('click', () => {
      this.change(this.activeIndex + 1)
    })
    this.prevEl?.addEventListener('click', () => {
      this.change(this.activeIndex - 1)
    })
    this.paginationChild?.forEach((span, index) => {
      span.addEventListener('click', () => {
        this.change(index)
      })
    })
    if (this.autoplay) {
      this.container.addEventListener('mouseenter', () => {
        clearInterval(this.autoTimer)
      })
      this.container.addEventListener('mouseleave', () => {
        this.autoplayFn()
      })
    }
    
  }
}

export default Swiper

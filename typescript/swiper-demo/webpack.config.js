const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  // 打包模式
  mode: 'development',
  // 入口文件
  entry: './src/index.ts',
  // 创建打包前后代码的映射文件
  devtool: 'eval-source-map',
  // 出口文件
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'js/index.js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    // 配置loader
    rules: [
      {
        test: /\.(scss|sass|css)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(js|ts)$/i,
        exclude: /node_modules/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  // 插件
  plugins: [
    new HtmlWebpackPlugin({ //打包时自动创建html，引入打包后的js
      template: './public/index.html'
    })
  ],
  // 配置开发服务器
  devServer: {
    open: true,
    hot: true,
    port: 8000
  }
}
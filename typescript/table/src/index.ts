import './scss/index.scss'
import Table from './table/table'
import Pagination from './pagination'
import { getStudent, IStudentItem } from './api'
// 在ts中引入js文件，需要在ts.config.json中添加配置 "allowJs": true
// 如果希望引入的js又类型提示需要手动添加 .d.ts 声明文件       
import { format, query } from './util/format'
// 部分第三方包自带声明文件
import axios from 'axios'
// 如果第三方插件没有自带的声明文件，请尝试下载 @types/包名
// 如果下载失败，就需要自己定义对应的声明文件
import Mock from 'mockjs'


let money = format(10000, 2)
console.log(money)

type IRes = {
  name: string;
  age: string;
}
let res = query<IRes>(location.href)
console.log(res.name)


// 创建表格
let student = new Table<IStudentItem>({
  wrapper: document.querySelector('.table-wrap')!,
  column: [
    {
      title: '姓名',
      key: 'name'
    },
    {
      title: '学号',
      key: 'id'
    },
    {
      title: '性别',
      key: 'sex',
      render: (sex, record) => {
        return sex ? '男' : '女'
      }
    },
    {
      title: '分数',
      key: 'num',
      render: (num, record) => {
        let color = 'green'
        if (num >= 60 && num < 90) {
          color = 'orange'
        } else if (num < 60) {
          color = 'red'
        }
        return `<b style="color: ${color}">${num}</b>`
      }
    },
    {
      title: '详细信息',
      render: (item, item1) => {
        return `我叫${item.name},性别${item.sex ? '男' : '女'},我考了${item.num}分`
      }
    },
    {
      title: '操作',
      render: item => {
        return `<button>删除</button>`
      }
    }
  ],
  dataSource: []
})
// 创建分页器
const pagination = new Pagination({
  wrapper: document.querySelector('.pagination-wrap')!,
  total: 200,
  pagesize: 30,
  current: 1,
  pagesizes: [20, 30, 40],
  onChange: getData,
  layout: ['jump', 'pagesizes', 'pager'] // 确定分页器的布局顺序
})

// 请求数据
function getData (pagenum = 1, pagesize = 30) {
  getStudent({
    pagesize,
    pagenum
  }).then(res => {
    // 给表格添加数据
    student.changeData(res.data.data)
    window.tip('获取数据成功')
    window.tip('当前版本号是' + _VERSION_)
    testMoney = 200
    console.log(testMoney)
    getVersion(new Date().toLocaleString())
  })
}

getData()





// // 创建第二个表格
// let table2 = new Table({
//   wrapper: document.querySelector('.food')!,
//   column: [
//     {
//       title: '早饭',
//       key: 'breakfast'
//     },
//     {
//       title: '午饭',
//       key: 'lunch'
//     },
//     {
//       title: '晚饭',
//       key: 'dinner'
//     }
//   ],
//   dataSource: [
//     {
//       breakfast: '包子',
//       lunch: '饺子',
//       dinner: '面条'
//     },
//     {
//       breakfast: '豆浆',
//       lunch: '鱼香肉丝',
//       dinner: '火锅'
//     },
//     {
//       breakfast: '煎饼',
//       lunch: '大盘鸡',
//       dinner: '烤肉'
//     }
//   ]
// })

// // 3s之后修改表格数据
// setTimeout(() => {
//   table2.changeData([
//     {
//       breakfast: '肉夹馍',
//       lunch: '宫爆鸡丁',
//       dinner: '沙县小吃'
//     }
//   ])
// }, 3000)





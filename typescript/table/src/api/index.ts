import axios from 'axios'


type IParams = {
  pagesize: number;
  pagenum: number;
}
export type IStudentItem = {
  id: number;
  name: string;
  sex: number;
  num: number;
}
type IStudentRes = {
  total: number;
  pagesize?: string;
  pagenum?: string;
  data: IStudentItem[]
}
export const getStudent = (params: IParams) => {
  return axios.get<IStudentRes>('/list/students', { params })
}

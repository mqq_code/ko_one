/// <reference path="../a.d.ts" />
/// 三斜线指令：引入声明文件


// **.d.ts: 类型声明文件
// 只能定义类型，不能写具体逻辑
// **.ts: 既能定义类型也可以写具体实现

// 全局的声明文件中定义全局变量类型
interface Window {
  tip: (msg: string) => void
}
// declare: 定义变量类型
// declare const _VERSION_: string
// declare let testMoney: number
// declare function getVersion(time: string): string


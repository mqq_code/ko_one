
export declare function format(money: number, num: number): string
export declare function query<T>(url: string): T

// export { format, query }

// 在局部模块中定义全局变量类型
declare global {
  // declare interface Window {
  //   tip: (msg: string) => void
  // }
  // // declare: 定义变量类型
  // declare const _VERSION_: string
  // declare let testMoney: number
  declare function getVersion(time: string): string
}


export function format(money, num) {
  return money.toFixed(num)
}

// http://www.baidu.com?a=b&c=100
export function query(url) {
  const res = {}
  const arr = url.split('?')[1].split('&')
  arr.forEach(key => {
    const [k, v] = key.split('=')
    res[k] = decodeURIComponent(v)
  })
  return res
}

import './table.scss'

interface IColumnItem<T> {
  title: string;
  key?: keyof T;
  render?: (params: any, record: T) => string
}

interface Iparams<T> {
  wrapper: HTMLElement;
  column: IColumnItem<T>[];
  dataSource: T[];
}

class Table<T> {
  wrapper: Iparams<T>['wrapper']
  column: Iparams<T>['column']
  dataSource: Iparams<T>['dataSource']
  thead!: HTMLTableRowElement
  tbody!: HTMLTableSectionElement
  constructor ({ wrapper, column, dataSource }: Iparams<T>) {
    this.wrapper = wrapper
    this.column = column // 接收列的数据
    this.dataSource = dataSource // 接收行数据
    this.init()
  }
  init () {
    this.wrapper.innerHTML = `<table border="1">
      <thead><tr></tr></thead>
      <tbody></tbody>
    </table>`
    this.thead = this.wrapper.querySelector('thead tr')!
    this.tbody = this.wrapper.querySelector('tbody')!
    this.renderHead()
    this.renderBody()
  }
  renderHead () {
    this.thead.innerHTML = this.column.map(item => `
      <th>${item.title} - ${String(item.key)}</th>
    `).join('')
  }
  renderBody () {
    this.tbody.innerHTML = this.dataSource.map(item => {
      return `<tr>
        ${this.column.map(v => {
          let text = v.key ? item[v.key] : ''
          if (v.render) {
            text = v.render(v.key ? item[v.key] : item, item)
          }
          return `<td>${text}</td>`
        }).join('')}
      </tr>`
    }).join('')
  }
  changeData(dataSource: Iparams<T>['dataSource']) {
    this.dataSource = dataSource
    this.renderBody()
  }
}

export default Table


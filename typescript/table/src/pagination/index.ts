import './index.scss'

interface ILayout {
  jump: string;
  pagesizes: string;
  pager: string;
}

interface IParams {
  wrapper: HTMLElement;
  total: number;
  pagesize: number;
  current: number;
  pagesizes?: number[];
  layout?: (keyof ILayout)[];
  onChange?: (current: number, pagesize: number) => void
}


class Pagination {
  wrapper: HTMLElement
  total: number // 总条数
  pagesize: number // 每页几条
  current: number // 当前页数
  totalPage: number // 总页数
  pageBtn!: HTMLElement
  prev!: HTMLButtonElement
  next!: HTMLButtonElement
  pageChild: HTMLElement[] = []
  pagesizeWrap?: HTMLSelectElement
  pagesizes: number[] = [10, 15, 20, 50]
  onChange: IParams['onChange']
  layout: IParams['layout'] = ['pager']
  constructor ({ wrapper, total, pagesize, current, pagesizes, layout, onChange }: IParams) {
    this.wrapper = wrapper
    this.total = total
    this.pagesize = pagesize
    this.current = current
    this.totalPage = Math.ceil(total / pagesize)
    if (pagesizes && pagesizes.length > 0) {
      this.pagesizes = pagesizes
    }
    if (layout && layout.length > 0) this.layout = layout
    this.onChange = onChange
    this.init()
  }
  init () {
    this.wrapper.classList.add('pagination-container')
    const layout = {
      jump: `<div class="jump"><input type="text" /><button>跳转</button></div>`,
      pagesizes: this.renderPageSize(),
      pager: `<button class="prev">上一页</button><div class="page-btn"></div><button class="next">下一页</button>`
    }
    this.wrapper.innerHTML = this.layout!.map(key => layout[key]).join('')
    this.pagesizeWrap = this.wrapper.querySelector('.pagesize-wrap')!
    this.pageBtn = this.wrapper.querySelector('.page-btn')!
    this.prev = this.wrapper.querySelector('.prev')!
    this.next = this.wrapper.querySelector('.next')!
    if (this.current <= 1) {
      this.prev.disabled = true
    }
    if (this.current >= this.totalPage) {
      this.next.disabled = true
    }
    this.renderPageBtn()
    this.bindEvent()
  }
  renderPageSize () {
    return `
      <select class="pagesize-wrap">
        ${this.pagesizes.map(v => `<option ${this.pagesize === v ? 'selected' : ''} value="${v}">每页${v}条</option>`).join('')}
      </select>
    `
  }
  // 渲染页码
  renderPageBtn () {
    let pageBtn = new Array(this.totalPage).fill(0)
    this.pageBtn.innerHTML = pageBtn.map((v, i) => `
      <button data-page="${i + 1}" class="${this.current === i + 1 ? 'active' : ''}">${i + 1}</button>
    `).join('')
    this.pageChild = [...this.pageBtn.children] as HTMLElement[]
  }
  // 切换页码
  changePage (pagenum: number) {
    this.current = pagenum
    this.prev.disabled = false
    this.next.disabled = false
    if (pagenum <= 1) {
      this.current = 1
      this.prev.disabled = true
    }
    if (pagenum >= this.totalPage) {
      this.current = this.totalPage
      this.next.disabled = true
    }
    this.wrapper.querySelector('.active')?.classList.remove('active')
    this.pageChild[this.current - 1].classList.add('active')
    // 通知组件更新页码了
    this.onChange && this.onChange(this.current, this.pagesize)
  }
  // 绑定事件
  bindEvent () {
    this.prev.addEventListener('click', () => {
      this.changePage(this.current - 1)
    })
    this.next.addEventListener('click', () => {
      this.changePage(this.current + 1)
    })
    // 事件委托绑定事件
    this.pageBtn.addEventListener('click', (e: MouseEvent) => {
      if ((e.target as HTMLElement).nodeName === 'BUTTON') {
        const pagenum = Number((e.target as HTMLButtonElement).getAttribute('data-page'))
        this.changePage(pagenum)
      }
    })
    // 修改每页条数
    this.pagesizeWrap?.addEventListener('change', () => {
      this.pagesize = Number(this.pagesizeWrap!.value)
      this.totalPage = Math.ceil(this.total / this.pagesize)
      this.renderPageBtn()
      this.changePage(1)
    })
    // 跳转
    let inp = this.wrapper.querySelector('.jump input') as HTMLInputElement
    this.wrapper.querySelector('.jump button')?.addEventListener('click', () => {
      let pagenum = Number(inp?.value)
      if (!isNaN(pagenum)) {
        this.changePage(pagenum)
      }
    })
  }
}

export default Pagination
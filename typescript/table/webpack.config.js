const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')


function createData (count) {
  let arr = []
  const firstName = ['姚', '马', '黄', '张', '李', '侯', '裴', '陈', '杜', '赵', '王', '牛', '林', '武', '吴', '韩', '许']
  const lastName = ['星星', '娇娇', '萱萱', '婷儿', '鲲', '困', '有容', '乃大', '壕', '鹏', '牛马', '邦硬', '大朗', '翠花', '铁柱', '胜利', '富贵', '世贤', '品如']
  for (let i = 0; i < count; i ++) {
    arr.push({
      id: i,
      name: firstName[Math.floor(Math.random() * firstName.length)] + lastName[Math.floor(Math.random() * lastName.length)],
      sex: Math.floor(Math.random() * 2),
      num: Math.floor(Math.random() * 101)
    })
  }
  return arr
}
const data = createData(200)



module.exports = {
  // 打包模式
  mode: 'development',
  // 入口文件
  entry: './src/index.ts',
  // 创建打包前后代码的映射文件
  devtool: 'eval-source-map',
  // 出口文件
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'js/index.js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: [".ts", ".tsx", ".js"]
  },
  module: {
    // 配置loader
    rules: [
      {
        test: /\.(scss|sass|css)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(js|ts)$/i,
        exclude: /node_modules/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  // 插件
  plugins: [
    new HtmlWebpackPlugin({ //打包时自动创建html，引入打包后的js
      template: './public/index.html'
    })
  ],
  // 配置开发服务器
  devServer: {
    open: true,
    hot: true,
    port: 8000,
    setupMiddlewares (middlewares, devserver) {
      devserver.app.get('/list/students', (req, res) => {
        const { pagesize, pagenum } = req.query
        let values = [...data]
        if (pagesize && pagenum) {
          values = data.slice(pagenum * pagesize - pagesize, pagenum * pagesize)
        }
        res.send({
          total: data.length,
          pagesize,
          pagenum,
          data: values
        })
      })
      return middlewares
    }
  }
}
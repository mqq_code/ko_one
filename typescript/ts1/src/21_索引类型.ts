
{

  interface IRes {
    name: string;
    age: number;
    sex: string;
    [k: string]: any; // 索引类型
  }

  let a: IRes['name']

  let obj: IRes = {
    name: 'string',
    age: 20,
    sex: '男',
    married: true
  }

}




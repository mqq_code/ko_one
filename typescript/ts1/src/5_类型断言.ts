{

// 当开发者比ts更确定变量是什么类型是可以使用类型断言
let inp = document.querySelector('.inp') as HTMLInputElement
let btn = document.querySelector('.btn')
let params: unknown

inp.addEventListener('change', (e: Event) => {
  console.log((e.target as HTMLInputElement).value)
})


// 类型断言:btn变量肯定是一个button元素
;(btn as HTMLButtonElement).onclick = function (e) {
  console.log((e.target as HTMLButtonElement).innerHTML)

  let str = (<HTMLInputElement>inp).value
  // console.log(inp?.classList) // 可选链，?前的元素可能不存在
  console.log(inp!.classList) // 非空断言，确定!前的元素一定不为null或者undefined
  if (str === 'age') {
    params = '年龄'
    // 当确定unknown类型的变量类型时需要使用断言修改类型
    console.log((params as string).slice(1))
  } else if (str === 'money') {
    params = 1000
    console.log((<number>params).toFixed(2))
  }
}



// let aa: string | null

// setTimeout(() => {
//   aa = Math.random() + ''
//   console.log('1000', aa)
// }, 1000)

// setTimeout(() => {
//   aa = aa!.split('').reverse().join('')
//   console.log(aa)
// }, 2000)


function change () {
  bb = 'abcdefg'
}

let bb: string
change()
console.log(bb!)



}

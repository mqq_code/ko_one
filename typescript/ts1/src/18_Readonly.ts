
{
  // Partial: 变成可选
  // Readonly: 变成只读
  // Pick
  // Record

  interface IPerson {
    name: string;
    age: number;
    sex: string;
  }

  // Readonly<IPerson>： 把 IPerson 的所有属性都改成只读属性
  type Iparams = Readonly<IPerson>
  let a: Iparams = {
    name: 'a',
    age: 10,
    sex: '1'
  }
  /*
    type Iparams = IPerson {
      readonly name: string;
      readonly age: number;
      readonly sex: string;
    }
  */

}




{
  // 1. 定义函数
  // 函数声明
  // function getSum(a: number, b: number): number {
  //   return a + b
  // }

  // 函数表达式
  // const getSum = function (a: number, b: number): number {
  //   return a + b
  // }
  // 箭头函数
  // let getSum = (a: number, b: number): number => {
  //   return a + b
  // }

  // // 类型推论：当定义变量没有声明类型时，会根据赋值反推变量的类型
  // let total = getSum(1, 2)


  // 2. 函数的可选参数, 可选参数必须在必填参数后边
  // function ajax(url: string, method?: string, data?: string) {
  //   let xhr = new XMLHttpRequest()
  //   xhr.open(url, method || 'get')
  // }

  // ajax('/api')

  // 3. 函数参数默认值
  // function ajax(url: string, method = 'get', data?: string) {
  //   let xhr = new XMLHttpRequest()
  //   xhr.open(url, method)
  // }

  // ajax('/api')


  // 4. 函数的剩余参数
  // const sum = (a: number, ...rest: number[]): number => {
  //   return rest.reduce((prev, val) => prev + val)
  // }
  // console.log(sum(1, 2, 3, 4, 5, 6, 7))


  // 5. 函数重载
  type IParams = number | string | any[] // 类型别名
  function sum (a: number, b: number): number
  function sum (a: string, b: string): string
  function sum (a: any[], b: any[]): any[]
  function sum (a: IParams, b: IParams): IParams | undefined {
    if (typeof a === 'string' && typeof b === 'string') {
      return a + b
    } else if (typeof a === 'number' && typeof b === 'number') {
      return a + b
    } else if (Array.isArray(a)) {
      return a.concat(b)
    }
  }
  console.log(sum(['10'], [10]))






}

{
  // 映射类型：根据一个类型生成的新类型

  interface IRes {
    name: string;
    age: number;
    sex: string;
  }
  type Readonly1<T> = {
    readonly [k in keyof T]: T[k]
  }
  type Iobj = Readonly1<IRes>

}




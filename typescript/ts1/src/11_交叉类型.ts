{

  type Ia = { name: string }
  type Ib = { hobby: string[]}
  // 使用 & 合并多个类型就是交叉类型
  type IPerson = Ia & Ib & {
    age: number
  }
  let person: IPerson = {
    name: '',
    age: 10,
    hobby: ['2']
  }

  // 如果使用 & 合并多个基础类型结果就是 never 类型
  type Ic = number & string


}

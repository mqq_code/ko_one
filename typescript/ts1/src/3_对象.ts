
// 定义类型别名
// type IObj = {
//   name: string;
//   age: number;
//   readonly sex: number;
//   hobby?: string[];
// }
// 定义接口
interface IObj {
  name: string;
  age: number;
  readonly sex: number;
  hobby?: string[]; // 可选属性
}

let obj: IObj = {
  name: '侯婷儿',
  age: 20,
  sex: 1
}

// 可选链：?前的变量存在的话执行后续内容，否则不执行
// console.log(obj.hobby?.join(''))
// console.log(obj.hobby && obj.hobby.join(''))
// if (obj.hobby) {
//   obj.hobby.join('')
// }


// function getInfo ({ name, age, sex, hobby }: IObj) {
//   console.log(`我叫${name},我今年${age}岁,性别${sex ? '男' : '女'}，爱好${hobby?.join('')}`)
// }

// console.log(getInfo(obj))

type IPerson = {
  name: string;
  age: number;
  sex: 0 | 1; // 文字类型: sex的值只能是 0或者1
  children?: IPerson[];
}

let xiaoming: IPerson = {
  name: '小明',
  age: 50,
  sex: 1,
  children: [
    {
      name: '小小明',
      age: 30,
      sex: 1,
      children: [
        {
          name: '小小明',
          age: 30,
          sex: 1,
          children: [
            {
              name: '小小明',
              age: 30,
              sex: 1
            }
          ]
        }
      ]
    },
    {
      name: '小二明',
      age: 20,
      sex: 0
    }
  ]
}

xiaoming.children?.forEach(child => {
  console.log(child)
})




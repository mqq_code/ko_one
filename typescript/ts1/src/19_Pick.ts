
{
  // Partial: 变成可选
  // Readonly: 变成只读
  // Pick: 根据接口中的部分属性组成新接口
  // Record

  interface IRes {
    name: string;
    age: number;
    sex: string;
    hobby: string[];
    height: number;
    weight: number;
    desc: string;
    color: string;
  }

  
  // Pick<IRes, key1 ｜ key2> 从IRes接口中挑选其中某些属性组成新的接口
  type IObj = Pick<IRes, 'name' | 'age' | 'sex' | 'hobby'>

  let obj: IObj = {
    name: 'string',
    age: 10,
    sex: '3',
    hobby: ['a', 'b', 'c', 'd']
  }


}




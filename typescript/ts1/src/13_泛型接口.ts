
{
  // 泛型接口
  interface IPerson<T> {
    name: string;
    age: number;
    sex: T;
    getSex(sex: T): string;
  }
  // type IPerson<T> = {
  //   name: string;
  //   age: number;
  //   sex: T;
  //   getSex(sex: T): string;
  // }

  // let xm: IPerson<string> = {
  //   name: '小明',
  //   age: 10,
  //   sex: '男',
  //   getSex(sex: string): string {
  //     return sex
  //   }
  // }

  // let xh: IPerson<number> = {
  //   name: '小红',
  //   age: 10,
  //   sex: 1,
  //   getSex(sex: number): string {
  //     return sex ? '男' : '女'
  //   }
  // }




  // 定义多维数组类型
  // type IdeepArr<T> = (T | IdeepArr<T>)[]
  type IdeepArr<T> = Array<T | IdeepArr<T>>

  let arr: IdeepArr<number> = [1,[2,[3,4,5,6],7],[9,[[8],7],6],[[[[5],4],3],2,1],10,11,12]
  let arr1: IdeepArr<string> = [['a',['b','c']],['d','e'],'f']
  let arr2: IdeepArr<{name: string}> = [
    [{name: 'aa'}, {name: 'bb'}],
    [{name: 'cc'}, [{name: 'dd'}]],
  ]
  let arr4 = [1, [2, [3, [4, [5, 6], 7], 8], 9], 10]

  function flat<T>(arr: IdeepArr<T>): T[] {
    return arr.reduce((prev: T[], val: T | IdeepArr<T>) => {
      return prev.concat(Array.isArray(val) ? flat(val) : val)
    }, [])
  }
  
  let res = flat<number>(arr4).map(v => v * 2)
  let res1 = flat<string>(arr1).map(v => v.toUpperCase())
  let res2 = flat<{name: string}>(arr2).map(v => v.name.toUpperCase())
  console.log(res2)






}




{

  type IArr = number[][]
  let arr: IArr = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12]
  ]

  // 定义类型别名
  type Iobj = {
    name: string;
    age: number;
    sex: string;
  }
  function getInfo (obj: Iobj) {
    return `我叫${obj.name}，今年${obj.age}，我是${obj.sex}生`
  }
  let xm: Iobj = {
    name: '王小明',
    age: 10,
    sex: '男'
  }
  console.log(getInfo(xm))

  
  type IA = number | string | (number | string)[]
  function fn(a: IA): IA {
    console.log(a)
    return a
  }

  let arr1: (number | string)[] = [1, 2, 3, 4, 'a', 'b']



}
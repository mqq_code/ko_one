
{
  
  class List<T> {
    list: T[]
    constructor(...rest: T[]) {
      this.list = rest
    }
    add (...rest: T[]) {
      this.list.push(...rest)
    }
  }


  let arr = new List<number>(1,2,3,4)
  arr.list.forEach(v => {
    console.log(v.toFixed(1))
  })
  arr.add(4,5,6,7)
  console.log(arr.list)





}




{
  // 1. 定义类
  // class Person {
  //   // 定义实例属性
  //   name: string
  //   age: number
  //   // 定义实例方法
  //   say (): void {
  //     console.log(`我叫${this.name},今年${this.age}岁`)
  //   }
  //   setAge (age: number) {
  //     this.age = age
  //     console.log('我改变年龄了')
  //   }
  //   // 构造函数
  //   constructor (name: string, age: number) {
  //     this.name = name
  //     this.age = age
  //   }
  //   // 静态属性: 类本身的属性叫静态属性，实例化对象无法访问静态属性，静态方法一般定义工具函数使用
  //   static type: string = 'person'
  //   static isPerson (obj: any) {
  //     return obj instanceof Person
  //   }
  // }
  // let xm = new Person('小明', 20)
  // console.log(xm)
  // // xm.say()
  // // xm.setAge(30)
  // // xm.say()
  // console.log(Person.isPerson(xm))


  // 2. 修饰符
  // class Person {
  //   public name: string // 公共属性：在class、实例化对象、子类中都可以访问
  //   private age: number // 私有属性：只能在本身的class中访问，实例化对象、子类中都无法访问
  //   protected sex: string = '男' // 受保护的属性：只能在本身的class和子类中访问，实例化对象无法访问
  //   readonly hobby: string[] = [] // 只读属性，不可修改
  //   say (): void {
  //     console.log(`我叫${this.name},今年${this.age}岁`)
  //   }
  //   setAge (age: number) {
  //     this.age = age
  //     console.log('我改变年龄了')
  //   }
  //   constructor (name: string, age: number) {
  //     this.name = name
  //     this.age = age
  //   }
  // }
  // let xm = new Person('小明', 20)
  // console.log(xm.name)
  // xm.say()

  // 3. 继承
  // class Animal {
  //   name: string
  //   age: number
  //   private sex: string // 私有属性只能 Animal 类中使用
  //   protected weight: number // 受保护的, Animal中和子类中可以使用，实例化对象不可以使用
  //   constructor (name: string, age: number, sex: string, weight: number) {
  //     this.name = name
  //     this.age = age
  //     this.sex = sex
  //     this.weight = weight
  //   }
  //   run () {
  //     console.log('我会跑')
  //   }
  //   eat () {
  //     console.log('我会吃')
  //   }
  // }
  // // let pig = new Animal('佩奇', 3, '母', 300)

  // class Dog extends Animal {
  //   color: string
  //   type: string
  //   constructor (name: string, age: number, sex: string, weight: number, color: string, type: string) {
  //     super(name, age, sex, weight) // 调用父类的构造函数
  //     this.color = color
  //     this.type = type
  //   }
  //   swim () {
  //     console.log('我天生会游泳')
  //     console.log(this.weight)
  //   }
  // }
  // let 旺财 = new Dog('旺财', 6, '公', 30, '黄狗', '土狗')
  // console.log(旺财)
  // 旺财.swim()

  // class Cat extends Animal {
  //   hobby: string[] = []
  //   constructor (name: string, age: number, sex: string, weight: number, hobby: string[]) {
  //     super(name, age, sex, weight)
  //     this.hobby = hobby
  //   }
  //   upTree () {
  //     console.log('我会上树')
  //   }
  // }

  // let 汤姆 = new Cat('汤姆', 3, '公', 10, ['吃鱼', '睡觉'])
  // console.log(汤姆)


  // 4. 抽象类
  // 定义类实现的规范，不可以实例化，只能继承
  abstract class Animal {
    // 继承抽象类的class必须实现抽象属性和抽象方法
    abstract name: string
    abstract age: number
    abstract say(a: string): void

    // 可以定义实例属性和方法
    type: string = '动物'
    run () {
      console.log('我喜欢提桶跑路')
    }
  }

  class Dog extends Animal {
    name: string
    age: number
    say(a: string): void {
      
    }
    constructor (name: string, age: number) {
      super()
      this.name = name
      this.age = age
    }
  }

  // let dog = new Dog('大黄', 10)
  // console.log(dog)

}
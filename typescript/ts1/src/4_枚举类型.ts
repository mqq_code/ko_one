{

let inp = document.querySelector('.inp') as HTMLInputElement

// 使用枚举来描述一些状态可以提高代码可读性
enum KeyCode {
  UP = 38,
  DOWN = 40,
  LEFT = 37,
  RIGHT = 39,
  A = 65,
  W = 87,
  S = 83,
  D = 68
}

inp!.onkeydown = (e) => {
  if (e.keyCode === KeyCode.UP) {

  } else if (e.keyCode === KeyCode.DOWN) {

  } else if (e.keyCode === KeyCode.LEFT) {

  } else if (e.keyCode === KeyCode.RIGHT) {

  } else if (e.keyCode === KeyCode.A) {

  } else if (e.keyCode === KeyCode.S) {

  } else if (e.keyCode === KeyCode.D) {

  } else if (e.keyCode === KeyCode.W) {

  }
}

enum Test {
  hot = 0,
  suggest = 1,
  result = 2
}
console.log(Test)


enum Sex {
  men = 1,
  women = 2,
  unkonwn = 3
}

function getSex (s: number) {
  if (s === Sex.men) {
    console.log('男')
  } else if (s === Sex.women) {
    console.log('女')
  } else if (s === Sex.unkonwn) {
    console.log('不知道')
  }
}



}

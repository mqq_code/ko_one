
{

  let obj = {
    name: '小小',
    age: 10,
    sex: '男',
    hobby: ['吃饭', '睡觉']
  }

  // typeof: 后边写js变量，返回该变量的类型
  let str = '123'
  type Is = typeof str
  type IObj = typeof obj

  // keyof: 后边写类型，返回对象类型中的所有key值
  type Ik = keyof IObj // type Ik = "name" | "age" | "sex" | "hobby"
  let a: Ik = 'name'


  // 条件类型
  type Itest<T extends number | string> = T extends string ? number : string
  
  // 如果a是字符串返回a.length,如果a是数字，保留一位小数返回
  function getLen<T extends number | string>(a: T): Itest<T>  {
    if (typeof a === 'string') {
      return a.length as Itest<T>
    }
    return a.toFixed(1) as Itest<T>
  }

  let strlen = getLen<string>('aaa')
  let money = getLen<number>(100)
  console.log(money)
  console.log(money)




  function getType<T>(a: T): string {
    return Object.prototype.toString.call(a).split(' ')[1].slice(0, -1)
  }
  console.log(getType<number>(1))
  console.log(getType<string>('aa'))
  console.log(getType<boolean>(true))
  console.log(getType<null>(null))
  console.log(getType<undefined>(undefined))
  console.log(getType<any[]>([]))
  console.log(getType<symbol>(Symbol(1)))
  console.log(getType<Function>(getLen))
  console.log(getType<Object>({}))




}




{
// jsvascript类型
// 基础类型：string、number、boolean、symbol、null、undefined
// 引用类型：object、array、function

// 类型注解
let str: string = 'qqq'
let num: number = 100
let flag: boolean = true
let s: symbol = Symbol(1)

// null和undefined类型的值只有它本身
let n: null = null
let un: undefined = undefined

// any: 任意类型，相当于放弃了类型校验，尽量少用
let a: any = 0
a = []
a = {}
a = '11'

// void: 没有值,当函数没有返回值时使用
const sum = (a: number, b: number): void => {
  console.log(111)
}

// unknown: 暂时不确定是什么类型，确定类型时需要通过类型断言指定类型
let test: unknown
test = 100;
(test as number).toFixed()

test = 'abc';
(test as string).indexOf('a')

// never: 不会出现的值
function err(msg: string): never {
  throw new Error(msg)
}

// 数组
// let arr: number[] = [1, 2, 3]
let arr: Array<number> = [1, 2, 3] // 泛型方式定义数组

// 元组：定义已知数量和类型的数组, 如果后续想给元组添加内容只能添加已经定义过的类型
let arr1: [number, string] = [10002, 'asddsadasdas']
arr1.push(100)
arr1.push('11111')
// arr1.push(true) 不可以添加未使用过的类型


}




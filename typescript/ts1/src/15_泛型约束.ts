
{

  // 示例一：传入的参数必须有length属性
  // function getMaxLen<T extends { length: number }>(a: T, b: T): number {
  //   return a.length > b.length ? a.length : b.length
  // }
  // console.log(getMaxLen('aaa', 'bbbbb'))
  // console.log(getMaxLen([4,5,6,7,8,9], [1,2,3,],))
  // console.log(getMaxLen({
  //   name: '小王',
  //   length: 10
  // }, {
  //   name: '小明',
  //   length: 5
  // }))

  // 示例二：T 必须是string或者number
  // function sum<T extends string | number>(a: any, b: any): T {
  //   return a + b
  // }
  // console.log(sum<number>(1, 2))
  // console.log(sum<string>('a', 'b'))
  // console.log(sum<boolean>(true, false))



  // 示例三：传入的第二个参数必须是第一个参数的属性名
  function getData<T, K extends keyof T>(obj: T, key: K): T[K] {
    return obj[key]
  }
  let obj = {
    name: '小小',
    age: 10,
    sex: '男',
    hobby: ['吃饭', '睡觉']
  }
  type IKey = keyof typeof obj
  getData<typeof obj, IKey>(obj, 'hobby')
  getData<number[], number>([1,2,3,4], 10)


}




{
  /*
  let res = [
    {
      title: '许嵩',
      songs: [
        {
          name: '断桥残雪',
          date: 2008
        },
        {
          name: '玫瑰花的葬礼',
          date: 2008
        },
        {
          name: '灰色头像',
          date: 2009
        }
      ]
    },
    {
      title: '徐良',
      songs: [
        {
          name: '坏女孩',
          date: 2010
        },
        {
          name: '客官不可以',
          date: 2011
        },
        {
          name: '飞机场',
          date: 2001
        }
      ]
    },
    {
      title: '汪苏泷',
      songs: [
        {
          name: '有点甜',
          date: 2010
        },
        {
          name: '不分手的恋爱',
          date: 2011
        },
        {
          name: '风度',
          date: 2012
        }
      ]
    }
  ]
  // 1. 定义接口
  let list = document.querySelector('.list')
  interface ISongs {
    name: string;
    date: number;
  }

  interface IList {
    title: string;
    songs: ISongs[]
  }

  let arr: IList[] = []

  function render() {
    arr = res
  }
  render()

  list!.innerHTML = arr.map(item => `
    <li>
      <h3>${item.songs}</h3>
      <ol>
        ${item.songs.map(val => `
          <li>${val.name} - ${val.date}</li>
        `)}
      </ol>
    </li>
  `).join('')
  
  */
  /*
  // interface IPerson {
  //   name: string;
  //   age: number;
  //   sex: string;
  //   children?: IPerson[]
  // }
  type IPerson = {
    name: string;
    age: number;
    sex: string;
    children?: IPerson[]
  }

  let 王家族谱: IPerson[] = [
    {
      name: '王老明',
      age: 90,
      sex: '男',
      children: [
        {
          name: '王权富贵',
          age: 60,
          sex: '男',
          children: [
            {
              name: '王金贵',
              age: 30,
              sex: '男'
            },
            {
              name: '王同归',
              age: 23,
              sex: '男'
            }
          ]
        }
      ]
    },
    {
      name: '王二明',
      age: 87,
      sex: '男',
      children: [
        {
          name: '王婆',
          age: 58,
          sex: '女'
        }
      ]
    }
  ]
  */

  // 2. 函数接口
  /*
  // interface ISum {
  //   (a: number, b: number): number 
  // }
  // let sum: ISum
  // sum = (a: number, b: number): number => {
  //   return a + b
  // }

  // 定义函数接口
  interface IAxios {
    // 定义函数本身的参数和返回值类型
    (options: {url: string; method: string}): void;
    // 定义函数的静态方法的参数和返回值类型
    post: (url: string) => void;
    get: (url: string) => void;
  }

  const axios: IAxios = function (options: {url: string; method: string}) {
    console.log('接口地址:', options.url)
    console.log('调用方式:', options.method)
  }
  axios.post = function (url: string) {
  }
  axios.get = function (url: string) {
  }

  // axios({
  //   url: '/api',
  //   method: 'post'
  // })
  axios.post('/api')
  axios.get('/list')
  */


  // 3. 类接口
  /*
  interface IPerson {
    name: string;
    age: number;
    sex?: string;
    say?(text: string): void;
    getInfo(): string;
  }

  abstract class Base {
    // 定义抽象属性和方法，子类中必须实现抽象属性和方法
    abstract name: string;
    abstract age: number;
    abstract sex: string;
    abstract say(text: string): void;
    abstract getInfo(): string;

    // 定义实例方法
    setName (name: string) {
      this.name = name
    }
  }

  // 抽象类规范子类必须实现抽象类的属性和方法，可以定义具体函数和属性
  // class Person extends Base {
  

  // Person类实现接口中的属性和方法，接口可以定义非必传属性
  class Person implements IPerson {
    name: string = ''
    age: number = 0
    sex: string = ''
    say(text: string): void {
      console.log(`${this.name}说：${text}`)
    }
    getInfo(): string {
      return ''
    }
    setName () {
    }
  }

  let xm = new Person()
  console.log(xm)
  // xm.setName('小明')
  // console.log(xm)

  // 抽象类和接口
  // 相同点：约束 class 的属性和方法的类型
  // 不同点：
  // 1. 抽象类可以定义具体的属性和方法让子类继承，接口不行
  // 2. 接口可以定义可选属性和方法，抽象类不可以
  */

  /*

  class Person {
    name: string
    age: number
    hobby: string[] = []
    constructor (name: string, age: number) {
      this.name = name
      this.age = age
    }
  }

  type IOptions = {
    name: string;
    age: number;
    job: string;
    sex: string;
  }
  interface IDoctor {
    job: string;
    sex: string;
    name: string;
    age: number;
    hobby: string[];
  }
  interface IStudent {
    classname: string
  }
  // class 只能继承一个父类，但是可以实现多个接口，使用逗号分隔
  class Doctor extends Person implements IDoctor,IStudent {
    job: string
    sex: string
    classname: string = '英语'
    constructor ({ name, age, job, sex }: IOptions) {
      super(name, age)
      this.job = job
      this.sex = sex
    }
  }

  let xm = new Doctor({
    name: '王小明',
    age: 20,
    job: '外科医生',
    sex: '男'
  })
  console.log(xm)
  */


  // 4. 接口继承
  /*
  interface IPerson {
    name: string;
    sex: string;
    age: number;
  }
  type IPerson = {
    name: string;
    sex: string;
    age: number;
  }
  class IPerson {
    name: string = ''
    sex: string = ''
    age: number = 0
    getInfo(): string {
      return ''
    } 
  }
  let person: IPerson = {
    name: '小明',
    age: 10,
    sex: '男',
    getInfo(): string {
      return ''
    } 
  }

  // 接口继承，可以继承 interface、type、class
  interface IDoctor extends IPerson {
    job: string;
  }
  let doctor: IDoctor = {
    name: '小红',
    age: 10,
    sex: '男',
    job: '医生',
    getInfo() {
      return ''
    }
  }

  */

  // 5. interface 和 type 的区别

  // 接口重复定义会合并接口内容
  // interface IPerson {
  //   name: string
  // }
  // interface IPerson {
  //   age: number;
  // }

  // type 重复定义直接报错
  // type IPerson = {
  //   name: string
  // }
  // type IPerson = {
  //   age: number
  // }

  // 接口可以使用继承扩展
  // type 可以使用交叉类型扩展
  type Ia = { name: string }
  type Ib = { hobby: string[]}
  type IPerson = Ia & Ib & {
    age: number
  }

  let person: IPerson = {
    name: '',
    age: 10,
    hobby: ['2']
  }







}
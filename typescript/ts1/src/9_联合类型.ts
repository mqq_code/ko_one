{
  function getLen (a: string | any[]): number {
    // 联合类型：只能访问所有类型共有的属性和方法
    if (Array.isArray(a)) { // 类型保护
      // 如果要使用其中某一种类型独有的方法，必须判断类型
      console.log(a.reverse())
    } else {
      console.log(a.replace('1', '111111'))
    }
    return a.length
  }
  
  getLen('12345')
  

}
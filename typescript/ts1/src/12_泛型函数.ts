
{

// 当函数内使用的类型不确定时可以使用泛型
// function flat<T>(arr: (T | T[])[]): T[] {
//   let res: T[] = []
//   arr.forEach(item => {
//     if (Array.isArray(item)) {
//       res.push(...item)
//     } else {
//       res.push(item)
//     }
//   })
//   return res
// }

// let arr = [1, [2, 3, 4], 5, [6, 7, 8], 9]
// let arr1 = ['a', ['b', 'c', 'd'], ['e', 'f']]

// let arr2 = flat<string>(arr1).map(v => v.toUpperCase())
// let arr3 = flat<number>(arr).map(v => v * 2)
// console.log(arr2)
// console.log(arr3)




  interface IOptons<T> {
    url: string;
    method?: 'get' | 'post';
    data?: any;
    success: (res: T) => void;
  }

  function axios<T>({ url, method = 'get', data, success }: IOptons<T>) {
    let xhr = new XMLHttpRequest()
    xhr.open(method, url)
    xhr.send()
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200 || xhr.status === 304) {
          let data: T = JSON.parse(xhr.responseText)
          success(data)
        }
      }
    }
  }

  // 定义天气接口返回值类型
  interface IRes {
    status: number;
    desc: string;
    data: {
      city: string;
      forecast: {
        date: string;
        fengli: string;
        fengxiang: string;
        high: string;
        low: string;
        type: string;
      }[]
    }
  }
  axios<IRes>({
    url: 'http://wthrcdn.etouch.cn/weather_mini?city=北京',
    success: res => {
      res.data.forecast.forEach(item => {
        console.log(item.date)
      })
    }
  })








}




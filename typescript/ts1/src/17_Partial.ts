
{
  // Partial
  // Pick
  // Readonly
  // Record

  interface IPerson {
    name: string;
    age: number;
    sex: string;
  }

  // Partial<IPerson>： 把 IPerson 的所有属性都改成非必传
  type Iparams = Partial<IPerson>
  /*
    type Iparams = IPerson {
      name?: string;
      age?: number;
      sex?: string;
    }
  */

  function changeObj (params: Iparams) {
    obj = {...obj, ...params}
  }


  let obj: IPerson = {
    name: '小明',
    age: 20,
    sex: '男'
  }
  changeObj({
    sex: '女',
    age: 100
  })
  console.log(obj)



}




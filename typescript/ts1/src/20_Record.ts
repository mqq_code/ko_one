
{
  // Partial: 变成可选
  // Readonly: 变成只读
  // Pick: 根据接口中的部分属性组成新接口
  // Record

  interface IRes {
    name: string;
    age: number;
    sex: string;
    hobby: string[];
    height: number;
    weight: number;
    desc: string;
    color: string;
  }

  let obj = {
    a: true,
    b: '',
    c: 100
  }

  type Ib = Record<keyof typeof obj, any[]>
  // 生成所有属性类型一致的接口
  type Ia = Record<'a' | 'b' | 'c', number>
  // type Ia = {
  //   a: number;
  //   b: number;
  //   c: number;
  // }


}




{

// 定义变量时如果没有声明类型，会根据赋值内容推断出改变量的类型
// let a = 100 // 推断出a的类型为number

// a = '' // 报错

// // 推断出 obj 的类型为 { name: string }
// let obj = {
//   name: '小明'
// }
// // obj的类型中不存在sex，报错
// obj.sex = '男'


}